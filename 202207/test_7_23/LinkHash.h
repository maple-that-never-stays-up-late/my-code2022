#pragma once
#pragma once
#include<vector>

//哈希函数1
template<class K>
struct Hash
{
    size_t operator()(const K& key)
    {
        return key;
    }
};

//哈希函数2
template<>
struct Hash<string>
{
    size_t operator()(const string& s)
    {
        // BKDR
        size_t value = 0;
        for (auto ch : s)
        {
            value *= 31;
            value += ch;
        }
        return value;
    }
};

//哈希表节点
template<class T>
struct HashNode
{
    T _data;
    HashNode<T>* _next;

    HashNode(const T& data)
        :_data(data)
        , _next(nullptr)
    {}
};



//开散列的哈希实现
namespace LinkHash
{
template<class K, class T, class Ref, class Ptr, class KeyOfT, class HashFunc>
struct __HTIterator
{
    typedef HashNode<T> Node;//哈希表节点类型重命名为Node
    typedef __HTIterator<K, T, Ref, Ptr, KeyOfT, HashFunc> Self;  //迭代器自身类型重命名为self

    Node* _node;
    HashTable<K, T, KeyOfT, HashFunc>* _pht;//指向哈希桶对象的指针,为了当前桶走完之后,去下一个桶

    //构造函数
    __HTIterator(Node* node, HashTable<K, T, KeyOfT, HashFunc>* pht)
        //节点的指针 + 哈希表对象的指针
        :_node(node)
        , _pht(pht)
    {}

	Ref operator*()
	{
		return _node->_data;//返回节点指向的数据
	}

	Ptr operator->()
	{
		return &_node->_data;//返回节点指向的数据的地址  
	}

	//前置++ 返回迭代器引用
	Self& operator++()
	{
		if (_node->_next)
		{
			_node = _node->_next;//去该桶的下一个位置
		}
		else	//当前_node节点的next是空了,当前桶走完了,要去下一个桶
		{
			//算一下我在第几个桶  用_node当场算
			KeyOfT kot;//不知道是key还是pair,把key取出来 
			HashFunc hf;//把取出来的key转化为整数
			size_t index = hf(kot(_node->_data)) % _pht->_tables.size();

			//  从index+1的桶开始找,因为有些桶是没有数据的 找下一个不为空的桶 
			++index;
			while (index < _pht->_tables.size())
			{
				if (_pht->_tables[index])   //不为空,说明有数据
				{
					break;//找到了不为空的桶
				}
				else
				{
					++index;
				}
			}

			// case1:表走完了，都没有找到下一个桶
			if (index == _pht->_tables.size())
			{
				_node = nullptr;
			}
			else 	//case2:找到了不为空的桶
			{
				_node = _pht->_tables[index];//指向该桶的第一个节点
			}
		}
		return *this;
	}

	//判断两个节点的指针是否相同
	bool operator!=(const Self& s) const
	{
		return _node != s._node;
	}

	//判断两个节点的指针是否相同
	bool operator==(const Self& s) const
	{
		return _node == s._node;
	}
};
}
