#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
using namespace std;
#if 1
//process函数含义:怪兽现在还剩hp血,还有times次可以砍,每次砍的伤害范围都在:[0,M]范围上,返回砍死的情况数
long process(int times, int M, int hp)
{
	if (times == 0)
	{
		//已经不能砍了,如果怪兽血量<=0 就是1种砍死的情况
		return hp <= 0 ? 1 : 0;
	}

	//case1:还能继续砍 && 怪兽已经死了
	//提前把怪兽打死,剩余砍的次数为time -> 此时递归展开的后面这些层,都能返回1.所以
	//相当返回方法数为:(M+1)^times次
	if (hp <= 0)
	{
		return (long)pow(M + 1, times);
	}

	//case2:还能继续砍 &&怪兽没死
	long ways = 0;
	//每次掉的血量在[0,M]枚举
	for (int i = 0; i <= M; i++)
	{
		ways += process(times - 1, M, hp - i);//M是固定的,剩余的血量:hp-i 砍的次数-1
	}
	return ways;//返回能砍死的情况数
}

double KillMonster(int N, int M, int K)
{
	//无效参数判断
	if (N < 1 || M < 1 || K < 1)
	{
		return -1;
	}
	double all = pow(M + 1, K);//总的情况数: (M+1)^k
	long kill = process(K, M, N);//杀死怪兽的情况数
	return (double)kill / all;//杀死怪兽的怪率
}

long process2(int times, int M, int hp, vector<vector<long>>& dp)
{
	if (dp[times][hp] != -1)
	{
		return dp[times][hp];
	}

	if (times == 0)
	{
		//已经不能砍了,如果怪兽血量<=0 就是1种砍死的情况
		dp[times][hp] =  hp <= 0 ? 1 : 0;
		return dp[times][hp];
	}

	//case1:还能继续砍 && 怪兽已经死了
	//提前把怪兽打死,剩余砍的次数为time -> 此时递归展开的后面这些层,都能返回1.所以
	//相当返回方法数为:(M+1)^times次
	if (hp <= 0)
	{
		dp[times][hp] = (long)pow(M + 1, times);
		return dp[times][hp];
	}

	//case2:还能继续砍 &&怪兽没死
	long ways = 0;
	//每次掉的血量在[0,M]枚举
	for (int i = 0; i <= M; i++)
	{
		ways += process(times - 1, M, hp - i);//M是固定的,剩余的血量:hp-i 砍的次数-1
	}
	dp[times][hp] =  ways;//返回能砍死的情况数
	return dp[times][hp];
}
double KillMonster2(int N, int M, int K)
{
	//无效参数判断
	if (N < 1 || M < 1 || K < 1)
	{
		return -1;
	}
	double all = pow(M + 1, K);//总的情况数: (M+1)^k
	//开辟一张 (K+1)*(N+1)的二维表,最初初始化为-1,表示(time,hp)这个过程没有计算过
	vector<vector<long>> dp(K + 1, vector<long>(N + 1,-1));
	process2(K, M, N, dp);

	//dp[times][hp]含义: 还有times刀要砍, 怪兽剩余血量为hp, 能砍死怪兽的情况数
	long kill = dp[K][N];
	return (double)kill / all;//杀死怪兽的怪率
}

double KillMonsterDp(int N, int M, int K)
{
	//无效参数判断
	if (N < 1 || M < 1 || K < 1)
	{
		return -1;
	}
	double all = pow(M + 1, K);//总的情况数: (M+1)^k
	//开辟一张 (K+1)*(N+1)的二维表
	//dp[times][hp]含义: 还有times刀要砍, 怪兽剩余血量为hp, 能砍死怪兽的情况数
	vector<vector<long>> dp(K + 1, vector<long>(N + 1));

	//填表: dp[剩余要砍的次数][怪兽剩余血量]
	dp[0][0] = 1; //hp=0的时候,只有0滴血的时候才是一种方法, 第一行只有0位置的值是1,其他是0
	//从第一行开始上往下填
	for (int times = 1; times <= K; times++)
	{
		//此时还有times刀可以砍,但是怪兽hp已经为0, 砍死的情况数为:(M+1)^times
		dp[times][0] = (long)pow(M + 1, times);

		
		for (int hp = 1; hp <= N; hp++)
		{
			long ways = 0;
			//求每一个格子(times,hp)都需要通过一个for循环
			for (int i = 0; i <= M; i++) //M:打击的伤害范围:0~M
			{
				//hp-i可能越界,如果不越界,就获取真实格子的值
				if (hp - i > 0)  
				{
					ways += dp[times - 1][hp - i];
				}
				else if(hp -i == 0)
				{
					ways += dp[times - 1][0];
				}
				else
				{
					ways += (long)pow(M + 1, times - 1);
				}
			}
			dp[times][hp] = ways;
		}
	}
	long kill = dp[K][N];
	return (double)kill / all;//杀死怪兽的怪率
}

int main()
{
	cout << KillMonster(2,3,5) << endl;
	cout << KillMonster2(2, 3, 5) << endl;
	cout << KillMonsterDp(2, 3, 5) << endl;
	return 0;
}
#endif

