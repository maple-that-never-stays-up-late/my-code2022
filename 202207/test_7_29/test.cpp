#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
using namespace std;

#if 0
double KillMonsterDp(int N, int M, int K)
{
	//无效参数判断
	if (N < 1 || M < 1 || K < 1)
	{
		return -1;
	}
	double all = pow(M + 1, K);//总的情况数: (M+1)^k
	//开辟一张 (K+1)*(N+1)的二维表
	//dp[times][hp]含义: 还有times刀要砍, 怪兽剩余血量为hp, 能砍死怪兽的情况数
	vector<vector<long>> dp(K + 1, vector<long>(N + 1));

	//填表: dp[剩余要砍的次数][怪兽剩余血量]
	dp[0][0] = 1; //hp=0的时候,只有0滴血的时候才是一种方法, 第一行只有0位置的值是1,其他是0
	//从第一行开始上往下填
	for (int times = 1; times <= K; times++)
	{
		//此时还有times刀可以砍,但是怪兽hp已经为0, 砍死的情况数为:(M+1)^times
		dp[times][0] = (long)pow(M + 1, times);


		for (int hp = 1; hp <= N; hp++)
		{
			long ways = 0;
			//求每一个格子(times,hp)都需要通过一个for循环
			for (int i = 0; i <= M; i++) //M:打击的伤害范围:0~M
			{
				//hp-i可能越界,如果不越界,就获取真实格子的值
				if (hp - i >= 0)
				{
					ways += dp[times - 1][hp - i];
				}
				else //越界了,相当于剩余血量<=0 ,
				{
					//剩余砍的次数为times-1次,直接获得砍死的方法数为(M+1)^(times-1)
					ways += (long)pow(M + 1, times - 1);
				}
			}
			dp[times][hp] = ways;
		}
	}
	long kill = dp[K][N];
	return (double)kill / all;//杀死怪兽的怪率
}
double KillMonsterDp2(int N, int M, int K)
{
	//无效参数判断
	if (N < 1 || M < 1 || K < 1)
	{
		return -1;
	}
	double all = pow(M + 1, K);//总的情况数: (M+1)^k
	//开辟一张 (K+1)*(N+1)的二维表
	//dp[times][hp]含义: 还有times刀要砍, 怪兽剩余血量为hp, 能砍死怪兽的情况数
	vector<vector<long>> dp(K + 1, vector<long>(N + 1));

	//填表: dp[剩余要砍的次数][怪兽剩余血量]
	dp[0][0] = 1; //hp=0的时候,只有0滴血的时候才是一种方法, 第一行只有0位置的值是1,其他是0
	//从第一行开始上往下填
	for (int times = 1; times <= K; times++)
	{
		//此时还有times刀可以砍,但是怪兽hp已经为0, 砍死的情况数为:(M+1)^times
		dp[times][0] = (long)pow(M + 1, times);

		for (int hp = 1; hp <= N; hp++)
		{
			//i:times j:hp
			//dp[i][j] = dp[i][j-1] + dp[i-1][j] -dp[i-1][j-1-M]  前两项的下标肯定不越界,可以直接加
			dp[times][hp] = dp[times][hp - 1] + dp[times - 1][hp];

			if (hp - 1 - M >= 0)
			{
				dp[times][hp] -= dp[times - 1][hp - 1 - M];//如果下标不越界就直接在表中拿值
			}
			else
			{
				dp[times][hp] -= pow(M + 1, times - 1);//否则用公式代替
			}
		}
	}
	long kill = dp[K][N];
	return (double)kill / all;//杀死怪兽的怪率
}
int main()
{
	cout << KillMonsterDp2(2, 3, 4) << endl;
	cout << KillMonsterDp(2, 3, 4) << endl;
	return 0;
}
#endif 


#if 0
//process递归函数含义:
// arr[index...]往后的面值，每种面值张数自由选择，
// 要搞出rest这么多钱, 返回最少需要的张数
// 拿系统最大值INT_MAX标记怎么都搞定不了
int process1(vector<int>& arr, int index, int rest)
{
	if (index == arr.size()) //不能继续选择了
	{
		//如果要搞出的钱rest=0,此时不需要花费张数,返回0,否则就不可能搞出rest了
		return rest == 0 ? 0 : INT_MAX;
	}
	int ans = INT_MAX;
	//当前面值的钱从0张开始试,但是使用的张数select*当前的面值 arr[index] 不能超过现在要搞出的钱rest
	for (int select = 0; select * arr[index] <= rest; select++)
	{
		//使用select张当前位置的面值,去index+1位置搞定rest - select * arr[index]
		//如果能搞定rest-select*arr[index],就返回搞定它最少需要的张数next
		//如果搞不定:next就是系统最大值
		int next = process1(arr, index + 1, rest - select * arr[index]);
		if (next != INT_MAX) //搞定了
		{
			//此时方案下需要的张数为: select + next (使用当前面值的张数+后序选择的张数)
			//和之前的答案做比较,返回最小的
			ans = min(ans, select + next);
		}
	}
	return ans;//返回构成rest的最小张数
}
int minCoins1(vector<int> arr, int aim)
{
	if (arr.size() == 0) return INT_MAX;
	return process1(arr, 0, aim);//返回从0位置开始往后任意选择,搞定aim的最少张数
}



int process2(vector<int>& arr, int index, int rest, vector<vector<int>>& dp)
{
	if (dp[index][rest] != -1) //当前过程计算过,直接拿值
	{
		return dp[index][rest];
	}

	if (index == arr.size()) //不能继续选择了
	{
		//如果要搞出的钱rest=0,此时不需要花费张数,返回0,否则就不可能搞出rest了
		dp[index][rest] = rest == 0 ? 0 : INT_MAX;
		return dp[index][rest];
	}
	int ans = INT_MAX;
	//当前面值的钱从0张开始试,但是使用的张数select*当前的面值 arr[index] 不能超过现在要搞出的钱rest
	for (int select = 0; select * arr[index] <= rest; select++)
	{
		//使用select张当前位置的面值,去index+1位置搞定rest - select * arr[index]
		//如果能搞定rest-select*arr[index],就返回搞定它最少需要的张数next
		//如果搞不定:next就是系统最大值
		int next = process1(arr, index + 1, rest - select * arr[index]);
		if (next != INT_MAX) //搞定了
		{
			//此时方案下需要的张数为: select + next (使用当前面值的张数+后序选择的张数)
			//和之前的答案做比较,返回最小的
			ans = min(ans, select + next);
		}
	}
	dp[index][rest] =  ans;//返回构成rest的最小张数
	return dp[index][rest];
}
int minCoins2(vector<int> arr, int aim)
{
	if (arr.size() == 0) return INT_MAX;
	int N = arr.size();
	vector<vector<int>> dp(N + 1, vector<int>(aim + 1,-1));//一张(N+1)*(aim+1)的二维表,初始化为-1
	process2(arr, 0, aim,dp);
	return dp[0][aim];
}

int minCoins3(vector<int> arr, int aim)
{
	if (arr.size() == 0) return INT_MAX;
	int N = arr.size();
	vector<vector<int>> dp(N + 1, vector<int>(aim + 1));//一张(N+1)*(aim+1)的二维表

	dp[N][0] = 0;
	for (int rest = 1; rest <= aim; rest++)
	{
		dp[N][rest] = INT_MAX;
	}

	for (int index = N - 1; index >= 0; index--)
	{
		for (int rest = 0; rest <= aim; rest++)
		{
			//每一个格子都需要一个for循环求
			int ans = INT_MAX;
			for (int select = 0; select * arr[index] <= rest; select++)
			{
				int next = dp[index +1 ][rest - select * arr[index]];
				if (next != INT_MAX)
				{
					ans = min(ans, next + select);
				}
			}
			//填表
			dp[index][rest] = ans;
		}
	}
	return dp[0][aim];
}


int minCoins4(vector<int> arr, int aim)
{
	if (arr.size() == 0) return INT_MAX;
	int N = arr.size();
	vector<vector<int>> dp(N + 1, vector<int>(aim + 1));//一张(N+1)*(aim+1)的二维表

	dp[N][0] = 0;
	for (int rest = 1; rest <= aim; rest++)
	{
		dp[N][rest] = INT_MAX;
	}

	for (int index = N - 1; index >= 0; index--)
	{
		for (int rest = 0; rest <= aim; rest++)
		{
			//和下标的格子 和 左边rest-arr[index]的格子+1  pk取较小值
			int p1 = dp[index + 1][rest];//下标格子
			int p2 = INT_MAX;
			//要保证不越界 && 并且这种可能性要有效
			if (rest - arr[index] >= 0 && dp[index][rest - arr[index]] != INT_MAX)
			{
				 p2 = dp[index][rest - arr[index]] + 1 ;	
			}
			//取较小值
			dp[index][rest] = min(p1, p2);
		}
	}
	return dp[0][aim];
}
int main()
{
	vector<int> v{ 1,  2 ,2,1,4};
	cout << minCoins1(v, 4) << endl;
	cout << minCoins2(v, 4) << endl;
	cout << minCoins3(v, 4) << endl;
	cout << minCoins4(v, 4) << endl;
	return 0;
}
#endif


#if 1


int main()
{

	return 0;
}
#endif