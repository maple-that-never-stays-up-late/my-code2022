﻿#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<string>
#include<list>
#include<vector>
using namespace std;

//125. 验证回文串  https://leetcode.cn/problems/valid-palindrome/
class Solution {
public:
    bool isPalindrome(string s) {
        if (s.empty()) return true;
        int slow = 0;
        int fast = 0;
        while (fast < s.size())
        {
            if (s[fast] >= 48 && s[fast] <= 57)//0~9
            {
                s[slow++] = s[fast++];
            }
            else if (s[fast] >= 65 && s[fast] <= 90) //大写字母->转小写
            {
                s[fast] += 32;
                s[slow++] = s[fast++];
            }
            else if (s[fast] >= 97 && s[fast] <= 122)//小写字母
            {
                s[slow++] = s[fast++];
            }
            else
            {
                fast++;
            }
        }
        //判断[0,slow-1]是否回文
        int right = slow - 1;
        int left = 0;
        while (left <= right)
        {
            if (s[left++] != s[right--]) return false;
        }
        return true;
    }
};

int test1()
{
    vector<int>array;
    array.push_back(100);
    array.push_back(300);
    array.push_back(300);
    array.push_back(300);
    array.push_back(300);
    array.push_back(500);
    vector<int>::iterator itor;
    //100 300 300 300 300 500
    for (itor = array.begin(); itor != array.end(); itor++)
    {
        if (*itor == 300)
        {
            itor = array.erase(itor);
        }
    }
    for (itor = array.begin(); itor != array.end(); itor++)
        cout << *itor << " ";
    return 0;
}
void  test2()
{
    int ar[] = { 1,2,3,4,5,6,7,8,9,10 };
    int n = sizeof(ar) / sizeof(int);
    vector<int> v(ar, ar + n);//迭代器构造
    cout << v.size() << ":" << v.capacity() << endl;//10  10
    v.reserve(100);
    v.resize(20);
    cout << v.size() << ":" << v.capacity() << endl;//20 100
    v.reserve(50);
    v.resize(5);
    cout << v.size() << ":" << v.capacity() << endl;//5 100
}

int main()
{
    int array[] = { 1, 2, 3, 4, 0, 5, 6, 7, 8, 9 };
    int n = sizeof(array) / sizeof(int);
    list<int> mylist(array, array + n);
    auto it = mylist.begin();
    while (it != mylist.end())
    {
        if (*it != 0)
            cout << *it << " ";
        else
            it = mylist.erase(it);
        ++it;
    }
    return 0;
}