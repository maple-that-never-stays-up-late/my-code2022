﻿#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
using namespace std;
#if 0
struct Cmp
{
    bool operator()(const pair<string, int>& left, const pair<string, int>& right)
    {
        // 次数从大到小排序  如果次数相同，再按照单词字典序排序
        if (left.second == right.second) return left.first + right.first < right.first + left.first;
        else return left.second > right.second;;
    }
};
int main()
{
    string str;
    getline(cin, str);
    map<string, int> map;

    //1.分割单词
    string tmp;
    for (int i = 0; i < str.size(); i++)
    {
        cout << str[i] << endl;
        if (str[i] == ' ' || str[i] == ',' || str[i] == '.')
        {
            map[tmp]++;
            tmp.clear();
        }
        else
        {
            tmp += tolower(str[i]);//转为小写
        }
    }
    for (auto& kv : map)
    {
        cout << kv.first << ":" << kv.second << endl;
    }

    //按照出现次数降序排序
    vector<pair<string, int>> v;
    for (auto& kv : map) v.push_back(kv);
    sort(v.begin(), v.end(), Cmp());
    for (auto& kv : v)
    {
        cout << kv.first << ":" << kv.second << endl;
    }
    return 0;
}
#endif


 