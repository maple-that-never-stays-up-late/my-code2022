#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
using namespace std;

//int main()
//{
//    const int a = 10;
//    int* p = const_cast<int*>(&a);
//    *p = 100;
//    cout << a << endl;  //10
//    cout << *p << endl; //100
//    cout << &a << endl;//00D9FE08
//    cout << p << endl;//00D9FE08
//}
//int main()
//{
//	//例子1:
//	int i = 1;
//	int* p = nullptr;
//	p = reinterpret_cast<int*>(i);
//	cout << p << endl;//00000001
//
//	//例子2:
//	double d = 12.34;
//	int* pd = reinterpret_cast<int*>(&d);
//}
//typedef void(*FUNC)();	//FUNC是函数指针,指向的函数返回值是void,没有参数
//int DoSomething(int i)
//{
//	cout << "DoSomething" << i << endl;
//	return 0;
//}
//
//int main()
//{
//	FUNC f = reinterpret_cast<FUNC>(DoSomething);
//	f();
//	return 0;
//}
//volatile const int a = 10; //a是全局变量
//int b = 0;
//int main()
//{
//	
//	int* p = const_cast<int*>(&a); //去掉了const属性
//	const char* str = "hello";
//	return 0;
//}
//class A
//{
//	virtual void f() {}
//public:
//};
//class B : public A
//{
//public:
//	int _b = 0;
//};
//
////如果pa是指向父类对象，那么不做任何处理
////如果pa是指向子类对象，那么请转回子类，并访问子类对象中_b成员
//
////父类指针可以接收子类指针 和 父类指针
//void func(A* pa)
//{
//
//	// dynamic_cast<B*>(pa)--如果pa指向的父类对象，那么则转换不成功，返回nullptr
//						  // 如果pa指向的子类对象，那么则转换成功，返回指向对象指针
//
//	B* pb1 = dynamic_cast<B*>(pa); 
//	if (pb1 == nullptr)
//	{
//		cout << "转换失败" << endl;
//	}
//	else
//	{
//		cout << "pb1:" << pb1 << endl;
//		pb1->_b++;
//	}
//	
//	//dynamic_cast会先检查是否能转换成功，能成功则转换，不能则返回nullptr
//	//static_cast会直接转化成功,认为是相近类型,  但是并不会检查
//	/*
//	B* pb1 = static_cast<B*>(pa);
//	B* pb2 = dynamic_cast<B*>(pa);
//
//	cout << "pb1:" << pb1 << endl;
//	cout << "pb2:" << pb2 << endl;
//	*/
//}
//int main()
//{
//	A aa;
//	B bb;
//	func(&aa);
//	func(&bb);
//
//	//父类对象不能赋值给子类对象!!!
//	//bb = reinterpret_cast<B>(aa); // reinterpret_cast也不允许
//}

//int func()
//{
//	throw 3.14;//抛出异常
//
//	return 1;//正常返回
//}
//void test()
//{
//	//try:里面写可能发生异常的代码
//	try
//	{
//		func();
//	}
//	//catch:捕获对应类型的异常
//	catch (char ch){ cout << ch << endl; }
//	catch (double d) { cout << d << endl; }
//}
//int main()
//{
//	test();//3.14
//	return 0;
//}

/*
double Division(int a, int b)
{
	// 当b == 0时抛出异常,除0错误！
	if (b == 0)
		throw "Division by zero condition!"; //throw一个对象 这个对象可以是任意类型！
	else
		return ((double)a / (double)b);
}
void Func1()
{
	try
	{
		int len, time;
		cin >> len >> time;
		cout << Division(len, time) << endl;
	}
	catch (int errid)
	{
		cout << errid << endl;
	}
}

int main()
{
	try
	{
		Func1();
	}
	catch (const char* errmsg)
	{
		cout << errmsg << endl;
	}
	catch (int errid)
	{
		cout << errid << endl;
	}
	catch (...)
	{
		cout << "unkown exception" << endl;
	}

	return 0;
}
*/

//double Division(int a, int b)
//{
//	// 当b == 0时抛出异常
//	if (b == 0)
//	{
//		throw "Division by zero condition!";
//	}
//	return (double)a / (double)b;
//}
//
//void Func()
//{
//	// 这里可以看到如果发生除0错误抛出异常，另外下面的array没有得到释放。
//	// 所以这里捕获异常后并不处理异常，异常还是交给外面处理，这里捕获了再重新抛出去。
//	int* array = new int[10];
//	int len, time;
//	cin >> len >> time;
//
//	try
//	{
//		cout << Division(len, time) << endl;
//	}
//	catch (...)  // 拦截异常，不是要处理异常，而是要正常释放资源
//	{
//		cout << "delete []" << array << endl;
//		delete[] array;
//		throw;  // 捕获到什么对象就重新抛出什么对象
//	}
//
//	cout << "delete []" << array << endl;
//	delete[] array;
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (const char* errmsg)
//	{
//		cout << errmsg << endl;
//	}
//
//	return 0;
//}


//#include<time.h>
//#include <thread>
//// 服务器开发中通常使用的异常继承体系
//class Exception
//{
//public:
//	Exception(const string& errmsg, int id)
//		:_errmsg(errmsg)
//		, _id(id)
//	{}
//
//	virtual string what() const  //返回发生了什么错误
//	{
//		return _errmsg;
//	}
//protected:
//	string _errmsg;//错误信息-是什么错误
//	int _id;//错误编号-区分不同的错误
//};
//
//
//class SqlException : public Exception
//{
//public:
//	SqlException(const string& errmsg, int id, const string& sql)
//		:Exception(errmsg, id)
//		, _sql(sql)
//	{}
//
//	virtual string what() const
//	{
//		string str = "SqlException:";
//		str += _errmsg;
//		str += "->";
//		str += _sql;
//
//		return str;
//	}
//
//private:
//	const string _sql;//记录出错了的 sql语句
//};
//
//class CacheException : public Exception
//{
//public:
//	CacheException(const string& errmsg, int id)
//		:Exception(errmsg, id)//调用父类的构造函数
//	{}
//
//	virtual string what() const
//	{
//		string str = "CacheException:";
//		str += _errmsg;
//		return str;
//	}
//};
//
//class HttpServerException : public Exception
//{
//public:
//	HttpServerException(const string& errmsg, int id, const string& type)
//		:Exception(errmsg, id)//初始化父类的构造函数
//		, _type(type)
//	{}
//
//	virtual string what() const
//	{
//		string str = "HttpServerException:";
//		str += _type;
//		str += ":";
//		str += _errmsg;
//
//		return str;
//	}
//
//private:
//	const string _type;//方法  请求类型是什么
//};
//
//
//void SQLMgr()
//{
//	srand(time(0));
//	if (rand() % 7 == 0)
//	{
//		throw SqlException("权限不足", 100, "select * from name = '张三'");//没有权限查张三的信息
//	}
//
//	//throw "xxxxxx"; //未知异常  
//}
//
//void CacheMgr()
//{
//	srand(time(0));
//	if (rand() % 5 == 0)
//	{
//		throw CacheException("权限不足", 100);
//	}
//	else if (rand() % 6 == 0)
//	{
//		throw CacheException("数据不存在", 101);
//	}
//
//	SQLMgr();
//}
//
//void HttpServer()
//{
//	srand(time(0));
//	if (rand() % 3 == 0)
//	{
//		//抛子类对象,由父类捕获!
//		throw HttpServerException("请求资源不存在", 100, "get");
//	}
//	else if (rand() % 4 == 0)
//	{
//		throw HttpServerException("权限不足", 101, "post");
//	}
//
//	CacheMgr();
//}
//
//void ServerStart()
//{
//	while (1)
//	{
//		this_thread::sleep_for(chrono::seconds(1));//睡眠1s , 1s跑一次
//
//		try {
//			HttpServer();
//		}
//		catch (const Exception& e) // 用父类对象捕获抛出的子类对象  此时e是父类的引用
//		{
//			//多态  条件:1.虚函数的重写 2.父类的指针/引用去调用重写的虚函数
//			//抛出的是谁的,就调用的是谁的what函数
//			cout << e.what() << endl;//到底发生了什么！
//		}
//		catch (...)  //未知异常,有人偷偷抛了其它异常！
//		{
//			cout << "Unkown Exception" << endl;
//		}
//	}
//}
//
//int main()
//{
//	ServerStart();
//
//	return 0;
//}

// 使用RAII思想设计的SmartPtr类
template<class T>
class SmartPtr
{
public:
	SmartPtr(T* ptr = nullptr)
		:_ptr(ptr)
	{}
	//重载operator->和operator* 像指针一样使用
	T* operator->()
	{
		return _ptr;
	}
	T& operator*()
	{
		return *_ptr;
	}
	~SmartPtr()
	{
		if (_ptr)
		{
			cout << "delele" << _ptr << endl;
			delete _ptr;
		}
	}
private:
	T* _ptr;
};

struct Date
{
	int _year;
	int _month;
	int _day;
};
int div()
{
	int a, b;
	cin >> a >> b;
	if (b == 0)
		throw invalid_argument("除0错误");	

	return a / b;
}
void func()
{
	SmartPtr<int> sp1(new int);
	SmartPtr<Date> sp2(new Date);

	*sp1 = 10;
	cout << *sp1 << endl;//10
	sp2->_day = 1;
	sp2->_year = 1;
	sp2->_month= 1;
	cout << sp2->_year << "-" << sp2->_month << "-" << sp2->_day << endl;

	cout << div() << endl;//无论是否抛异常,都能正确释放空间资源
}
//int main()
//{
//	try
//	{
//		func();
//	}
//	catch(const exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}  
//int main()
//{
//	SmartPtr<int> sp1(new int);
//	//拷贝构造
//	SmartPtr<int> sp3;
//	sp3 = sp1;
//	//赋值重载
//	SmartPtr<int> sp2  = sp1; 
//
//	return 0;
//}
#include<memory>
//int main()
//{
//	auto_ptr<int> sp1(new int);
//	auto_ptr<int> sp2(sp1); // 管理权转移,sp1就变成空指针了
//	*sp2 = 10;
//	cout << *sp2 << endl;//10
//	cout << *sp1 << endl;//崩溃
//	return 0;
//}
//namespace Mango	//为了和库里面的做区分
//{
//template<class T>
//class auto_ptr
//{
//public:
//	auto_ptr(T* ptr =nullptr)
//		_ptr(ptr)
//	{}
//	
//	//sp1(sp2)
//	auto_ptr(const auto_ptr<T>& sp)
//		:_ptr(sp._ptr)
//	{
//		sp._ptr = nullptr;//管理权转移,原来的对象悬空
//	}
//
//	//sp1 = sp2
//	auto_ptr<T>& operator=(const auto_ptr<T>& sp)
//	{
//		if (this != &sp)//防止sp1 = sp1自己给自己赋值的情况
//		{
//			//1.释放原来的空间
//			if (_ptr) delete _ptr;
//			//2.指向新空间
//			_ptr = sp._ptr;
//			//3.原来的对象悬空
//			sp._ptr = nullptr;
//		}
//		return *this;
//	}
//
//	// 像指针一样使用
//	T& operator*()
//	{
//		return *_ptr;
//	}
//
//	T* operator->()
//	{
//		return _ptr;
//	}
//
//	~auto_ptr()
//	{
//		if (_ptr)
//		{
//			cout << "delete" << _ptr << endl;
//			delete _ptr;
//		}
//	}
//private:
//	T* _ptr;//管理一个指针
//};
//}

//#include<memory>
//int main()
//{
//	unique_ptr<int> sp1(new int);
//	unique_ptr<int> sp2(sp1);
//	unique_ptr<int> sp3;
//	sp3 = sp1;
//	return 0;
//}

// unique_ptr/scoped_ptr   守卫指针！
// 原理：简单粗暴 -- 防拷贝
namespace Mango
{
	template<class T>
	class unique_ptr
	{
	public:
		unique_ptr(T* ptr)
			:_ptr(ptr)
		{}

		~unique_ptr()
		{
			if (_ptr)
			{
				cout << "delete:" << _ptr << endl;
				delete _ptr;
			}
		}

		// 像指针一样使用
		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		//方法1:只声明不实现+设计成私有   -C++98
		//方法2:用delete修饰	-C++11
		unique_ptr<T>& operator=(const unique_ptr<T>& sp) = delete;
		unique_ptr(const unique_ptr<T>& sp) = delete; //防止拷贝！

	private:
		T* _ptr;
	};
}

//int main()
//{
//	shared_ptr<int> sp1(new int(0));
//	shared_ptr<int> sp2 = sp1;
//	*sp2 = 101;
//
//	cout << *sp1 << endl;//101
//
//	shared_ptr<int> sp3(new int(20));
//	sp1 = sp3;
//	cout << *sp1 << " " << *sp2 << " " << *sp3;//20 101 20
//	return 0;
//}

namespace Mango
{
template<class T>
class shared_ptr
{
public:
	shared_ptr(const T* ptr)
		:_ptr(ptr)
		,_pRefCount(new int(1)) //构造的时候,该资源只被一个对象共享
	{}

	//拷贝构造 sp1(sp2)
	shared_ptr(const shared_ptr<T>& sp)	
		:_ptr(sp._ptr)//指向同一块资源
		,_pRefCount(sp._pRefCount)//指向同一个计数空间
	{
		//_pRefCount:计数空间* _pRefCount : 多少个对象共享这块空间

		(*_pRefCount)++; //多了一个共享该资源的对象,计数++, 
	}

	//赋值 sp1 = sp2
	shared_ptr<T>& operator=(const shared_ptr<T>& sp)
	{
		if (_ptr != sp._ptr)////判断是不是管理的同一份资源
		{
			//1.现在_ptr要管理另外一份资源了,先处理原来资源的计数问题
			//如果--之后为0,(注意是前置--) 说明_ptr是最后一个指向该资源的对象
			//现在_ptr要指向新资源了,释放原来的资源和计数空间
			if (--(*_pRefCount) == 0)
			{
				delete _ptr;
				delete _pRefCount;
			}
			//2.赋值
			_ptr = sp._ptr;//指向新资源
			_pRefCount = sp._pRefCount;//指向同一个计数空间
			(*_pRefCount)++;//多了一个指向该资源的对象, 计数++
		}
		return *this;
	}
	~shared_ptr()
	{
		//计数--,如果减到0,说明自己是最后一个管理该资源的对象
		//就释放这块资源和计数空间
		if (--(*_pRefCount) == 0 && _ptr)
		{
			cout << "delete:" << _ptr << endl;
			delete _ptr;
			delete _pRefCount;

			_ptr = nullptr;
			_pRefCount = nullptr;
		}
	}
	//像指针一样使用
	T& operator*()
	{
		return *_ptr;
	}

	T* operator->()
	{
		return _ptr;
	}

private:
	T* _ptr;
	//引用计数,多少个指针指向这块空间,就说明该份资源被几个对象共享
	int* _pRefCount;
};
}