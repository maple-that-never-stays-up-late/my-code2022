#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
using namespace std;
#if 1
//此时认为数组范围为:[0,R]
//即在[0,R]范围内,求>=value的最左侧位置（下标）  二分
int nearestIndex(vector<int> arr, int R, int value)
{
	int L = 0;
	int index = R;//记录>=value的下标,注意:不能初始化为-1
	while (L <= R)
	{
		int mid = L + ((R - L) >> 1);
		if (arr[mid] >= value)
		{
			index = mid;//记录此时的结果
			R = mid - 1;
		}
		else
		{
			L = mid + 1;
		}
	}
	return index;
}

int maxPoint(vector<int> arr, int L)	//绳子长度为L
{
	int res = 1;//初始化为1！！！ 绳子最少覆盖1个位置(覆盖自己)
	//每一个点作为绳子的末尾点,看最多覆盖多少个位置
	for (int i = 0; i < arr.size(); i++)
	{
		int nearest = nearestIndex(arr, i, arr[i] - L);//在[0,i]上,求出>= arr[i] -L 最左侧的位置
		//此时以i位置为结尾的绳子,最多能覆盖的点的范围就是:[nearset,i]
		res = max(res, i - nearest + 1);
	}
	return res;
}

int maxPoint2(vector<int> arr, int L)	//绳子长度为L
{
	int left = 0;
	int right = 0;
	int n = arr.size();
	int ans = 0;
	for (; left < n; left++)//枚举以每一个位置开头最多能覆盖的点
	{
		//left(开头)固定的情况下,right往右到不能再往右
		//right不能越界 && arr[right]到arr[left]的距离<=绳子长度 ,right就继续往右扩
		while (right < n && arr[right] - arr[left] <= L)
		{
			right++;
		}
		//来到这里,此时right就来到了固定left的情况下,不能再往右的位置
		//此时left为绳子开头的情况下,能覆盖的范围是[left,right) 覆盖的元素个数为right - left
		ans = max(ans, right - left);
	}
	return ans;
}
int main()
{
	cout << maxPoint(vector<int>{1, 3, 4, 5,6,7, 13, 16, 17,21}, 4) << endl;
	cout << maxPoint2(vector<int>{1, 3, 4, 5,6,7, 13, 16, 17,21}, 4) << endl;
	return 0;
}
#endif