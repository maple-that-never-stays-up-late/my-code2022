#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
using namespace std;
#if 0
int tableSizeFor(int n)
{
	n--;

	//下述代码:将n最高位的比特位1之后的比特位全部置1 (后面填满1)
	//无符号右移  
	n |= (unsigned int)n >> 1;
	n |= (unsigned int)n >> 2;
	n |= (unsigned int)n >> 4;
	n |= (unsigned int)n >> 8;
	n |= (unsigned int)n >> 16;

	return (n < 0) ? 1 : n + 1;
}

int main()
{
	int n = -2;
	//无符号右移  
	n |= (unsigned int)n >> 1;
	n |= (unsigned int)n >>  2;
	n |= (unsigned int)n >>  4;
	n |= (unsigned int)n >>  8;
	n |= (unsigned int)n >>  16;
	cout << n << endl;
	return 0;
}
#endif

#if 0
int minSteps1(string s)
{
	if (s.size() == 0) return 0;
	int step1 = 0;//记录所有的字符G移动到左边的代价和
	int gi = 0;//字符G应该放的位置
	//遍历字符串,G去左边的代价
	for (int i = 0; i < s.size(); i++)
	{
		if (s[i] == 'G')
		{
			step1 += i - gi;//当前G字符去左边gi位置的代价为:i-gi
			gi++;
		}
	}
	int step2 = 0;//记录所有的字符B移动到左边的代价和
	int bi = 0;
	//遍历字符串,B去左边的代价
	for (int i = 0; i < s.size(); i++) 
	{
		if (s[i] == 'B') 
		{
			step2 += i - bi;//当前B字符去左边gi位置的代价为:i-gi
			bi++;
		}
	}
	return min(step1, step2);
}
int minSteps2(string s)
{
	if (s.size() == 0)	return 0;
	int step1 = 0;
	int step2 = 0;
	int gi = 0;
	int bi = 0;
	for (int i = 0; i < s.size(); i++)
	{
		if(s[i]=='G')
		{
			step1 += i - (gi++);
		}
		else//s[i]=='B'
		{
			step2 += i - (bi++);
		}
	}
	return min(step1, step2);
}

int process(vector<vector<int>>& m, int i, int j, vector<vector<int>>& dp)
{
	if (dp[i][j] != -1)	//当前过程计算过
	{
		return dp[i][j];
	}
	int up = i > 0 && m[i][j] < m[i - 1][j] ? process(m, i - 1, j, dp) : 0;
	int down = i < (m.size()-1) && m[i][j] < m[i + 1][j] ? process(m, i + 1, j, dp) : 0;
	int left = j > 0 && m[i][j] < m[i][j - 1] ? process(m, i, j - 1, dp) : 0;
	int right = j < (m[0].size()-1) && m[i][j] < m[i][j + 1] ? process(m, i, j + 1, dp) : 0;

	//up,down,left,right是我后序走的递增链,只代表后序往哪里走构成的链长度
	//最后还要加上自己这个数的位置,所以要+1
	dp[i][j] = max(max(up, down), max(left, right)) + 1;
	return dp[i][j];
}
int longestIncreasingPath(vector<vector<int>>& matrix)
{
	int ans = 0;//记录最长递增链长度
	int row = matrix.size();
	int col = matrix[0].size();
	vector<vector<int>> dp(row, vector<int>(col,-1));//row*col的二维表,最初初始化为-1

	//每个位置出发都试一遍
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			int cur = process(matrix, i, j,dp);//从当前位置开始走,返回走出来的最长递增链长度
			ans = max(ans, cur);
		}
	}
	return ans;
}


int main()
{
	vector<vector<int>> vv{ {1,2} };
	cout << longestIncreasingPath(vv) << endl;
	return 0;
}
#endif
#include<unordered_map>
#if 0
////process递归函数含义:从arr[index...]往后任意使用数字, 返回最后搞出rest这个数的方法数是多少！
//int process(vector<int>& arr, int index, int rest)
//{
//	if (index == arr.size())//没数可以选择了
//	{
//		return rest == 0 ? 1 : 0;//如果正好凑出rest,就是一种方法,否则不是
//	}
//	//第一种选择:当前数前面放-号,往后选择要搞出的是:rest+arr[index] 
//	//第二种选择:当前数前面放+号,往后选择要搞出的是:rest-arr[index]
//	int p1 = process(arr, index + 1, rest - arr[index]);
//	int p2 = process(arr, index + 1, rest + arr[index]);
//	//返回两种选择的方法数之和
//	return p1 + p2;
//}
//int findTargetSumWays(vector<int>& arr, int sum)
//{
//	return process(arr, 0, sum);
//}

//process递归函数含义:从arr[index...]往后任意使用数字, 返回最后搞出rest这个数的方法数是多少！
//int process(vector<int>& arr, int index, int rest, unordered_map<int, unordered_map<int, int >>& dp)
//{
//	//如果dp表中以index为key的这个rest过程计算过了,就返回
//	if (dp.find(index) != dp.end() && dp[index].find(rest) != dp[index].end())
//	{
//		return dp[index][rest];//dp[index]:{rest,方法数}  dp[index][rest]:(index,rest)对应的方法数
//	}
//	if (index == arr.size())
//	{
//		return rest == 0 ? 1 : 0;
//	}
//	int p1 = process(arr, index + 1, rest - arr[index], dp);
//	int p2 = process(arr, index + 1, rest + arr[index], dp);
//	dp[index][rest] = p1 + p2;
//	return dp[index][rest];
//}
//int findTargetSumWays(vector<int>& arr, int target)
//{
//	unordered_map<int, unordered_map<int, int >> dp;
//	process(arr, 0, target,dp);
//	return dp[0][target];
//}

//返回在arr的数任意选择,搞出累加和为sum的方法数
int dp(vector<int>& arr, int sum)
{
	int n = arr.size();
	// dp[i][j]含义: nums[0...i]范围上, 有多少集合累加和是j
	vector<vector<int>> dp(n, vector<int>(sum + 1));
	// nums前缀长度为0的所有子集，有多少累加和是0？一个：空集
	dp[0][0] = 1;//如果第一个元素为0.则dp[0][0]应该为2)，其他层的根据第一层改变

	//第0行中dp[0][j]  只有arr[0]为j,才能构成累加和为j
	for (int j = 0; j <= sum; j++) 
	{
		//dp[0][j]，看第一个元素的大小情况，进行赋值1(如果第一个元素为0.则dp[0][0]应该为2)
		if (arr[0] == j) dp[0][j] += 1;
	}

	//从第1行填到n-1行  index的范围:[0,n-1]
	for (int i = 1; i < n; i++)
	{
		for (int j = 0; j <= sum; j++)
		{
			dp[i][j] = dp[i - 1][j];//case1:不使用i位置的数,转化为去[0,i-1]搞定j的方法数
			if (j - arr[i] >= 0) //不越界才有case2
			{
				dp[i][j] += dp[i - 1][j - arr[i]];//使用i位置的数,转化为去[0,i-1]搞定j-arr[i]的方法数
			}
		}
	}
	//递归含义是:返回arr[0...n-1]上,有多少子集目标和为sum
	//所以返回的是dp[n-1][sum]
	return dp[n - 1][sum];
}
int findTargetSumWays(vector<int>& arr, int target)
{
	int sum = 0;
	//1.求所有数累加和
	for (auto& x : arr) sum += x;
	//2.如果要搞出的数target>sum,那就一定搞不出了
	//3.如果sum和target的奇偶性不同,那么也搞不出
	if (target > sum || ((target & 1) ^ (sum & 1)) != 0)
	{
		return 0;
	}
	//4.求在arr的数任意选择,搞出累加和为(traget+sum)/2的方法 ->经典背包动态规划
	return dp(arr,(target + sum) >> 1);
}

int main()
{
	vector<int> v{ 1, 1, 1, 1, 1 };
	cout << findTargetSumWays( v, 3) << endl;
	return 0;
}
#endif