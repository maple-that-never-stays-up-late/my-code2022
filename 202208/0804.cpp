#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<unordered_map>
using namespace std;

#if 0
//CD62 设计有setAll功能的哈希表  https://www.nowcoder.com/practice/7c4559f138e74ceb9ba57d76fd169967
class MyMap
{
public:
	MyMap()
		:time(0), allValue(-1), SetAllTime(-1)
	{}

	void put(int key, int val)
	{
		pair<int, long> kv = make_pair(val, time);
		map[key] = kv;
		time++;
	}

	int get(int key)
	{
		if (!containsKey(key)) return -1;
		pair<int, long> kv = map[key];
		return kv.second > SetAllTime ? kv.first : allValue;
	}

	bool containsKey(int key)
	{
		return map.find(key) == map.end() ? false : true;
	}

	void setAll(int val)
	{	
		allValue = val;
		SetAllTime = time;
		time++;
	}
private:
	unordered_map<int, pair<int, long>> map;//key:值  value:{key对应的值,记录的时间}
	long time;//此时的时间
	int allValue;//记录SetAll时候的值
	long SetAllTime; //记录SetAll时候的时间
};
#endif

#if 0
//CD12 换钱的最少货币数  https ://www.nowcoder.com/practice/4e05294fc5aa4d4fa8eacef2e606e5a8
#include<iostream>
#include<vector>
#include<limits.h>
using namespace std;
int process(vector<int>& arr, int index, int rest)
{
	if (index == arr.size())
		return rest == 0 ? 1 : INT_MAX;
	int ans = INT_MAX;
	int ways = 0;
	for (int select = 0; select * arr[index] <= rest; select++)
	{
		int next = process(arr, index + 1, rest - select * arr[index]);
		if (next != INT_MAX) //搞定了
		{
			//此时方案下需要的张数为: select + next (使用当前面值的张数+后序选择的张数)
			//和之前的答案做比较,返回最小的
			ans = min(ans, select + next);
		}
	}
	return ans;
}
int main()
{
	vector<int> v;
	int n, target;
	cin >> n >> target;
	v.resize(n);
	for (int i = 0; i < n; i++)
	{
		cin >> v[i];
	}
	if (target == 0)
	{
		cout << "0" << endl;//一张都不选
	}
	else if (target < 0)
	{
		cout << "-1" << endl;
	}
	else
	{
		int ans = process(v, 0, target);
		if (ans == INT_MAX) cout << "-1" << endl;
		else cout << ans << endl;
	}
	return 0;
}
#endif

#include<algorithm>

#if 0
/*
给定数组hard和money，长度都为N，hard[i]表示i号工作的难度， money[i]表示i号工作的收入,给定数组ability，长度都为M

ability[j]表示j号人的能力，每一号工作都可以提供无数的岗位，难度和收入都一样 
但是人的能力必须>=这份工作的难度才能上班,返回一个长度为M的数组ans，ans[j]表示j号人能获得的最好收入
*/
struct Job
{
	int money;//收入
	int hard;//难度

	Job(int h, int m)
		:money(m), hard(h)
	{}
};

struct Cmp
{
	//难度由小到大,难度一样的,按money由大到小排序
	bool operator()(Job j1, Job j2)
	{
		if (j1.hard == j2.hard)
			return j1.money > j2.money;
		else
			return j1.hard < j2.hard;
	}
};

int process(vector<pair<int, int>>& workSalary, int cur)//在workSalary数组找 <= cur且离它最近的难度的工作下标
{
	int left = 0;
	int right = workSalary.size() - 1;
	int ans = -1;
	while (left <= right)
	{
		int mid = left +((right - left) >> 1);
		if (workSalary[mid].first <= cur)
		{
			ans = mid;
			left = mid + 1;
		}
		else
		{
			right = mid - 1;
		}
	}
	return ans;//找不到
}
vector<int> getMoneys(vector<Job> job,vector<int> ability)
{
	sort(job.begin(), job.end(), Cmp());
	//去重,难度一样的,只要收入大的
	unordered_map<int, int> map;//难度上升,报酬也上升

	//排完序之后,难度最小的组中薪资最大的在第一位 
	map.insert(make_pair(job[0].hard, job[0].money));
	Job pre = job[0];//记录上一份进入map的工作

	//job数组是按难度排完序的,难度相同时,收入大的在前面
	for (int i = 1; i < job.size(); i++)
	{
		//难度一样时,只要收入大的
		//难度上升,收入下降的不要

		//所以两个条件:当前工作的难度和上一个进表的工作不同 && 当前工作的收入>上一个进表工作的收入
		if (job[i].hard != pre.hard && job[i].money > pre.money)
		{
			pre = job[i];
			map.insert(make_pair(job[i].hard, job[i].money));
		}
	}

	//此时workSalary就是每个难度下的最好收入,并且难度有序,收入有序
	//难度上升,收入上升
	vector<pair<int,int>> workSalary(map.begin(), map.end());
	for (auto kv : workSalary) cout << kv.first << " " << kv.second << endl;;

	int n = ability.size();
	vector<int> ans(n);//有几个人,就记录每一个人的最好收入,最初初始化为-1
	
	for (int i = 0; i < ability.size(); i++)
	{
		int cur = ability[i];//当前这个人的能力
		int nearst = process(workSalary, cur);//在workSalary数组找<=cur且离它最近的工作下标
		if (nearst == -1)//说明cur这个人没有能力适应任何工作, 填0
		{
			ans[i] = 0;
		}
		else
		{
			ans[i] = workSalary[nearst].second;//否则这个人的收入就是难度离他最近的工作的收入
		}
	}
	return ans;
}

int main()
{
	vector<Job> v{ {7,13},{1,5},{1,9},{3,6} ,{3,13},{5,17} }; //{难度,收入}
	vector<int> ability{ 0,1,2,3,4,5,8,2 };
	vector<int> ans = getMoneys(v, ability);
	for (auto x : ans) cout << x << " ";
	return 0;
}

#endif


#if 0
/*
process递归函数的含义:从index往后的.....所有的司机，往A和B区域分配,A区域还有rest个名额
返回把index位置开始后面的司机分配完,并且最终A和B区域司机同样多的情况下,index及其往后的这些司机整体收入最大是多少
*/
int process(vector<vector<int>>& income, int index, int rest, vector<vector<int>>& dp)
{
	if (dp[index][rest] != -1)//该过程计算过,直接拿值
	{
		return dp[index][rest];
	}
	if (index == income.size())//没有司机可以分配了
	{
		dp[index][rest] = 0;
		return dp[index][rest];
	}
	//还有司机可以分配
	//case1:剩下的司机数量正好等于A区域的剩余名额
	if (income.size() - index == rest)
	{
		//当前司机去A的收入 +  然后递归(递归往下的过程司机都是去A区域)
		dp[index][rest] = income[index][0] + process(income, index + 1, rest-1, dp);
		return dp[index][rest];
	}
	//case2:还有司机,但是A区域没有名额了,剩下的司机必去B区域
	if (rest == 0)
	{
		//当前司机去B的收入 + 然后递归(递归往下的过程司机都是去B区域)
		dp[index][rest] =  income[index][1] + process(income, index + 1, rest, dp);
		return dp[index][rest];
	}
	//case3:当前司机，可以去A，也可以去B. 去A/B的收入 + 往下递归
	int p1 = income[index][0] + process(income, index + 1, rest - 1,dp);//case1:去A,A区域剩余名额-1
	int p2 = income[index][1] + process(income, index + 1, rest, dp);//case2:去B
	//返回最大收入
	dp[index][rest] =  max(p1, p2);
	return dp[index][rest];
}
int maxMoney(vector<vector<int>> income)
{
	//如果司机个数为奇数个,就不能平分了!
	if (income.size() == 0 || (income.size() & 1 ))
	{
		return 0;
	}
	int n = income.size();// 司机数量一定是偶数才能平分，A区域分N /2个人  B区域分N/2个人
	int mid = n / 2;
	vector<vector<int>> dp(n + 1, vector<int>(mid + 1,-1));//一张(N+1)*(mid+1)的二维表,最初初始化为-1
	process(income, 0, mid,dp);
	return dp[0][mid];
}

int maxMoney(vector<vector<int>> income)
{
	//如果司机个数为奇数个,就不能平分了!
	if (income.size() == 0 || (income.size() & 1))
	{
		return 0;
	}
	int n = income.size();// 司机数量一定是偶数才能平分，A区域分N /2个人  B区域分N/2个人
	int mid = n / 2;
	vector<vector<int>> dp(n + 1, vector<int>(mid + 1));//一张(N+1)*(mid+1)的二维表
	
	for (int rest = 0; rest <= mid; rest++)
	{
		dp[n][rest] = 0;
	}
	for (int index = n - 1; index >= 0; index--)
	{
		for (int rest = 0; rest <= mid; rest++)
		{
			if (n - index == rest) // N - index == rest  只能去A区域
			{

				dp[index][rest] = income[index][0] + dp[index + 1][rest - 1];
			}
			else if (rest == 0) //rest==0,只能去B区域
			{
				dp[index][rest] = income[index][1] + dp[index + 1][rest];
			}
			else // 当前司机，可以去A，或者去B
			{
				int p1 = income[index][0] + dp[index + 1][rest - 1];
				int p2 = income[index][1] + dp[index + 1][rest];
				dp[index][rest] = max(p1, p2);
			}
		}
	}
	//从0开始往后的司机,A区域还有M个名额的最大收益
	return dp[0][mid];
}
#endif

