#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<string>
#include<vector>
using namespace std;
#if 0
int main()
{
	string str;
	while (getline(cin,str))
	{
		string tmp;
		for (int i = 0; i < str.size(); i++)
		{
			if (str[i] >= '0' && str[i] <= '9') tmp += str[i];
		}
		cout << tmp << endl;
	}
	return 0;
}
#endif

#if 0
//一个数的裂开方法

//process递归函数的含义:上一个拆出来的数是pre,还剩下rest需要被拆解,返回拆解的方法数
int process(int pre, int rest, vector<vector<int>>& dp)
{
	if (dp[pre][rest] != -1)
	{
		return dp[pre][rest];
	}
	if (rest == 0)
	{
		//说明之前的方案都严格遵循了每一个拆分的数都不比左边的数小,那么之前做的拆法就是一种方案
		dp[pre][rest]= 1;
		return dp[pre][rest];
	}
	if (pre > rest)	//前一个数大于我剩下要拆的数->0种方法
	{
		dp[pre][rest] = 0;
		return dp[pre][rest];
	}
	//pre < rest
	int ways = 0;
	//从上一个数pre开始试,试到rest,累加可以凑出rest的方法数
	for (int cur = pre; cur <= rest; cur++)
	{
		//现在我选的数cur,就变成了我下一步的前一个拆分的数,还有rest - cur要拆解
		ways += process(cur, rest - cur,dp);
	}
	dp[pre][rest] =  ways;
	return dp[pre][rest];
}
int splitWays(int n)
{
	if (n <0) return 0;
	if (n == 1) return 1;//1只有一种裂开方法,就是它本身
	vector<vector<int>> dp(n + 1, vector<int>(n + 1,-1));
	process(1, n,dp);
	return dp[1][n];
}

int splitWaysdp(int n)
{
	if (n < 0) return 0;
	if (n == 1) return 1;
	vector<vector<int>> dp(n + 1, vector<int>(n + 1));
	//第0行不处理

	for (int pre = 1; pre <= n; pre++)
	{
		dp[pre][0] = 1;//第0列全是1
		dp[pre][pre] = 1;//对角线全是1
	}
	//第n行已经填好了 ->只有dp[n][0]和dp[n][n]=1,其它为0
	//从下往上填
	for (int pre = n - 1; pre >= 1; pre--)
	{
		//从pre+1位置开始往右填  (填每一行主对角线右边的值)、
		for (int rest = pre + 1; rest <= n; rest++)
		{
			int ways = 0;
			for (int first = pre; first <= rest; first++)
			{
				ways += dp[first][rest - first];
			}
			dp[pre][rest] = ways;
		}
	}
	return dp[1][n];
}
int main()
{
	cout << splitWays(234) << endl;
	cout << splitWaysdp(234) << endl;
	return 0;
}
#endif


#if 0
//当前来到index位置,往后可以任意选择
//返回累加和尽量接近rest但不能超过rest的情况下,最接近的累加和是多少 
int process(vector<int>& arr, int index, int rest, vector<vector<int>>& dp)
{
	if (dp[index][rest] != -1)
	{
		return dp[index][rest];
	}
	if (index == arr.size())//没数可以选择了,最接近的累加和可以认为是0
	{
		dp[index][rest] =  0;
		return dp[index][rest];
	}
	//还有数可以选择: arr[index...]
	
	//case1:不要index位置的数,去index+1位置找最接近rest的累加和
	int p1 = process(arr, index + 1, rest,dp);

	//case2:要index位置的数,去index+1位置找最接近rest-arr[index]的累加和
	//必须要满足arr[index]<=rest,才有case2
	int p2 = 0;
	if (arr[index] <= rest)
	{
		//要arr[i]的数 + 后序i+1往后的数字做选择,目标是最接近rest - arr[i]的累加和
		p2 = arr[index] + process(arr, index + 1, rest - arr[index], dp);
	}
	//p1和p2都接近rest,谁更大谁更接近rest
	dp[index][rest] =  max(p1, p2);
	return dp[index][rest];
}
int RightWays(vector<int> arr)
{
	if (arr.size() == 0 || arr.size() < 2)	//不可以划分为两半
	{
		return 0;
	}
	int sum = 0;
	for (auto x : arr) sum += x;
	int n = arr.size();
	vector<vector<int>> dp(n+1, vector<int>(sum / 2 + 1, -1));
	process(arr, 0, sum/2,dp);
	return dp[0][sum/2];
}
int RightWaysdp(vector<int> arr)
{
	if (arr.size() == 0 || arr.size() < 2)	//不可以划分为两半
	{
		return 0;
	}
	int sum = 0;
	for (auto x : arr) sum += x;
	int n = arr.size();
	vector<vector<int>> dp(n + 1, vector<int>(sum / 2 + 1));
	//当index == n的时候:为0   即dp[n][rest]=0,dp表最初默认就是全0
	
	//从N-1行往上推
	for (int i = n - 1; i >= 0; i--) 
	{
		for (int rest = 0; rest <= sum/2; rest++)
		{
			// 可能性1，不使用arr[i]
			int p1 = dp[i + 1][rest];
			// 可能性2，要使用arr[i]
			int p2 = 0;
			if (arr[i] <= rest)
			{
				p2 = arr[i] + dp[i + 1][rest - arr[i]];
			}
			dp[i][rest] = max(p1, p2);
		}
	}
	return dp[0][sum/2];
}
int main()
{
	vector<int> v{ 11111111,2 };
	cout << RightWays(v) << endl;
	cout << RightWaysdp(v) << endl;
	return 0;
}
#endif

#if 0
int dp(vector<int> &arr) 
{
	if (arr.size() == 0  || arr.size() < 2) 
	{
		return 0;
	}
	int sum = 0;
	for (int num : arr) {
		sum += num;
	}
	sum /= 2;
	int N = arr.size();
	int M = (N + 1) / 2;
	vector<vector<vector<int>>>dp(N + 1, vector<vector<int>>(M + 1, vector<int>(sum + 1)));
	// 最初所有位置初始化为-1
	for (int i = 0; i <= N; i++) 
	{
		for (int j = 0; j <= M; j++) 
		{
			for (int k = 0; k <= sum; k++)
			{
				dp[i][j][k] = -1;
			}
		}
	}

	//i = N的时候,只有pick=0 才是0
	for (int rest = 0; rest <= sum; rest++)
	{
		dp[N][0][rest] = 0;
	}
	//第N层填好了 从下往上填
	for (int i = N - 1; i >= 0; i--) 
	{
		for (int picks = 0; picks <= M; picks++) 
		{
			for (int rest = 0; rest <= sum; rest++) 
			{
				int p1 = dp[i + 1][picks][rest];
				// 就是要使用arr[i]这个数
				int p2 = -1;
				int next = -1;
				if (picks - 1 >= 0 && arr[i] <= rest)  //要防止越界！
				{
					next = dp[i + 1][picks - 1][rest - arr[i]];
				}
				if (next != -1) {
					p2 = arr[i] + next;
				}
				dp[i][picks][rest] = max(p1, p2);
			}
		}
	}
	if ((arr.size() & 1) == 0) 
	{
		return dp[0][arr.size() / 2][sum];
	}
	else
	{
		return max(dp[0][arr.size() / 2][sum], dp[0][(arr.size() / 2) + 1][sum]);
	}
}
int main()
{
	vector<int> v{1,5,7,87,78,4,3,2};
	cout << dp(v) << endl;
	return 0;
}
#endif 


#if 1 

//判断现在i行的皇后如果放在j列上,和之前的皇后是否不打架
bool isValid(vector<int>& record, int i, int j)
{
	// 检查0..i-1行的皇后和此时i行的皇后是否打架！
	for (int k = 0; k < i; k++)
	{
		//k:第k行的皇后 record[k]:第k行的皇后放在哪一列
		//共列 || 共斜线 -> 就打架
		if (j == record[k] ||abs(record[k] - j) == abs(i - k)) 
		{
			return false;
		}
	}
	//不打架！ 当前第i行的皇后可以放在第j列！
	return true;
}

int process(int index, vector<int> record, int n)
{
	if (index == record.size())
	{
		//index到了终止位置:说明之前的皇后在不打架的情况下,所有的皇后填完了
		//那么之前做过的决定就是一种方法
		return 1;
	}
	int res = 0;
	//第index行的皇后放在哪一列呢?从第0列开始试一遍,只要和之前的皇后不打架就可以放！
	for (int cur = 0; cur < n; cur++)
	{
		//isValid:检查index行的皇后放在cur列是否会和之前的皇后打架
		if (isValid(record, index, cur))
		{
			//不打架,此时cur列可以放该皇后
			record[index] = cur;//当前皇后放在i行j列上

			res += process(index + 1, record, n);//统计当前皇后放在cur列,往后放皇后最后满足条件的方法数
		}
		//如果打架,就去考虑放在下一个位置
	}
	return res;
}
int solveNQueens(int n)	//n皇后问题
{
	if (n < 1)
	{
		return 0;
	}
	vector<int> record(n);
	return process(0,record,n);
}
int main()
{
	cout << solveNQueens(4) << endl;
	return 0;
}
#endif