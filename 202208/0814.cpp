#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
#include<string>
#include<unordered_map>
using namespace std;

#if 0
//判断现在i行的皇后如果放在j列上,和之前的皇后是否不打架
bool isValid(vector<int>& record, int i, int j)
{
    // 检查0..i-1行的皇后和此时i行的皇后是否打架！
    for (int k = 0; k < i; k++)
    {
        //k:第k行的皇后 record[k]:第k行的皇后放在哪一列
        //共列 || 共斜线 -> 就打架
        if (j == record[k] || abs(record[k] - j) == abs(i - k))
        {
            return false;
        }
    }
    //不打架！ 当前第i行的皇后可以放在第j列！
    return true;
}

//generateBoard:将当前n皇后的位置信息保存
void generateBoard(vector<int>& record, int n, vector<string>& res)
{
    //一行一行处理,先填满 '.' 再填皇后的列位置即record[i]
    for (int index = 0; index < n; index++)
    {
        string ans(n, '.');//用n个;.'初始化ans
        ans[record[index]] = 'Q';//当前index行的皇后放在该行的record[index]列中
        res.push_back(ans);
    }
}

void process(int index, vector<int>& record, int n, vector<vector<string>>& result)
{
    if (index == record.size())
    {
        //index到了终止位置:说明之前的皇后在不打架的情况下,所有的皇后填完了
        //那么之前做过的决定就是一种有效方法
        vector<string> res;
        generateBoard(record, n, res);
        result.push_back(res);
    }
    //第index行的皇后放在哪一列呢?从第0列开始试一遍,只要和之前的皇后不打架就可以放！
    for (int cur = 0; cur < n; cur++)
    {
        //isValid:检查index行的皇后放在cur列是否会和之前的皇后打架
        if (isValid(record, index, cur))
        {
            //不打架,此时cur列可以放该皇后
            record[index] = cur;//当前皇后放在i行j列上

            process(index + 1, record, n, result);//统计当前皇后放在cur列,往后放皇后最后满足条件的
        }
        //如果打架,就去考虑放在下一个位置
    }
}

vector<vector<string>> solveNQueens(int n) {
    vector<vector<string>> result;
    vector<int> record(n);
    process(0, record, n, result);
    return result;
}

int main()
{
    solveNQueens(1);
    return 0;
}
#endif

#if 0
int findUnsortedSubarray(vector<int>& nums)
{
    if (nums.size() == 0 || nums.size() < 2)
    {
        return 0;
    }
    int n = nums.size();
    //第一遍:从左往右遍历数组
    int right = -1;//记录最右边画×的位置
    int maxVal = INT_MIN;
    for (int i = 0; i < n; i++)
    {
        if (nums[i] >= maxVal)
        {
            maxVal = nums[i];
        }
        else 
        {
            right = i;//记录画×位置
        }
    }
    //第二遍:从右往左遍历数组
    int minVal = INT_MAX;
    int left = n;//记录最左侧画×的位置
    for (int i = n - 1; i >= 0; i--)
    {
        if (nums[i] <= minVal)
        {
            minVal = nums[i];
        }
        else
        {
            left = i;//记录画×位置
        }
    }

    //最后需要排序的是[left,right]位置,个数为:right-left+1
    //注意:如果数组本身是有序的: 如 1 2 3 4 此时left =4  right =-1  返回的是-4,而我们需要返回的是0
    //所以需要返回0和right-left+1的较大者
    return max(0, right - left + 1);
}
#endif


#if 0
    struct Node
    {
        string info;//信息
        Node* next;//指向下一个节点的指针
        Node(string str,Node* n = nullptr)
            :info(str),next(n)
        {}
    };
    class MessageBox
    {
    public:
        MessageBox()
            :waitPoint(1)//最初要等的是1号信息来
        {}
        void Print()
        {
        
            Node* cur = headMap[waitPoint]; //拿出我等的序号的节点
            headMap.erase(waitPoint);// 把当前等待序号的信息从头表移除
            //相当于是遍历链表打印      
            while (cur)
            {
                cout << cur->info << " ";
                cur = cur->next;
                waitPoint++;//等待的信息编号++ 
            }
            //跳出循环时,waitPoint就是下一个要等待的序号
            tailMap.erase(waitPoint - 1);//把当前连续区间的尾信息从尾表移除
            cout << endl;
        }
        void receive(int num,string info)//接收消息的编号以信息的内容
        {
            if (num < 1) return; //无效消息
            Node* cur = new Node(info);
            //1.建立了num~num这个连续区间的头和尾的信息
            headMap[num] = cur;
            tailMap[num] = cur;
            //2.查询尾表有没有某个连续区间以num-1结尾的信息
            if (tailMap.find(num - 1) != tailMap.end())
            {
                //num-1结尾的信息链接num这个信息节点cur,然后在尾表删除num-1这个信息,从头表删除num这个信息
                Node* pnext = tailMap[num - 1];
                pnext->next = cur;
                tailMap.erase(num - 1);
                headMap.erase(num);
            }
            //3. 查询有没有某个连续区间以num+1开头的信息
            if (headMap.find(num + 1) != headMap.end())
            {
                //当前num这个信息链接num+1信息的节点,然后在头表删除num+1这个信息,从尾表删除num这个信息
                Node* pnext = headMap[num + 1];
                cur->next = pnext;
                headMap.erase(num + 1);
                tailMap.erase(num);
            }
            //3.看打印的时机到了没有
            //如果当前来的信息,刚好是我等的信息就打印
            if (num == waitPoint)
            {
                Print();//  进入打印行为
            }
        }
    private:
        unordered_map<int, Node*>   headMap;//头表,所有连续区间的开头序号
        unordered_map<int, Node*>   tailMap;//尾表,所有连续区间的结尾序号    
        int waitPoint;  //表示当前等的信息是什么序号
    };
void test()
{
    MessageBox box;
    // 假设信息从1开始....
    cout << "这是2来到的时候" << endl;
    box.receive(2, "B"); // - 2"
    cout << "这是1来到的时候" << endl;
    box.receive(1, "A"); // 1 2 -> print, trigger is 1

    box.receive(4, "D"); // - 4
    box.receive(5, "E"); // - 4 5
    box.receive(7, "G"); // - 4 5 - 7
    box.receive(8, "H"); // - 4 5 - 7 8
    box.receive(6, "F"); // - 4 5 6 7 8
    box.receive(3, "C"); // 3 4 5 6 7 8 -> print, trigger is 3

    box.receive(9, "I"); // 9 -> print, trigger is 9
        
    box.receive(10, "J"); // 10 -> print, trigger is 10

    box.receive(12, "L"); // - 12
    box.receive(13, "M"); // - 12 13
    box.receive(11, "K"); // 11 12 13 -> print, trigger is 11
}
int main()
{
    test();
    return 0;
}
#endif 

#include<queue>
#include<unordered_set>
#if 0
struct TreeNode
{
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int v)
        :val(v),left(nullptr),right(nullptr)
    {}
};
//构造以root为头的这棵树的父亲表
void createParentMap(TreeNode* root, unordered_map<TreeNode*, TreeNode*>& parent)
{
    if (root == nullptr)  return;
    
    //root的左孩子和右孩子的父亲为root
    if (root->left != nullptr)
    {
        parent[root->left] = root;
        createParentMap(root->left, parent);//递归左树
    }
    if (root->right != nullptr)
    {
        parent[root->right] = root;
        createParentMap(root->right, parent);//递归左树
    }
}
//求与目标节点target相距为K的节点
vector<TreeNode*> DistanceKNodes(TreeNode* root,TreeNode* target,int k)
{
    vector<TreeNode*> ans;
    if (root == nullptr || k<=0) return ans;

    //1.构建父亲表
    unordered_map<TreeNode*, TreeNode*> parent;//父亲表-记录每个节点的父亲
    parent[root] = nullptr;//根节点没有父亲
    createParentMap(root, parent);//填表

    //2.宽度优先遍历
    queue<TreeNode*> q;
    unordered_set<TreeNode*> visited;//记录节点有没有进入过队列
    int curLevel = 0;//记录当前来到的层数
    q.push(root);
    visited.insert(root);

    while (!q.empty())
    {
        //当前层有size个节点
        for (int size = 0; size < q.size(); size++)
        {
            TreeNode* cur = q.front();

            if (curLevel == k)
            {
                ans.push_back(cur);
            }

            //左孩子不为空&&左孩子没有进过队列
            if (cur->left && visited.find(cur->left) != visited.end())
            {
                q.push(cur->left);
                visited.insert(cur->left);
            }
            //右孩子不为空&&右孩子没有进过队列
            if (cur->right && visited.find(cur->right) != visited.end())
            {
                q.push(cur->right);
                visited.insert(cur->right);
            }
            //父节点不为空&&父没有进过队列
            TreeNode* parentNode = parent[cur];
            if (parentNode  && visited.find(parentNode) != visited.end())
            {
                q.push(parentNode);
                visited.insert(parentNode);
            }
        }
        //层数++
        curLevel++;
        //层数>k,说明已经收集完了
        if (curLevel > k)
        {
            break;//跳出循环
        }
    }
    return ans;
}

int main()
{
}
#endif


#if 1
int main()
{

    return 0;
}
#endif