#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<string>
#include<queue>
#include<vector>
#include<stack>
#include<unordered_map>
#include<unordered_set>
#include<algorithm>
using namespace std;
#if 0
int maxPairNum(vector<int> arr, int k)
{
	//无效参数处理
	if (arr.size() == 0 || k < 0 || arr.size() < 2)
	{
		return 0;
	}
	sort(arr.begin(), arr.end());

	int ans = 0;//记录最多可以打多少场比赛
	int n = arr.size();
	int left = 0;
	int right = 0;
	vector<bool> usedR(n,false);//表示每个位置是否用过(已经和别人组队了),最多是n个空间
	while (left < n && right < n)
	{
		//1.left位置已经使用过 ->left++
		if (usedR[left])
		{
			left++;
		}
		else if (left == right) //2.窗口内只有一个数 -> right++
		{
			right++;
		}
		else	//left位置的数没有使用过&&窗口元素个数>=2
		{
			int distance = arr[right] - arr[left];
			if (distance == k) //此时组队!
			{
				ans++;
				usedR[right] = true;
				right++;
				left++;
			}
			else  if (distance < k)
			{
				right++;//扩大范围
			}
			else  //distance > k
			{
				left++;//缩小范围
			}
		}
	}
	return ans;
}
int main()
{
	cout << maxPairNum(vector<int>{1,3,1,3,7,5}, 2) << endl;
	return 0;
}
#endif

#if 0
//最长无重复子串长度
int lengthOfLongestSubstring(string s)
{
	if (s.size() == 0) return 0;
	//map[i] = k, 则代表i这个ascii码的字符上次出现在k位置
	vector<int> map(256, -1);//256个元素,最初初始化为-1
	
	//0位置的字符在0位置出现过
	map[s[0]] = 0;
	int n = s.size();
	int ans = 1;//只要字符串有长度,最长不重复字符最起码有1个长度
	int pre = 1;//代表上一个位置向左推了多长 (相当于dp[i-1]位置)

	//从1位置开始推
	for (int i = 1; i < n; i++)
	{
		pre = min(i - map[s[i]], pre + 1);
		ans = max(ans, pre);
		map[s[i]] = i;
	}
	return ans;
}
#endif

#if 0
int types(vector<string> arr)
{
	unordered_set<string> type;
	for (auto str : arr)
	{
		bool map[26];//标志每个字母是否出现过
	    //遍历字符串,将出现的字符置为true
		for (int i = 0; i < str.size(); i++)
		{
			map[str[i] - 'a'] = true;
		}
		string tmp;
		//遍历26个小写字母 
		for (int i = 0; i < 26; i++)
		{
			if (map[i])	//如果当前字母出现过
				tmp += 'a' + i;
		}
		type.insert(tmp);
	}
	return type.size();
}

#endif
#if 0
int types(vector<string> arr)
{
	unordered_set<int> type;
	for (auto str : arr)
	{
		int key = 0;
		for (int i = 0; i < str.size(); i++)
		{
			//当前字符-'a'就是对应的哪个位要置1  然后或到key上
			key |= (1 << (str[i] - 'a'));
		}
		type.insert(key);
	}
	return type.size();
}
#endif
