#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<vector>
#include<iostream>
#include<string>
using namespace std;

class TreeNode
{
public:
	TreeNode(int v)
		:val(v),left(nullptr),right(nullptr)
	{}
	TreeNode* left;
	TreeNode* right;
	int val;
};
//Morris遍历
#if 1
void Morris(TreeNode* root)
{
	if (root == nullptr) return;
	TreeNode* cur = root;//从头开始遍历 
	TreeNode* mostRight = nullptr;//记录左树最右节点
	//cur走到空就结束
	while (cur)
	{
		mostRight = cur->left;//先指向左孩子
		printf("%c ", cur->val);
		//根据cur有没有左孩子划分情况
		if (mostRight != nullptr) //有左孩子
		{
			//找左树最右节点  (两个条件:右指针不为空 && 指向的不是cur)
			while (mostRight->right && mostRight->right != cur)
			{
				mostRight = mostRight->right;
			}

			//此时mostRight就是左树最右节点
			//如果mostRight的右指针为空-> 当前节点是第一次到达,mostRight右指针指向自己,然后cur向左走
			//如果mostRight的右指针指向的是cur ->当前是第二次到达该节点,恢复mostRight右指针指向空,然后cur向右走
			if (mostRight->right == nullptr)
			{
				mostRight->right = cur;
				cur = cur->left;	
			}
			else
			{
				mostRight->right = nullptr;
				cur = cur->right;
			}
		}
		else //cur没有左孩子->直接向右走
		{
			cur = cur->right;
		}
	}
}
#endif

#if 1
void MorrisPre(TreeNode* root)
{
	if (root == nullptr) return;
	TreeNode* cur = root;//从头开始遍历 
	TreeNode* mostRight = nullptr;//记录左树最右节点
	//cur走到空就结束
	while (cur)
	{
		mostRight = cur->left;//先指向左孩子
		//根据cur有没有左孩子划分情况
		if (mostRight != nullptr) //有左孩子
		{
			//找左树最右节点  (两个条件:右指针不为空 && 指向的不是cur)
			while (mostRight->right != nullptr && mostRight->right != cur)
			{
				mostRight = mostRight->right;
			}

			//此时mostRight就是左树最右节点
			//如果mostRight的右指针为空-> 当前节点是第一次到达,mostRight右指针指向自己,然后cur向左走
			//如果mostRight的右指针指向的是cur ->当前是第二次到达该节点,恢复mostRight右指针指向空,然后cur向右走
			if (mostRight->right == nullptr)	//这里是第一次到达,打印!
			{
				printf("%c ", cur->val);
				mostRight->right = cur;
				cur = cur->left;
			}
			else
			{
				mostRight->right = nullptr;
				cur = cur->right;
			}
		}
		else //cur没有左孩子->直接向右走  打印
		{
			printf("%c ", cur->val);
			cur = cur->right;
		}
	}
}
#endif
#if 1
void MorrisIn(TreeNode* root)
{
	if (root == nullptr) return;
	TreeNode* cur = root;//从头开始遍历 
	TreeNode* mostRight = nullptr;//记录左树最右节点
	//cur走到空就结束
	while (cur)
	{
		mostRight = cur->left;//先指向左孩子
		//根据cur有没有左孩子划分情况
		if (mostRight != nullptr) //有左孩子
		{
			//找左树最右节点  (两个条件:右指针不为空 && 指向的不是cur)
			while (mostRight->right != nullptr && mostRight->right != cur)
			{
				mostRight = mostRight->right;
			}

			//此时mostRight就是左树最右节点
			//如果mostRight的右指针为空-> 当前节点是第一次到达,mostRight右指针指向自己,然后cur向左走
			//如果mostRight的右指针指向的是cur ->当前是第二次到达该节点,恢复mostRight右指针指向空,然后cur向右走
			if (mostRight->right == nullptr)
			{
				mostRight->right = cur;
				cur = cur->left;
			}
			else//这里是第2次到达cur,打印!
			{
				printf("%c ", cur->val);
				mostRight->right = nullptr;
				cur = cur->right;
			}
		}
		else //cur没有左孩子->直接向右走  打印
		{
			printf("%c ", cur->val);
			cur = cur->right;
		}
	}
}
#endif

#if 1

//类似链表反转
TreeNode* reverseEdge(TreeNode* root)
{
	TreeNode* prev = nullptr;
	TreeNode* cur = root;
	TreeNode* next = nullptr;
	//反转节点, 右树指针right相当于是next指针
	while (cur)
	{
		//prev cur next
		next = cur->right;
		cur->right = prev;

		//迭代
		prev = cur;
		cur = next;
	}
	return prev;//返回反转之后的头节点
}
void printEdge(TreeNode* root)	//逆序打印root为头的这棵树的右边界
{
	if (root == nullptr) return;
	TreeNode* tail = reverseEdge(root);//先反转节点
	TreeNode* cur = tail;
	//类似遍历链表
	while (cur)
	{
		printf("%c ", cur->val);
		cur = cur->right;
	}
	//恢复二叉树的形状
	reverseEdge(tail);
}

void MorrisPos(TreeNode* root)
{
	if (root == nullptr) return;
	TreeNode* cur = root;//从头开始遍历 
	TreeNode* mostRight = nullptr;//记录左树最右节点
	//cur走到空就结束
	while (cur)
	{
		mostRight = cur->left;//先指向左孩子
		//根据cur有没有左孩子划分情况
		if (mostRight != nullptr) //有左孩子
		{
			//找左树最右节点  (两个条件:右指针不为空 && 指向的不是cur)
			while (mostRight->right != nullptr && mostRight->right != cur)
			{
				mostRight = mostRight->right;
			}

			//此时mostRight就是左树最右节点
			//如果mostRight的右指针为空-> 当前节点是第一次到达,mostRight右指针指向自己,然后cur向左走
			//如果mostRight的右指针指向的是cur ->当前是第二次到达该节点,恢复mostRight右指针指向空,然后cur向右走
			if (mostRight->right == nullptr)//这里是第1次到达cur节点
			{
				mostRight->right = cur;
				cur = cur->left;
				continue;//结束这一轮循环	
			}
			else//这里是第2次到达cur  ->后序遍历,把打印时机放在第二次到达的时候
			{
				mostRight->right = nullptr;
				printEdge(cur->left);//逆序打印左树的右边界;	
			}
		}
		//cur没有左孩子就直接向右走 || 恢复cur左树最右节点的指向后,往右移动
		cur = cur->right;
	}
	//Morris遍历完成后,再逆序打印整棵树的右边界
	printEdge(root);
}
#endif


#if 1
void MorrisIn(TreeNode* root)
{
	if (root == nullptr) return;
	TreeNode* cur = root;//从头开始遍历 
	TreeNode* mostRight = nullptr;//记录左树最右节点
	//cur走到空就结束
	while (cur)
	{
		mostRight = cur->left;//先指向左孩子
		//根据cur有没有左孩子划分情况
		if (mostRight != nullptr) //有左孩子
		{
			//找左树最右节点  (两个条件:右指针不为空 && 指向的不是cur)
			while (mostRight->right != nullptr && mostRight->right != cur)
			{
				mostRight = mostRight->right;
			}

			//此时mostRight就是左树最右节点
			//如果mostRight的右指针为空-> 当前节点是第一次到达,mostRight右指针指向自己,然后cur向左走
			//如果mostRight的右指针指向的是cur ->当前是第二次到达该节点,恢复mostRight右指针指向空,然后cur向右走
			if (mostRight->right == nullptr)
			{
				mostRight->right = cur;
				cur = cur->left;
				continue;
			}
			else
			{
				mostRight->right = nullptr;
			}
			//如果当前节点没有左树(如果一个节点只能到自己一次)就会跳过上面的if来到这里 直接打印
			//或者 第二次到达的时候走完else,然后出来打印
			//然后cur往右走
			printf("%c ", cur->val);
			cur = cur->right;
		}	
	}
}
#endif


bool isBST(TreeNode* root)
{
	if (root == nullptr) return;
	TreeNode* cur = root;//从头开始遍历 
	TreeNode* mostRight = nullptr;//记录左树最右节点
	int pre = INT_MIN;//上一个中序遍历的节点的值,假设为系统最小
	bool ans = true;//记录是否为BST

	//cur走到空就结束
	while (cur)
	{
		mostRight = cur->left;//先指向左孩子
		//根据cur有没有左孩子划分情况
		if (mostRight != nullptr) //有左孩子
		{
			//找左树最右节点  (两个条件:右指针不为空 && 指向的不是cur)
			while (mostRight->right != nullptr && mostRight->right != cur)
			{
				mostRight = mostRight->right;
			}

			//此时mostRight就是左树最右节点
			//如果mostRight的右指针为空-> 当前节点是第一次到达,mostRight右指针指向自己,然后cur向左走
			//如果mostRight的右指针指向的是cur ->当前是第二次到达该节点,恢复mostRight右指针指向空,然后cur向右走
			if (mostRight->right == nullptr)
			{
				mostRight->right = cur;
				cur = cur->left;
				continue;
			}
			else
			{
				mostRight->right = nullptr;
			}
			//如果当前节点没有左树(如果一个节点只能到自己一次)就会跳过上面的if来到这里 直接打印
			//或者 第二次到达的时候走完else,然后出来打印
			//打印行为->和之前一个节点值对比的过程
			//然后cur往右走
			if (pre != INT_MIN && cur->val <= pre)
			{
				ans = false;
			}
			pre = cur->val;
			cur = cur->right;
		}
	}
	return ans;
}

//process函数含义: 返回以x为头的这棵树的最小深度是多少
int process(TreeNode* x)
{
	//1.如果x是叶子节点,x为头的这棵树的深度就是1
	if (x->left == nullptr && x->right == nullptr)
	{
		return 1;
	}
	int leftH = INT_MAX;
	int rightH = INT_MAX;
	if (x->left)
		leftH = process(x->left);
	if (x->right)
		rightH = process(x->right);
	//左树和右树都不为空 -> 最小深度=min(左树和右树的最小深度)+1
	return min(leftH, rightH) + 1;
}
int minHeight(TreeNode* head)
{
	if (head == nullptr) return 0;
	return process(head);
}

TreeNode* NewNode(char  x)
{
	TreeNode* newnode = new TreeNode(x);
	return newnode;
}
TreeNode* CreaterTree()
{
	TreeNode* A = NewNode('A');
	TreeNode* B = NewNode('B');
	TreeNode* C = NewNode('C');
	TreeNode* D = NewNode('D');
	TreeNode* E = NewNode('E');
	TreeNode* F = NewNode('F');
	TreeNode* G = NewNode('G');
	A->left = B;
	A->right = C;
	B->left = D;
	B->right = E;
	C->left = F;
	C->right = G;
	return A;
}
int main()
{
	TreeNode* root = CreaterTree();
	//Morris(root);
	//MorrisPre(root);
	//cout << endl;
	//MorrisIn(root);
	MorrisPos(root);
	return 0;
}