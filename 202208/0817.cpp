#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<string>
#include<vector>
#include<unordered_map>
using namespace std;
class QueryBox1
{
public:
	QueryBox1(vector<int>& v)
		:arr(v)
	{}

	int query(int L, int R, int val)
	{
		int ans = 0;
		for (int i = L; i <= R; i++)
		{
			if (arr[i] == val) ans++;
		}
		return ans;
	}
private:
	vector<int> arr;
};


class QueryBox2
{
public:
	QueryBox2(vector<int>& v)
		:arr(v)
	{
		//根据arr数组生成一张表
		for (int i = 0; i < arr.size(); i++)
		{
			if (um.find(arr[i]) == um.end())
			{
				vector<int> a;
				a.push_back(i);
				um.insert(make_pair(arr[i], a));
			}
			else
			{
				um[arr[i]].push_back(i);
			}
		}
	}
	//countLess函数作用:在有序数组arr中,找到<val的数中最右的位置
	int countLess(vector<int>& arr, int val)
	{
		int left = 0;
		int right = arr.size() - 1;
		int mostRight = -1;//记录<val的数中最右的位置
		while (left <= right)
		{
			int mid = left + (right - left) / 2;
			if (arr[mid] < val)
			{
				mostRight = mid;
				left = mid + 1;
			}
			else
			{
				right = mid - 1;
			}
		}
		return mostRight + 1;
	}
	int query(int L, int R, int val)
	{
		if (um.find(val) == um.end())//要查询的数不在数组中
		{
			return 0;
		}
		vector<int> indexArr = um[val];
		int a = countLess(indexArr, L);//查询<R+1的最右的位置
		int b = countLess(indexArr, R + 1);//查询<R+1的最右的位置
		return b - a;
	}
private:
	vector<int> arr;
	unordered_map<int, vector<int>>um;//{值,该值在arr数组的哪些位置出现过}
};

class QueryBox3
{
public:
	QueryBox3(vector<int>& v)
		:arr(v)
	{
		//根据arr数组生成一张表
		for (int i = 0; i < arr.size(); i++)
		{
			if (um.find(arr[i]) == um.end())
			{
				vector<int> a;
				a.push_back(i);
				um.insert(make_pair(arr[i], a));
			}
			else
			{
				um[arr[i]].push_back(i);
			}
		}
	}
	//在有序数组arr中,查找>=val的最左位置
	int greatValLeft(vector<int>& arr, int val)
	{
		int l = 0;
		int r = arr.size() - 1;
		int mostLeft = -1;
		while (l <= r)
		{
			int mid = l + (r - l) / 2;
			if (arr[mid] >= val)
			{
				mostLeft = mid;//记录此时的答案
				r = mid - 1;// 去左侧查找s
			}
			else
			{
				l = mid + 1;
			}
		}
		return mostLeft;
	}
	//在有序数组arr中,查找<=val的最右位置
	int LessValRight(vector<int>& arr, int val)
	{
		int l = 0;
		int r = arr.size() - 1;
		int mostRight = -1;
		while (l <= r)
		{
			int mid = l + (r - l) / 2;
			if (arr[mid] <= val)
			{
				mostRight = mid;//记录此时的答案
				l = mid + 1;// 去右侧查找
			}
			else
			{
				r = mid - 1;//去左侧查找
			}
		}
		return mostRight;
	}
	int query(int L, int R, int val)
	{
		if (um.find(val) == um.end())//要查询的数不在数组中
		{
			return 0;
		}
		vector<int> indexArr = um[val];
		int left = greatValLeft(arr, L);//>=L的最左位置
		int right = LessValRight(arr, R);//<=R的最右位置
		return right - left + 1;
	}
private:
	vector<int> arr;
	unordered_map<int, vector<int>>um;//{值,该值在arr数组的哪些位置出现过}
};
int main()
{
	vector<int> v{ 3,2,1,3,2,1,1,3,2 };
	QueryBox1 bx1(v);
	QueryBox2 bx2(v);
	QueryBox2 bx3(v);
	cout << bx1.query(0,8,2) << endl;
	cout << bx2.query(0, 8,2) << endl;
	cout << bx3.query(0, 8,2) << endl;
	return 0;
}