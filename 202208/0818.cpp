#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<vector>
#include<string>
#include<iostream>
using namespace std;
#if 0 
//求nums数组的最大子数组和
int maxSubArray(vector<int>& nums)
{
    if (nums.size() == 1) return nums[0];
    int pre = nums[0];//作用相当于dp[i-1],上一步dp的值 
    int maxVal = nums[0];
    for (int i = 1; i < nums.size(); i++)
    {
        pre = max(nums[i], nums[i] + pre);
        maxVal = max(maxVal, pre);
    }
    return maxVal;
}

//子矩阵最大累加和
//时间复杂度:O(行^2 * 列)
int maxSum(vector<vector<int>> arr)
{
    if (arr.size() == 0) return 0;
    int n = arr.size();
    int m = arr[0].size();
    int maxVal = INT_MIN;
    for (int i = 0; i < n; i++)
    {
        // i~j  必须包含从i行到j行的数据,且只包含i行到j行的数据的情况下的答案是什么
        vector<int> tmp(m);
        for (int j = i; j < n; j++) 
        {
            //把当前行的元素累加到tmp数组,空间压缩
            for (int k = 0; k < m; k++) 
            {
                tmp[k] += arr[j][k];//累加当前第j行的元素的对应位置到tmp中
            }

            //调用子方法:求tmp数组的子数组的最大累加和 和当前的最大累加和对比
            maxVal = max(maxVal, maxSubArray(tmp));
        }
    }
    return  maxVal;
}
#endif

