#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
#include<string>
using namespace std;
#if 0
//process函数的含义:arr[index..]位置及其往后的货币可以自由使用,凑出rest的最少货币张数返回！
int process(vector<int>& arr, int index, int rest)
{
	if (rest < 0)
	{
		return INT_MAX;//无效答案
	}
	if (index == arr.size()) // 没有货币可以选了
	{
		return rest == 0 ? 0 : INT_MAX;//刚好凑出rest,此时需要的货币是0张,否则是无效解
	}
	//不要当前位置的数,去index+1凑出rest的最少货币张数返回
	int p1 = process(arr, index + 1, rest);
	//要index位置的数,去index+1凑出rest-arr[index]的最少货币张数返回
	int p2 = process(arr, index + 1, rest - arr[index]);
	//p2可能是无效解
	if (p2 != INT_MAX)
	{
		p2 += 1;//使用了当前index位置的钱,张数++
	}
	return min(p1,p2);
}
int minCoins(vector<int> arr,int aim)
{
	return process(arr, 0, aim);
}

//process函数的含义:arr[index..]位置及其往后的货币可以自由使用,凑出rest的最少货币张数返回！
int process1(vector<int>& arr, int index, int rest, vector<vector<int>>& dp)
{
	if (rest < 0)
	{
		return INT_MAX;
	}
	//注意rest可能为负数!!!
	if (dp[index][rest] != INT_MIN)
	{
		return dp[index][rest];
	}
	if (index == arr.size()) // 没有货币可以选了
	{
		dp[index][rest] =  rest == 0 ? 0 : INT_MAX;//刚好凑出rest,此时需要的货币是0张,否则是无效解
		return dp[index][rest];
	}
	//不要当前位置的数,去index+1凑出rest的最少货币张数返回
	int p1 = process1(arr, index + 1, rest,dp);
	//要index位置的数,去index+1凑出rest-arr[index]的最少货币张数返回
	int p2 = process1(arr, index + 1, rest - arr[index],dp);
	//p2可能是无效解
	if (p2 != INT_MAX)
	{
		p2 += 1;//使用了当前index位置的钱,张数++
	}
	dp[index][rest] =  min(p1, p2);
	return dp[index][rest];
}
int minCoins2(vector<int> arr, int aim)
{
	int n = arr.size();
	//开辟(N+1)*(aim+1)的二维表 ,最初初始化为INT_MIN
	vector<vector<int>> dp(n + 1, vector<int>(aim + 1,INT_MIN));
	process1(arr, 0, aim,dp);
	return dp[0][aim];
}

int minCoinsdp(vector<int> arr, int aim)
{
	int n = arr.size();
	//开辟(N+1)*(aim+1)的二维表 
	vector<vector<int>> dp(n + 1, vector<int>(aim + 1));
	
	//填表 dp[index][rest]
	//base case:当index == N的时候,只有0位置是0,其它位置是系统最大INT_MAX
	dp[n][0] = 0;
	for (int rest = 1; rest <= aim; rest++)
	{
		dp[n][rest] = INT_MAX;
	}
	//从下往上推
	for (int index = n - 1; index >= 0; index--)
	{
		for (int rest = 0; rest <= aim; rest++)
		{
			int p1 = dp[index + 1][rest];
			int p2 = rest - arr[index] >= 0 ? dp[index + 1][rest - arr[index]] : INT_MAX;
			if (p2 != INT_MAX)
			{
				p2 += 1;
			}
			dp[index][rest] = min(p1, p2);
		}
	}
	return dp[0][aim];
}
int main()
{
	vector<int> v{ 1,2,3,4,5,6,7 };
	cout << minCoins(v, 5) << endl;
	cout << minCoins2(v, 5) << endl;
	cout << minCoinsdp(v, 5) << endl;
	return  0;
}
#endif

#if 0
vector<vector<int>> rightWay(vector<int>& arr)
{
	int n = arr.size();
	vector<vector<int>> res(n, vector<int>(2));
	for (int i = 0; i < arr.size(); i++)
	{
		int leftLessIndex = -1;
		int rightLessIndex = -1;
		//遍历左边部分找<arr[i]的离它最近的位置
		int cur = i - 1;
		while (cur >= 0)
		{
			if (arr[cur] < arr[i])
			{
				leftLessIndex = cur;
				break;
			}
			cur--;
		}
		//遍历右边部分找>arr[i]的离它最近的位置
		cur = i + 1;
		while (cur < n)
		{
			if (arr[cur] < arr[i])
			{
				rightLessIndex = cur;
				break;
			}
			cur++;
		}
		//记录当前位置的答案 
		res[i][0] = leftLessIndex;
		res[i][1] = rightLessIndex;
	}
	return res;
}
int main()
{
	vector<int> v{ 3,4,2,6,1,7,0 };
	vector<vector<int>> ans = rightWay(v);
	for (int i = 0; i < ans.size(); i++)
	{
		for (int j = 0; j < ans[0].size(); j++)
		{
			cout << ans[i][j] <<" ";
		}
		cout << endl;
	}
}
#endif

#include<stack>

#if 1
vector<vector<int>> getNearLessNoRepeat(vector<int>& nums)
{
	int n = nums.size();
	//n*2的二维表 
	vector<vector<int>> ans(n, vector<int>(2));

	//栈由栈顶底到栈顶必须要严格从小到大的, 栈种保存的是下标！！不是值
	stack<int> st;//单调栈！
	
	//遍历数组生成信息
	for (int i = 0; i < n; i++)
	{
		//栈不为空 && 现在栈顶位置在数组中代表的值>当前数
		//即当前数不能压入栈的时候: 弹出栈顶元素并且生成栈顶元素的信息,直到满足规则
		while (!st.empty() && nums[st.top()] > nums[i])
		{
			int top = st.top();
			st.pop();
			//设置弹出元素的左右信息
			//原来top下面压着的(变成了现在的栈顶)就是左信息,当前入栈的就是右信息
			int leftLess = st.empty() ? -1 :st.top();
			int rightLess = i;
			ans[top][0] = leftLess;
			ans[top][1] = rightLess;
		}
		st.push(i);//入栈(存的是下标！！！)
	}
	//遍历完了，但栈中还有元素的情况
	//这种情况下，直接一个个弹出，右边信息为-1（没有让它离开的数据），左边信息为当前数字弹出后的栈顶元素
	while (!st.empty())
	{
		int top = st.top();//设置当前栈顶元素的信息
		st.pop();
		int leftLess = st.empty() ? -1 : st.top();
		int rightLess = - 1;
		ans[top][0] = leftLess;
		ans[top][1] = rightLess;
	}
	return ans;
}
#endif
