#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
#include<string>
#include<stack>
using namespace std;
#if 1
vector<vector<int>> getNearLessNoRepeat(vector<int>& nums)
{
	int n = nums.size();
	//开辟n*2的二维表
	vector<vector<int>> ans(n, vector<int>(2));
	stack<vector<int>> st;//单调栈,此时栈中存的是vector,vector里面存放每一个相同值的下标
	//遍历数组,生成每一个位置的信息
	for (int i = 0; i < n; i++)
	{
		//如果栈不为空并且当前数字要进栈已经打破了规则，就弹出数字并且生成信息
		//st.top()得到是vector,里面存的是下标
		while (!st.empty() && nums[i] < nums[st.top()[0]])
		{
			vector<int> top = st.top();
			st.pop();
			//数组中的每个位置都需要生成信息
			for (int j = 0; j < top.size(); j++)
			{
				//左信息结果值是取弹出后栈顶的vector的最后一个数字
				int leftLess = st.empty() ? -1 : st.top()[st.top().size() - 1];
				int rightLess = i;//当前要入栈的元素下标就是右信息
				ans[top[j]][0] = leftLess;
				ans[top[j]][1] = rightLess;
			}
		}
		//判断当前元素是合并还是单独开空间
		if (!st.empty() && nums[i] == nums[st.top()[0]])
		{
			st.top().push_back(i);
		}
		else
		{
			vector<int>tmp = { i };
			st.push(tmp);
		}
	}
	//遍历完成,栈中还有元素就单独结算
	//右边信息为 - 1（没有让它离开的数据），左边信息为当前数字弹出后的栈顶元素
	while (!st.empty())
	{
		vector<int> top = st.top();
		st.pop();
		for (int i = 0;i<top.size();i++)
		{
			int rightLess = -1;
			int leftLess = st.empty() ? -1 : st.top()[st.top().size() - 1];
			ans[top[i]][1] = -rightLess;
			ans[top[i]][0] = leftLess;
		}
	}
	return ans;
}
#endif

#if 1
int AllTimesMinToMax(vector<int>& arr)
{
	//暴力方法
	//枚举每个子数组都累加一遍
	//O(N^3)
	int maxVal = INT_MIN;
	for (int i = 0; i < arr.size(); i++)
	{
		for (int j = i; j < arr.size(); j++)
		{
			//子数组[i,j]
			int minVal = INT_MAX;//找子数组的最小值
			int sum = 0;//求子数组的累加和
			//在[i,j]求和+找最小值
			for (int k = i; k <= j; k++)
			{
				sum += arr[k];
				minVal = min(minVal, arr[k]);
			}
			//更新最大值
			maxVal = max(maxVal, sum * minVal);
		}
	}
	return maxVal;
}

int AllTimesMinToMax2(vector<int>& nums)
{
	int n = nums.size();
	// 生成前缀和数组
	int* sum = new int[n];
	sum[0] = nums[0];	
	for (int i = 1; i < n; i++)
	{
		sum[i] = sum[i - 1] + nums[i];
	}
	int maxVal = INT_MIN;
	stack<int> st;
	for (int i = 0; i < n; i++)
	{
		while (!st.empty() && nums[i] <= nums[st.top()])
		{
			int top = st.top();
			st.pop();
			int leftLess = st.empty()?-1:st.top();
			int rightLess = i;
		
			int ans = st.empty() ? sum[rightLess - 1] : sum[rightLess - 1] - sum[leftLess];
			maxVal = max(maxVal, ans * nums[top]);
		}
		st.push(i);
	}
	//最后遍历完数组之后,如果栈中还有值,单独处理
	while (!st.empty())
	{
		int top = st.top();
		st.pop();
		int leftLess = st.empty() ? -1 : st.top();
		//此时的右信息相当于是n
		int rightLess = n;
		int ans = st.empty() ? sum[rightLess - 1] : sum[rightLess - 1] - sum[leftLess];
		maxVal = max(maxVal, ans *nums[top]);
	}
	return maxVal;
}
#endif
int main()
{
	vector<int> v{ 1,5,6,8,3,3,2,1,3 };
	cout << AllTimesMinToMax(v) << endl;
	cout << AllTimesMinToMax2(v) << endl;
	return 0;
}