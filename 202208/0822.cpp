#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<string>
#include<stack>
#include<vector>
using namespace std;

#if 0
struct  TreeNode
{
	TreeNode* left;
	TreeNode* right;
	int val;
};
bool SameTree(TreeNode* head1, TreeNode* head2)
{
	if (head1 == nullptr ^ head2 == nullptr)//含义就是:一个为空树一个不为空, 返回false
		return false;

	if (head1 == nullptr && head2 == nullptr)
		return true;
	
	// 两棵树都不为空
	//比较值&&比较二者的左树和右树是否都相同
	return head1->val == head2->val 
		&& SameTree(head1->left, head2->left) && SameTree(head2->right, head2->right);
}
int SameNumber(TreeNode* head)
{
	if (head == nullptr)
		return 0;
	
	int leftNumber = SameNumber(head->left);//左树上有多少个相同子树
	int rightNumber = SameNumber(head->right);//右树上有多少个相同子树
	int headNumber = SameTree(head->left, head->right) ? 1 : 0;//我的左树结构和右树结构相同就+1
	return leftNumber + rightNumber + headNumber;
}
#endif

int minCost(string str1, string str2,int ic,int dc,int rc)
{
	int n = str1.size();
	int m = str2.size();
	//开辟一张(n+1)*(m+1)的二维表
	vector<vector<int>> dp(n + 1, vector<int>(m + 1));
	//base case
	dp[0][0] = 0;
	for (int i = 1; i <=n;i++)// 填第0列
	{
		dp[i][0] = dc * i;//只能删除 ,代价dc->2dc->3dc->，，，，
	}
	for (int j = 1; j <=m; j++) //填第0行
	{
		dp[0][j] = ic * j;//只能插入(添加),代价:ic->2ic->3ic....
	}
	
	//普遍位置
	//从第1行填到第n行
	for (int i = 1; i <=n; i++)
	{
		for (int j = 1; j <=m; j++)
		{
			int p1 = dp[i - 1][j] + dc;//case1:str1的前i-1个字符编辑成str2前j个字符的代价+ 然后删除掉str1的第i-1个字符
			int p2 = dp[i][j - 1] + ic;//case2:str1的前i个字符编辑成str2前j-1个字符,然后加上str2的最后一个字符
			int p3 = INT_MAX;
			int p4 = INT_MAX;
			if (str1[i - 1] == str2[j - 1])
			{
				p3 = dp[i - 1][j - 1];//case3:只需要str1的前i-1个字符编辑成str2前j-1个字符即可
			}
			else
			{
				p4 = dp[i - 1][j - 1] + rc;//case4:只需要str1的前i-1个字符编辑成str2前j-1个字符 + 替换str1的最后一个字符
			}
			
			//取四种可能性的最小值 
			dp[i][j] = min(min(p1, p2), min(p3, p4));
		}
	}
	return dp[n][m];
}
#include<algorithm>

struct Cmp
{
	bool operator()(const string& l, const string& r)
	{
		return l.size() > r.size();//按长度排降序
	}
};
//生成s的子序列放到res
void process(string s, int index, string tmp, vector<string>& res)
{
	if (index == s.size())
	{
		res.push_back(tmp);
		return;
	}
	
	process(s, index + 1, tmp + s[index], res);// 要index位置的字符
	process(s, index + 1, tmp , res);//不要index位置的字符
}
int minCost(string s1, string s2)
{
	vector<string> res;
	string tmp;
	process(s2, 0, tmp, res);//生成s2的子序列放到res中
	//按照长度排序
	sort(res.begin(), res.end(),Cmp());
	//遍历所有的子序列
	for (auto& str : res)
	{
		//判断当前子序列是不是s1的子串
		if (s1.find(str) != -1)
		{
			return s2.size() - str.size();//要删除的字符就是s2的长度-当前子序列的长度
		}
	}
	//上面没有返回
	//说明s2全删掉变成空字符串才是s1的字符串
	return s2.size();
}

//从x字符串只通过删除的方式变到y字符串 返回至少要删几个字符
//如果变不成，返回Integer.Max
int onlyDelete(string x, string y)
{
	//如果x的长度本来就小于y的长度 就没戏！
	if (x.size() < y.size())
	{
		return INT_MAX;
	}
	int n = x.size();
	int m = y.size();
	//开辟一张(n+1)*(m+1)的二维表,最初初始化为系统最大
	vector<vector<int>> dp(n + 1, vector<int>(m + 1, INT_MAX));
	//base case:
	for (int i = 0; i <= n; i++)//第0列
	{
		dp[i][0] = i;//x字符串删除i个字符串变成空串y
	}

	for (int xlen = 1; xlen <= n; xlen++)
	{
		//主对角线上方是没用的
		//列ylen 的范围是0~xlen&&不能超过M(列的长度),所以取二者的较小值
		for (int ylen = 1; ylen <= min(xlen, m); ylen++)
		{
			
			int p1 = INT_MAX;
			int p2 = INT_MAX;
			if (dp[xlen - 1][ylen] != INT_MAX)//case1:
			{
				p1 = dp[xlen - 1][ylen] + 1;//删除s1的最后一个字符,用s1的xlen-1个字符 变成s2的前ylen个字符
			}

			//xlen和ylen是长度,所以最后一个字符是x[xlen-1] 和y[ylen-1]
			//最后一个字符相等 并且 我前面这一坨变成你前面这一坨不是无效的解->才有case2
			if (x[xlen - 1] == y[ylen - 1] && dp[xlen - 1][ylen - 1] != INT_MAX)
			{
				//case2:dp[xlen - 1][ylen - 1]
				//最后一个字符等:  保留最后一个字符,让x前面部分变成y的后面部分
				p2 = dp[xlen - 1][ylen - 1];
			}
			dp[xlen][ylen] = min(p1, p2);//取最小代价
		}
	}
	return dp[n][m];
}
int main()
{
	cout << minCost("abcde", "axbc") << endl;
	cout << onlyDelete("axbc","abcde") << endl;
	return 0;
}