#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<string>
#include<vector>
#include<queue>
using namespace std;

int minKth1(vector<int>& arr, int k)
{
	if (arr.size() == 0 || k<0 || k>arr.size())
	{
		return -1;
	}
	priority_queue<int> pq;//K个数的大根堆
	//1.先把前k个数进堆
	for (int i = 0; i < k; i++)
	{
		pq.push(arr[i]);
	}
	//2.遍历后面的数
	for (int i = k; i < arr.size(); i++)
	{
		if (arr[i] < pq.top())
		{
			pq.pop();
			pq.push(arr[i]);
		}
	}
	return pq.top();
}
int main()
{
	vector<int> v{ 1,2,3,4,5,6,7 };
	cout << minKth1(v, 7) << endl;
	return 0;
}