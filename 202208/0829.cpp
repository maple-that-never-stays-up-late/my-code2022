#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
#include<string>
using namespace std;
void InsertSort(vector<int>& arr, int L, int R)
{
	for (int i = L + 1; i <= R; i++)
	{
		for (int j = i - 1; j >= L && arr[j] > arr[j + 1]; j--)
		{
			swap(arr[j], arr[j + 1]);
		}
	}
}
//取出[L...R]范围的中位数
int getMedian(vector<int>& arr, int L, int R)
{
	InsertSort(arr, L, R);//插入排序  -因为每个小组最多5个数  选常数时间最好的排序
	return arr[(L + R) / 2];
}

// arr[L...R]  五个数一组每个小组内部排序 每个小组中位数领出来，组成marr, marr中的中位数，返回
int medianOfMedians(vector<int>& arr, int L, int R)
{
	int size = R - L + 1;//元素个数[L,R]
	int offset = size % 5 == 0 ? 0 : 1;//元素个数不是5的倍数->最后一组凑不出5个数
	vector<int> mArr(size / 5 + offset);
	for (int team = 0; team < mArr.size(); team++) 
	{
		int teamFirst = L + team * 5;
		// L ... L + 4
		// L +5 ... L +9
		// L +10....L+14

		//每一个组把自己的中位数取出来放到mArr数组
		//最后一组可能不够5个,所以范围是 min(R, teamFirst + 4)
		mArr[team] = getMedian(arr, teamFirst,min(R, teamFirst + 4));
	}
	//然后这个子过程调用大方法:找mArr数组中从[0,m-1]范围上求它中间位置
	return bfprt(mArr, 0, mArr.size() - 1, mArr.size() / 2);
}

//partition含义:返回的是等于pivot区域的左边界和右边界, 本质是荷兰国旗问题
vector<int> partition(vector<int>& arr, int L, int R, int pivot)
{
	int less = L - 1;
	int more = R + 1;
	int cur = L;
	while (cur < more)
	{
		if (arr[cur] < pivot)
		{
			swap(arr[++less], arr[cur++]);
		}
		else if (arr[cur] > pivot)
		{
			swap(arr[cur], arr[--more]);
		}
		else
		{
			cur++;
		}
	}
	return vector<int>{less + 1, more - 1};
}

// arr[L..R]  如果排序的话，返回位于index位置的数是什么,其中index在[L,R]范围上
int bfprt(vector<int>& arr, int L, int R, int index)
{
	if (L == R) return arr[L];
	// medianOfMedians函数含义:
	//[L...R]的每五个数一组  每一个小组内部排好序  小组的中位数组成新数组 这个新数组的中位数返回
	int pivot = medianOfMedians(arr, L, R);


	//然后和之前的一样了,拿这个值做划分
	//命中了就返回,没命中走其中一侧
	vector<int> range = partition(arr, L, R, pivot);
	//观察是否命中了 ->index位于[range[0],range[1]]之间
	if (index >= range[0] && index <= range[1])
	{
		return arr[index];//此时index位置的数就是要求的排完序后位于第index位置的数
	}
	//每命中的话,根据index和range[]的关系,去其中一侧递归  左侧:[L,range[0]-1] 右侧:[range[1]+1,R]
	//index比左边界还小->去左侧递归找位于index位置的数, 否则去右侧找  
	else if (index < range[0])
	{
		return bfprt(arr, L, range[0] - 1, index);
	}
	else
	{
		return bfprt(arr, range[1] + 1, R, index);
	}
}
int main()
{

	return 0;
}