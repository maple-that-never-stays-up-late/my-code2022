#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include <algorithm>
#include <vector>
#include <list>
#include<iostream>
#include<queue>
using namespace std;
//int main()
//{
//	vector<int> v{ 2, 6, 5, 8 };
//	list<int> L{ 9,1,2,4 };
//	sort(v.begin(), v.end());
//	L.sort();
//	vector<int> vRet(v.size() + L.size());
//	//将v和L两个有序序列 
//	merge(v.begin(), v.end(), L.begin(), L.end(), vRet.begin());
//	for (auto e : vRet)
//		cout << e << " ";//0 2 3 5 5 6 7 8 9
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	
//	vector<int> v1{ 4, 1, 8, 0, 5, 9, 3, 7, 2, 6 };
//	// 找该区间中前4个最小的元素, 元素最终存储在v1的前4个位置
//	partial_sort(v1.begin(), v1.begin() + 4, v1.end());
//	
//	vector<int> v2{ 4, 1, 8, 0, 5, 9, 3, 7, 2, 6 };
//	// 找该区间中前4个最大的元素, 元素最终存储在v1的前4个位置
//	partial_sort(v2.begin(), v2.begin() + 4, v2.end(), greater<int>());
//	return 0;
//}
//int main()
//{
//	vector<int> v{ 1,2,3,4,5,6 };
//	reverse(v.begin(), v.end());
//	for (auto e : v)
//	{
//		cout << e << " ";//6 5 4 3 2 1
//	}
//	return 0;
//}
//#include <algorithm>
//#include <vector>
//#include<iostream>
//using namespace std;
//int main()
//{
//	// 因为next_permutation函数是按照大于字典序获取下一个排列组合的
//	// 因此在排序时必须保证序列是升序的
//	vector<int> v = { 4, 1, 2, 3 };
//	sort(v.begin(), v.end());
//
//	do
//	{
//		cout << v[0] << " " << v[1] << " " << v[2] << " " << v[3] << endl;
//	} while (next_permutation(v.begin(), v.end()));
//	cout << endl;
//
//	// 因为prev_permutation函数是按照小于字典序获取下一个排列组合的
//	// 因此在排序时必须保证序列是降序的
//	sort(v.begin(), v.end(), greater<int>());
//	do
//	{
//		cout << v[0] << " " << v[1] << " " << v[2] << " " << v[3] << endl;
//	} while (prev_permutation(v.begin(), v.end()));
//	return 0;
//}

#if 0
//设计在无序数组中收集最大的前K个数字
vector<int> maxTopK1(vector<int>& arr, int k)
{
	if (arr.size() == 0 || k<0 )
	{
		return {};
	}
	if (k >= arr.size())
	{
		return arr;
	}
	//1.排序 O(N*logN)
	//sort默认是排升序,我们这里要选最大的,所以排降序
	sort(arr.begin(), arr.end(), greater<int>{});
	vector<int> ans;
	//因为我们想让ans中升序排序,所以倒序插入
	for (int i = k-1; i >=0; i--)
	{
		ans.push_back(arr[i]);
	}
	return ans;
}

//设计在无序数组中收集最大的前K个数字
vector<int> maxTopK2(vector<int>& arr, int k)
{
	if (arr.size() == 0 || k < 0)
	{
		return {};
	}
	if (k >= arr.size())
	{
		return arr;
	}
	priority_queue<int,vector<int>,greater<int>> pq;//k个数的小堆,默认是大根堆less<int>
	//1.先把前k个数放到堆里
	for (int i = 0; i < k; i++)
		pq.push(arr[i]);
	//2.如果当前数比堆顶元素大就替换它进堆
	for (int i = k; i < arr.size(); i++)
	{
		if (arr[i] > pq.top())
		{
			pq.pop();
			pq.push(arr[i]);
		}
	}
	vector<int> ans;
	//3.把堆里的数据放到ans中
	while (!pq.empty())
	{
		ans.push_back(pq.top());
		pq.pop();
	}
	return ans;
}

//partition含义:返回的是等于pivot区域的左边界和右边界, 本质是荷兰国旗问题
vector<int> partition(vector<int>& arr, int L, int R, int pivot)
{
	int less = L - 1;
	int more = R + 1;
	int cur = L;
	while (cur < more)
	{
		if (arr[cur] < pivot)
		{
			swap(arr[++less], arr[cur++]);
		}
		else if (arr[cur] > pivot)
		{
			swap(arr[cur], arr[--more]);
		}
		else
		{
			cur++;
		}
	}
	return vector<int>{less + 1, more - 1};
}

//process含义:在数组arr[L....R]范围上,如果排完序,位于index位置的数是哪个,index在[L,R]范围上
int process(vector<int>& arr, int L, int R, int index)
{
	if (L == R)//此时L==R==index
	{
		return arr[L];
	}

	int pivot = arr[R];//选右边的作为Key,也可以在[L,R]上随机选择一个数作为pivot,也可以用三数取中
	vector<int> range = partition(arr, L, R, pivot);//range数组:记录等于pivot的左右边界

	//观察是否命中了 ->index位于[range[0],range[1]]之间
	if (index >= range[0] && index <= range[1])
	{
		return arr[index];//此时index位置的数就是要求的排完序后位于第index位置的数
	}
	//每命中的话,根据index和range[]的关系,去其中一侧递归  左侧:[L,range[0]-1] 右侧:[range[1]+1,R]
	//index比左边界还小->去左侧递归找位于index位置的数, 否则去右侧找  
	else if (index < range[0])
	{
		return process(arr, L, range[0] - 1, index);
	}
	else
	{
		return process(arr, range[1] + 1, R, index);
	}
}
//求arr数组中第k小的数
int minKth(vector<int>& arr, int k)
{
	if (arr.size() == 0 || k <= 0 || k > arr.size())
	{
		return -1;
	}
	return process(arr, 0, arr.size() - 1, k - 1);
}

vector<int> maxTopK3(vector<int>& arr, int k)
{
	if (arr.size() == 0 || k < 0)
	{
		return {};
	}
	if (k >= arr.size())
	{
		return arr;
	}
	//1.求出第K大的数是什么->即arr.size()-k小的数什么   ->O(N)
	int num = minKth(arr, arr.size() - k); 
	vector<int> ans;
	int index = 0;
	//2.再次遍历数组arr,如果发现比num大的数,就收集到数组中
	for (int i = 0;i < arr.size(); i++)
	{
		if (arr[i] > num)	//注意这里放严格大的
		{
			ans[index++] = arr[i];
		}
	}
	//没放满k个位置->其余的值都是num
	for (; index < k; index++)
	{
		ans[index] = num;
	}

	//3.排序数组 -> O(k*logk)
	sort(ans.begin(), ans.end());
	return ans;
}
int main()
{
	vector<int> v{ 1,2,3,4,5,5,6,7,9 };
	vector<int> ans1 = maxTopK2(v, 5);
	vector<int> ans2 = maxTopK2(v, 5);
	vector<int> ans3 = maxTopK2(v, 5);
	for (auto e : ans1)
	{
		cout << e << " ";
	}
	cout << endl;

	for (auto e : ans2)
	{
		cout << e << " ";
	}
	cout << endl;

	for (auto e : ans3)
	{
		cout << e << " ";
	}
	cout << endl;
	return 0;
}

#endif


#if 0
#include<cstdlib>
//蓄水池算法
class RandomBox
{
public:
	RandomBox(int cap = 5) //最多荣安cap个球
		:capacity(cap)
		,count(0)
	{
		bag.resize(cap);
	}

	//等概率返回1~i的数
	int randNum(int i)
	{
		return (int)rand() * i + 1;
	}

	void add(int num)//决定当前数num是否被放进袋子
	{
		//前10个球直接放进去
		count++;//当前球号是count
		if (count <= capacity)//当前球的个数<=袋子的大小,就直接放进去
		{
			bag[count - 1] = num;
		}
		else//袋子满了
		{
			//假设现在吐出的是i号球,我们以10/i的概率决定它要不要进袋子
			//如果返回的是1~capacity就做这件事(把该球放到袋子) 否则不做
			if (randNum(count) <= capacity)
			{
				//此时等概率的把袋子里面的任意一个球替换掉
				bag[randNum(capacity) - 1] = num;//randNum(capacity) - 1:返回的是:0~capacity-1的数
			}
		}
	}
	
	//返回当前袋子里面的球
	vector<int> choices()
	{
		return bag;
	}
private:
	vector<int> bag;//袋子
	int capacity;//记录袋子的球的个数
	int count;//当前的球号
};
#endif