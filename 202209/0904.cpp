#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<vector>
#include<string>
#include<iostream>
using namespace std;

//题目1:给定一个正数组成的数组，长度一定大于1，求数组中哪两个数与的结果最大
int maxAndValue1(vector<int>& arr)
{
	int maxVal = INT_MIN;
	//每一轮循环只确定一个元素arr[i],然后和后面的所有元素进行&
	//时间复杂度:O(N^2)
	for (int i = 0; i < arr.size(); i++)
	{
		
		for (int j = i+1; j < arr.size(); j++)
		{
			maxVal = max(maxVal, arr[i] & arr[j]);
		}
	}
	return maxVal;
}

//最优解:时间复杂度:O(N)
int maxAndValue2(vector<int>& arr)
{

}
int main()
{

	return 0;
}