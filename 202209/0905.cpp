#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<vector>
#include<string>
#include<iostream>
using namespace std;

#if 0
//题目1:给定一个正数组成的数组，长度一定大于1，求数组中哪两个数与的结果最大
int maxAndValue1(vector<int>& arr)
{
	int maxVal = INT_MIN;
	//每一轮循环只确定一个元素arr[i],然后和后面的所有元素进行&
	//时间复杂度:O(N^2)
	for (int i = 0; i < arr.size(); i++)
	{

		for (int j = i + 1; j < arr.size(); j++)
		{
			maxVal = max(maxVal, arr[i] & arr[j]);
		}
	}
	return maxVal;
}

//最优解:时间复杂度:O(N)
int maxAndValue2(vector<int>& arr)
{
	int M = arr.size();//标记淘汰区的范围
	//[0,M-1]代表当前有效的元素, [M,arr.size())表示当前淘汰区的元素
	int ans = 0;
	//第31位不用管,从第30位开始考察,最多到第0位
	for (int bit = 30; bit >= 0; bit--)
	{
		//每一轮考察都只用看[0,M-1]有效区的元素,进行淘汰
		int index = 0;
		int tmp = M;//保留淘汰区的边界,因为有可能不淘汰元素！

		while (index < M)
		{
			//如果有效区域的元素arr[index]在第bit位是0
			//->当前数和淘汰区的前一个元素交换.然后淘汰区往左扩,继续考察交换后的这个元素
			if (((arr[index] >> bit) & 1) == 0)
			{
				::swap(arr[index], arr[--M]);
			}
			else //arr[index]在第bit位是1
			{
				index++;//考察下一个元素
			}
		}
		//根据M的大小抉择:
		if (M == 2) // 只有0位置和1位置不在淘汰区  arr[0,1] ,直接&完返回
		{
			return arr[0] & arr[1];
		}
		else if (M < 2) ////说明最终答案在bit位上是不可能为1的,此时恢复淘汰区
		{
			M = tmp;
		}
		else //有不止两个数在bit位上有1,说明最后答案在这一位上肯定有1
		{
			ans |= (1 << bit);
		}
	}
	return ans;
}
int main()
{
	vector<int> v{ 5,6,9,1,2,7,8,2,0,4 };
	cout << maxAndValue1(v) << endl;
	cout << maxAndValue2(v) << endl;
	return 0;
}
#endif

struct TreeNode
{
	int val;
	TreeNode* left;
	TreeNode* right;
};
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
struct Info
{
	long uncovered; // x没有被覆盖，x为头的树至少需要几个相机
	long coveredNoCamera; // x被相机覆盖，但是x没相机，x为头的树至少需要几个相机
	long coveredHasCamera; // x被相机覆盖了，并且x上放了相机，x为头的树至少需要几个相机

	Info(long un, long no, long has)
		:uncovered(un), coveredNoCamera(no), coveredHasCamera(has)
	{}
};
class Solution {
public:


	Info process(TreeNode* x)
	{
		//设置空树的信息
		if (x == nullptr)
		{
			//空树->不需要被覆盖,也可以认为是直接被覆盖,不需要放相机
			return Info(INT_MAX, 0, INT_MAX);
		}

		Info leftInfo = process(x->left);
		Info rightInfo = process(x->right);

		//处理以x为头节点的这棵树的信息
		//信息1:x为头的uncovered: x自己不被覆盖&&x下方所有节点都被覆盖:最少需要多少相机
		//信息1 = 左树的头节点被覆盖且无相机的最少相机数+右树头节点被覆盖且无相机的最少相机数
		//原因:因为如果左孩子和右孩子有相机,那么x为头的树就被覆盖了,不符合信息1的前提
		//又要求x下方所有节点都被覆盖,所以不能用左树和右树的uncover
		long uncovered = leftInfo.coveredNoCamera + rightInfo.coveredNoCamera;

		//信息2: x下方的点都被covered，x也被cover，但x上没相机,最少需要多少相机
		//只要x的左树/右树有相机就是coveredNoCamera状态
		//信息2=min(左树和右树都有相机,左树有相机右树没有,右树有相机左树没有)
		long p1 = leftInfo.coveredHasCamera + rightInfo.coveredHasCamera;
		long p2 = leftInfo.coveredHasCamera + rightInfo.coveredNoCamera;
		long p3 = leftInfo.coveredNoCamera + rightInfo.coveredHasCamera;
		long coveredNoCamera = min(p1, min(p2, p3));

		//信息3:x下方的点都被covered，x也被cover，且x上有相机
		//因为x有相机了,所以左树和右树什么状态都可以,都可以保证左树和右树的根被覆盖到！3种状态谁小选谁
		p1 = min(leftInfo.uncovered, min(leftInfo.coveredNoCamera, leftInfo.coveredHasCamera));
		p2 = min(rightInfo.uncovered, min(rightInfo.coveredNoCamera, rightInfo.coveredHasCamera));
		long coveredHasCamera = p1 + p2 + 1;//因为x上有相机,所以一定要+1,这是放在x上的相机

		return Info(uncovered, coveredNoCamera, coveredHasCamera);
	}
	int minCameraCover(TreeNode* root) {
		Info ans = process(root);
		//最后x没被覆盖(uncovered),就在x这里多+一个相机 + 1
		//然后和其它两个可能性pk
		return (int)min(ans.uncovered + 1, min(ans.coveredNoCamera, ans.coveredHasCamera));
	}
};