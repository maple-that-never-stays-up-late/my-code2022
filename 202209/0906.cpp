#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
using namespace std;

#if 0
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

struct Info
{
    long uncovered; // x没有被覆盖，x为头的树至少需要几个相机
    long coveredNoCamera; // x被相机覆盖，但是x没相机，x为头的树至少需要几个相机
    long coveredHasCamera; // x被相机覆盖了，并且x上放了相机，x为头的树至少需要几个相机

    Info(long un, long no, long has)
        :uncovered(un), coveredNoCamera(no), coveredHasCamera(has)
    {}
};
class Solution {
public:


    Info process(TreeNode* x)
    {
        //设置空树的信息
        if (x == nullptr)
        {
            //空树->不需要被覆盖,也可以认为是直接被覆盖,不需要放相机
            return Info(INT_MAX, 0, INT_MAX);
        }

        Info leftInfo = process(x->left);
        Info rightInfo = process(x->right);

        //处理以x为头节点的这棵树的信息
        //信息1:x为头的uncovered: x自己不被覆盖&&x下方所有节点都被覆盖:最少需要多少相机
        //信息1 = 左树的头节点被覆盖且无相机的最少相机数+右树头节点被覆盖且无相机的最少相机数
        //原因:因为如果左孩子和右孩子有相机,那么x为头的树就被覆盖了,不符合信息1的前提
        //又要求x下方所有节点都被覆盖,所以不能用左树和右树的uncover
        long uncovered = leftInfo.coveredNoCamera + rightInfo.coveredNoCamera;

        //信息2: x下方的点都被covered，x也被cover，但x上没相机,最少需要多少相机
        //只要x的左树/右树有相机就是coveredNoCamera状态
        //信息2=min(左树和右树都有相机,左树有相机右树没有,右树有相机左树没有)
        long p1 = leftInfo.coveredHasCamera + rightInfo.coveredHasCamera;
        long p2 = leftInfo.coveredHasCamera + rightInfo.coveredNoCamera;
        long p3 = leftInfo.coveredNoCamera + rightInfo.coveredHasCamera;
        long coveredNoCamera = min(p1, min(p2, p3));

        //信息3:x下方的点都被covered，x也被cover，且x上有相机
        //因为x有相机了,所以左树和右树什么状态都可以,都可以保证左树和右树的根被覆盖到！3种状态谁小选谁
        p1 = min(leftInfo.uncovered, min(leftInfo.coveredNoCamera, leftInfo.coveredHasCamera));
        p2 = min(rightInfo.uncovered, min(rightInfo.coveredNoCamera, rightInfo.coveredHasCamera));
        long coveredHasCamera = p1 + p2 + 1;//因为x上有相机,所以一定要+1,这是放在x上的相机

        return Info(uncovered, coveredNoCamera, coveredHasCamera);
    }
    int minCameraCover(TreeNode* root) {
        Info ans = process(root);
        //最后x没被覆盖(uncovered),就在x这里多+一个相机 + 1
        //然后和其它两个可能性pk
        return (int)min(ans.uncovered + 1, min(ans.coveredNoCamera, ans.coveredHasCamera));
    }
};
#endif

#include<vector>
#include<unordered_map>
#include<unordered_set>

#if 0
int diff1(vector<int>& arr)
{
    if (arr.size() == 0) return 0;
    unordered_set<int> um;
    for (auto x : arr)
    {
        um.insert(x * x);
    }
    return um.size();
}

int diff2(vector<int>& arr)
{
    if (arr.size() == 0) return 0;
    unordered_set<int> um;
    for (auto x : arr)
    {
        um.insert(abs(x));
    }
    return um.size();
}

int diff3(vector<int>& arr)
{
    if (arr.size() == 0) return 0;
    //双指针
    int left = 0;
    int right = arr.size() - 1;
    int ans = 0;
    //记录双指针当前指向元素的绝对值
    int leftAbs = 0;
    int rightAbs = 0;

    while (left<=right)
    {
        leftAbs = abs(arr[left]);
        rightAbs = abs(arr[right]);
        ans++;//此时就是一种结果
        //根据情况滑动,谁大谁滑动,相同时,一起滑动,要滑动到和自己绝对值不相同的元素
        if (leftAbs < rightAbs)//此时右指针滑动
        {
            while (right >= 0 && abs(arr[right]) == rightAbs) 
            {
                right--;
            }
        }
        else if(leftAbs > rightAbs) //左指针滑动
        {
            while (left <= arr.size() - 1 && abs(arr[left]) == leftAbs)
            {
                left++;
            }
        }
        else  //二者一起滑动
        {
            while (right >= 0 && abs(arr[right]) == rightAbs) 
            {
                right--;
            }
            while (left <= arr.size() - 1 && abs(arr[left]) == leftAbs)
            {
                left++;
            }
        }
    }
    return ans;
}
int main()
{
    vector<int> v{ -7,-7,-7,-6,-2,-1,0,2,4,5,6,7,9,9,9,9 };
    cout << diff1(v) << endl;
    cout << diff2(v) << endl;
    cout << diff3(v) << endl;
    return 0;
}
#endif
#include<string>
//process函数的含义：
//所有的可分解字符串都已经放在了set中, str[i....]如果能够被set中的贴纸分解的话,返回分解的方法数
int process(string& str, int i, unordered_set<string>& set)
{
    if (i == str.size())// 没字符串需要分解了！,此时就是一种有效的分解方法
    {
        return 1;
    }

    int ways = 0;
    //str[i...]往后的字符需要分解
    // [i ... end] 前缀串 每一个前缀串去试探
    for (int end = i; end < str.size(); end++) 
    {
        //[i,i],[i,i+1] ...试后面的每一个前缀,看能否被拆分出来！
        string pre = str.substr(i, end + 1);// 左闭右开
        if (set.find(pre) != set.end()) 
        {	
            ways += process(str, end + 1, set);//只要有这个前缀,就往下继续拆解递归
        }
    }
    return ways;
}
int ways1(string str, vector<string>& arr)
{
    unordered_set<string> set;
    for (auto& s : arr)
    {
        set.insert(s);
    }
    return process(str, 0, set);
}

int ways2(string str, vector<string>& arr)
{
    unordered_set<string> set;
    for (auto& s : arr)
    {
        set.insert(s);
    }
    int n = str.size();
    int* dp = new int[n+1];
    dp[n] = 1;
    //dp[i]的含义:str从i位置出发及其后面所有的字符串,能被set分解有几种方法数
    for (int i = n - 1; i >= 0; i--)
    {
        for (int end = i; end < n; end++) 
        {
            if (set.find(str.substr(i, end + 1)) != set.end()) 
            {
                dp[i] += dp[end + 1];
            }
        }
    }
    int ans = dp[0];
    delete[] dp;
    return ans;
}

class Node
{
public:
    Node()
    {
        nexts.resize(26, nullptr);
        end = false;
    }
    vector<Node*> nexts;//只有小写字母
    bool end;//标志是否是前缀的结尾位置
};

int ways3(string str, vector<string>& arr)
{
    Node* root;
    //构建前缀树
    for (auto& s : arr)
    {
        Node* cur = root;
        int index = 0;
        for (int i = 0; i < s.size(); i++)
        {
            index = s[i] - 'a';//当前字符去哪条路
            if (cur->nexts[index] == nullptr)
            {
                cur->nexts[index] = new Node();//构建一条路
            }
            //往下走
            cur = cur->nexts[index];
        }
        //最后的位置是字符串的结尾位置
        cur->end = true;
    }

    int n = str.size();
    int* dp = new int[n + 1];
    dp[n] = 1;
    for (int i = n - 1; i >= 0; i--) 
    {
        Node* cur = root;
        for (int end = i; end < n; end++)
        {
            int path = str[end] - 'a';
            if (cur->nexts[path] == nullptr)
            {
                break;
            }
            cur = cur->nexts[path];
            if (cur->end) 
            {
                dp[i] += dp[end + 1];
            }
        }
    }
    int ans = dp[0];
    delete[] dp;
    return ans;
}