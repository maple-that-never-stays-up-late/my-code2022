#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

//返回nums数组的最长递增子序列长度
int lengthOfLIS(vector<int>& nums)
{
    if (nums.size() <= 1) return nums.size();
    //dp[i]:子序列必须以i位置结尾,最长递增子序列为多长
    vector<int> dp(nums.size(), 1);
    int ans = 0;//记录最长递增子序列长度
    for (int i = 1; i < nums.size(); i++)
    {
        for (int j = 0; j < i; j++)
        {
            if (nums[i] > nums[j])
                dp[i] = max(dp[i], dp[j] + 1);
        }
        // 每个数更新完毕后随时更新最大子序列长度
        ans = max(ans, dp[i]);
    }
    return ans;
}




class Envelope
{
public:
    Envelope(int len,int hei)
        :length(len)
        ,height(hei)
    {}
    int length;//长度
    int height;//高度
};
struct Cmp
{
    bool operator()(const Envelope& left,const Envelope& right)
    {
        //按长度排序:从小到大排序, 当长度一样时,按高度由大到小排序
        int tmp =  left.length != right.length ? left.length - right.length : right.height - left.height;
        return tmp > 0 ?true : false;
    }
};
int maxEnvelopes(vector<Envelope>& matrix)
{
    //对数组元素进行排序
    sort(matrix.begin(), matrix.end(), Cmp());
    //取出排序好的高度数据
    vector<int> arr;
    for (int i = 0; i < matrix.size(); i++)
    {
        arr.push_back(matrix[i].height);
    }
    return lengthOfLIS(arr);
}

int main()
{
    vector<Envelope> matrix = { {7,2},{5,6},{1,3},{1,5},{1,2},{5,4},{7,6} };
    cout << maxEnvelopes(matrix) << endl;
    return 0;
}