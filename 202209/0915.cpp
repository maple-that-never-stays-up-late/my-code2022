#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
#include<string>
using namespace std;

struct  Node
{
	Node(int v)
		:val(v)
	{}
	Node* left;
	Node* right;
	int val;
};

struct Info
{
	Info(Node* s,Node* e)
		:start(s),end(e)
	{}
	Node* start;//已经串好的双向链表的头节点
	Node* end;//已经串好的双向链表的尾节点
};

//process函数含义:返回以x为头的这棵树形成的双向链表的头和尾
Info process(Node* x)
{
	if (x == nullptr)
		return Info(nullptr, nullptr);
	//拿到左树和右树的信息
	Info leftInfo = process(x->left);
	Info rightInfo = process(x->right);

	//进行链接: 左树链表的尾  x节点  右树链表的头  注意:要防止为空
	if (leftInfo.end != nullptr)
	{
		leftInfo.end->right = x;
	}
	x->left = leftInfo.end;
	x->right = rightInfo.start;
	if (rightInfo.start != nullptr)
	{
		rightInfo.start->left = x;
	}

	//返回以x为头的这棵树形成的链表的头和尾
	//如果leftInfo.start为空,说明左树为空,那么X就作为头节点返回
	//如果rightInfo.end为空,说明右树为空,那么X就作为尾节点返回
	Node* xHead = leftInfo.start != nullptr ? leftInfo.start : x;
	Node* xEnd = rightInfo.end != nullptr ? rightInfo.end : x;
	return Info(xHead, xEnd);
}

//主函数:给定一个搜索二叉树,转为头尾相连的双向链表,返回头节点
Node* treeToDoublyList(Node* head)
{
	if (head == nullptr)
		return nullptr;
	Info allInfo = process(head);//拿到首尾不相接的双向链表
	//首尾相连
	allInfo.end->right = allInfo.start;
	allInfo.start->left = allInfo.end;

	return allInfo.start;
}