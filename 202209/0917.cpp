#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
using namespace std;

//LeetCode32.最长有效括号
class Solution {
public:
    int longestValidParentheses(string s)
    {
        if (s == "") return 0;

        // dp[i] : 子串必须以i位置结尾的情况下,往左最远能扩出多长的有效区域
        //所以i的取值范围:0~n-1
        int n = s.size();
        vector<int> dp(n);

        dp[0] = 0;//只有一个字符的时候是无效的,答案为0
        int ans = 0;//记录最长有效括号的长度
        int pre = 0;//当前谁和i位置的右括号去匹配
        for (int i = 1; i < n; i++)
        {
            //当前i位置的字符是右括号才讨论
            if (s[i] == ')')
            {
                pre = i - dp[i - 1] - 1;
                //pre不越界,并且 pre位置的是左括号,此时才做讨论
                if (pre >= 0 && s[pre] == '(')
                {
                    //当前位置最多能扩的长度是:
                    //起码是前面一个位置扩的长度dp[i - 1]+当前i位置和pre位置配对成功的两个长度
                    //再加上pre-1位置结尾的情况下的有效长度！  注意:要保证pre-1>=0不越界 
                    dp[i] = dp[i - 1] + 2 + (pre > 0 ? dp[pre - 1] : 0);
                }
            }
            ans = max(ans, dp[i]);
        }
        return ans;
    }
};

//(二分:在[0, sumArr.size() - 1]范围上找 >= k的最左侧的位置)
int LeftGreaterK(vector<int>& sumArr, int L, int R, int k)
{
    int ans = -1;
    while (L <= R)
    {
        int mid = L + (R - L) / 2;
        if (sumArr[mid] >= k)
        {
            ans = mid;
            R = mid - 1;
        }
        else
        {
            L = mid + 1;
        }
    }
    return ans;
}

//arr中求子数组的累加和是<=K的并且是最大的,返回这个最大的累加和
int getMaxLessOrEqualK(vector<int>& arr, int k)
{
    //1.先求出arr的前缀和数组
    vector<int> sumArr;
    sumArr.push_back(0);
    int maxVal = INT_MIN;
    int sum = 0;//所有数的累加和
    // 每一步的i，都求子数组必须以i结尾的情况下，求个子数组的累加和，是<=K的，并且是最大的
    for (int i = 0; i < arr.size(); i++) 
    {
        sum += arr[i]; // sum 就是arr[0..i]的累加和
        //找>=sum-k最接近的值的下标, 不为空说明有答案
        int tmpIndex = LeftGreaterK(sumArr, 0, sumArr.size() - 1,sum-k);
        if (tmpIndex != -1)
        {
            maxVal = max(maxVal, sum - sumArr[tmpIndex] );//max就是最接近k的累加和maxVal
        }
        sumArr.push_back(sum); // 当前的前缀和加入到set中去
    }
    return maxVal;
}


int main()
{
    vector<int> v{ 1,3,4,5,7 };
    cout << getMaxLessOrEqualK(v, 4);
    return 0;
}