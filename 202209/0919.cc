#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
#include<string>
using namespace std;


struct Node
{
	Node* left;
	Node* right;
	int val;
	Node(int v)
		:left(nullptr),right(nullptr),val(v)
	{}
};

//当前节点node在第level层,求以node为头的子树,最大深度是多少
int mostLeftLevel(Node* node, int level)
{
	while (node->left != nullptr)
	{
		level++;
		node = node->left;//一直往左树延申
	}
	return level;
}

//process函数的含义:当前来到node节点,它在level层,其中整棵树的高度是height,返回node为头的子树(必是完全二叉树)，有多少个节点
int process(Node* node, int level, int height)
{
	if (level == height) //此时node在最底层,说明其是叶子节点
	{
		return 1;
	}
	//根据node节点的右树的最左孩子在哪一层决定往哪里递归,右树在level+1层！
	int RightMostleftLevel = mostLeftLevel(node->right, level + 1);

	//case1:node的右树的最左孩子到了最后一层 -> node的左树是满的！
	//做法:公式求出左树的节点 然后往右边递归,求左树的节点
	if (RightMostleftLevel == height)
	{
		//此时node节点左树的高度为:整棵树的高度 - node节点为头的树的高度
		//所以左树的节点个数为: 2^(height - level) -1 再加上node节点本身
		int nodeNum = (1<<(height - level) ) -1  +1 ;
		return nodeNum + process(node->right, level + 1, height);//递归node节点的右树求个数
	}
	else  //node 的右树的最左孩子没到最后一层->说明node的右树是满的
	{
		//此时node的右树的高度为:整棵树的高度 - node节点为头的树的高度 -1
		//所以右树的节点个数为: 2^(height - level-1) -1 再加上node节点本身
		int nodeNum = (1 << (height - level -1 )) - 1 + 1;//此时节点  = 2^node右树的高度 -1  +node本身
		return nodeNum + process(node->left, level + 1, height);//递归node节点的左树求个数
	}
}

//求完全二叉树的节点个数
int NodeNum(Node* head)
{
	//空树也是完全二叉树,节点个数为0个
	if (head == nullptr) return 0;
	int height = mostLeftLevel(head, 1);//求出整棵树的高度
	return process(head, 1, height);
}
#include<algorithm>
typedef Node TreeNode;
class Solution {
public:
	void InorderPush(TreeNode* cur, vector<int>& v)
	{
		if (!cur) return;
		InorderPush(cur->left, v);
		v.push_back(cur->val);
		InorderPush(cur->right, v);
	}
	void InorderPop(TreeNode* cur, vector<int>& v, int& index)
	{
		if (!cur) return;
		InorderPop(cur->left, v, index);
		cur->val = v[index++];
		InorderPop(cur->right, v, index);
	}
	void recoverTree(TreeNode* root) {
		vector<int> v;
		InorderPush(root, v);
		sort(v.begin(), v.end());
		int index = 0;
		InorderPop(root, v, index);
	}
};


class Solution {
public:
	TreeNode* x1 = nullptr;//记录第一个错误节点
	TreeNode* x2 = nullptr;//记录第二个错误节点
	TreeNode* pre = nullptr;//记录上一个节点
	int flag = 0;//记录当前是第几次降序

	//中序遍历
	void inOrder(TreeNode* cur)
	{
		if (cur == nullptr) return;
		inOrder(cur->left);
		//前后比较 第一次遍历的时候,pre为空
		if (pre != nullptr && pre->val > cur->val)
		{
			flag++;
			if (flag == 1)
			{
				x1 = pre;
				x2 = cur;
			}
			else //flag == 2
			{
				x2 = cur;
			}
		}
		pre = cur;
		inOrder(cur->right);
	}
	void recoverTree(TreeNode* root) {
		inOrder(root);
		if (x1 && x2)
			swap(x1->val, x2->val);
	}
};



class Solution {
public:
	TreeNode* x1 = nullptr;//记录第一个错误节点
	TreeNode* x2 = nullptr;//记录第二个错误节点
	TreeNode* pre = nullptr;//记录上一个节点
	void inOrder(TreeNode* cur)
	{
		if (cur == nullptr) return;
		inOrder(cur->left);
		//前后比较
		if (pre != nullptr && pre->val > cur->val)
		{
			if (x1 == nullptr) x1 = pre; //记录第一个错误节点

			x2 = cur;//不管是1次/2次降序, 第二个错误节点都能记录正确
		}
		pre = cur;
		inOrder(cur->right);
	}
	void recoverTree(TreeNode* root) {
		inOrder(root);
		swap(x1->val, x2->val);
	}
};

class Solution {
public:
    TreeNode* x1 = nullptr;
    TreeNode* x2 = nullptr;
    TreeNode* pre = nullptr;


    //Morris版本中序遍历
    void MorrisIn(TreeNode* root)
    {
        if (root == nullptr) return;
        TreeNode* cur = root;//从头开始遍历 
        TreeNode* mostRight = nullptr;//记录左树最右节点
        //cur走到空就结束
        while (cur)
        {
            mostRight = cur->left;//先指向左孩子
            //根据cur有没有左孩子划分情况
            if (mostRight != nullptr) //有左孩子
            {
                //找左树最右节点  (两个条件:右指针不为空 && 指向的不是cur)
                while (mostRight->right != nullptr && mostRight->right != cur)
                {
                    mostRight = mostRight->right;
                }

                //此时mostRight就是左树最右节点
                //如果mostRight的右指针为空-> 当前节点是第一次到达,mostRight右指针指向自己,然后cur向左走
                //如果mostRight的右指针指向的是cur ->当前是第二次到达该节点,恢复mostRight右指针指向空,然后cur向右走
                if (mostRight->right == nullptr)
                {
                    mostRight->right = cur;
                    cur = cur->left;
                    continue;
                }
                else
                {
                    mostRight->right = nullptr;
                }
            }	
            //如果当前节点没有左树(如果一个节点只能到自己一次)就会跳过上面的if来到这里 直接处理
            //或者 第二次到达的时候走完else,然后出来处理,然后cur往右走
            //比较+处理
            if(pre!=nullptr && pre->val > cur->val)
            {
                x1 = x1==nullptr?pre:x1;//第一次降序,就取pre的值,第二次降序,x1不变
                x2 = cur;
            }
            pre = cur;
            cur = cur->right;
        }
    }
    void recoverTree(TreeNode* root) {
        MorrisIn(root);
        swap(x1->val,x2->val);
    }
};