#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

//LeetCode41-缺失的第一个正整数
class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {
        sort(nums.begin(), nums.end());//排序
        //因为要求的是缺失的最小正整数,所以ans至少也是1
        int ans = 1;//记录缺失的最小正整数
        for (int i = 0; i < nums.size(); i++)
        {
            //当前数==缺失的最小正整数   缺失的最小正整数++
            if (nums[i] == ans) ans++;
        }
        return ans;
    }
};


class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {
        //例如:当L来到5位置,说明0~4位置上放着的是1~5的数,即i位置上放的是i+1的数
        int L = 0;//当前盯着的位置, 0~L-1是有效区的范围
        int R = nums.size();//垃圾区的左边界

        //R是垃圾区的左边界,当L和R指向同一个位置时,当前位置就是垃圾值,不需要管
        while (L < R)
        {
            //如果当前L位置的数放的就是L+1 (即i位置上放的是i+1的数)->有效区往右扩
            if (nums[L] == L + 1)
            {
                L++;
            }
            //去垃圾区的情况:
            //case1:当前数<=L    因为1~L-1的数已经收集好了,如果当前数<=L就是没用的
            //case2:当前数>R     因为要收集的目标是1~R,如果当前数>R 就是没用的
            //case3:当前数和他应该去的位置的数一样 此时就相当于是重复的数,当前数也是没用的
                    //当前数:nums[L] 其要去的位置:nums[nums[L] -1] (因为i位置上放的是要是i+1)
            else if (nums[L] <= L || nums[L] > R || nums[nums[L] - 1] == nums[L])
            {
                swap(nums[L], nums[--R]); //当前元素和垃圾区的前一个位置交换,继续考察当前位置
            }
            else  //L去它应该去的位置,和那个位置的数交换,继续考察当前位置
            {
                swap(nums[L], nums[nums[L] - 1]);
            }
        }
        //因为0~L-1位置的数都是收集完整的的数1~L, 此时L+1就是缺失的最小正整数
        return L + 1;
    }
};



