#pragma once
#include<iostream>
#include<vector>
#include<algorithm>
#include<stack>
using namespace std;
void Print(vector<int>& v)
{
	for (auto& x : v)	cout << x << " ";
	cout << endl;
}
//Hoare版本
int PartSort1(vector<int>& v, int left, int right)
{
	int keyi = left;
	while (left < right)
	{
		while (left < right && v[right] >= v[keyi])
		{
			right--;
		}
		while (left < right && v[left] <= v[keyi])
		{
			left++;
		}
		swap(v[right], v[left]);
	}
	swap(v[left], v[keyi]);
	return left;
}

//挖坑法
int PartSort2(vector<int>& v, int left, int right)
{
	int pivot = left;
	int keyi = v[left];
	while (left < right)
	{
		while (left < right && v[right] >= keyi)
		{
			right--;
		}
		v[pivot] = v[right];
		pivot = right;

		while (left < right && v[left] <= keyi)
		{
			left++;
		}
		v[pivot] = v[left];
		pivot = left;
	}
	v[pivot] = keyi;
	return pivot;
}
void QuickSortNonR(vector<int>& v, int left, int right)
{
	stack<int> st;
	st.push(left);
	st.push(right);
	while (!st.empty())
	{
		int end = st.top();
		st.pop();
		int begin = st.top();
		st.pop();

		int keyi = PartSort1(v, begin, end);
		//[begin,keyi-1] keyi [keyi+1,end]
		//先归并左区间,就先把右区间放进去,后进先出
		if (end  > keyi + 1)
		{
			st.push(keyi + 1);
			st.push(end);
		}
		if (keyi - 1   > begin)
		{
			st.push(begin);
			st.push(keyi -1 );
		}
	}
}

void QucikSort(vector<int>& v, int left, int right)
{
	if (left >= right) return;

	int key = PartSort2(v, left, right);
	//[left,key-1] key [key+1,right]
	QucikSort(v, left, key - 1);
	QucikSort(v, key + 1, right);
}


void _MergeSort(vector<int>& v, int left, int right, vector<int>& tmp)
{
	if (left >= right)	return;
	int mid = left + ((right - left) >> 1);
	//[left,mid] [mid+1,right]
	_MergeSort(v, left, mid , tmp);
	_MergeSort(v, mid + 1, right, tmp);

	//归并
	int begin1 = left;
	int end1 = mid ;
	int begin2 = mid + 1;
	int end2 = right;
	int index = left;
	while (begin1 <=end1 && begin2 <=end2)
	{
		if (v[begin1] < v[begin2]) tmp[index++] = v[begin1++];
		else tmp[index++] = v[begin2++];
	}
	while(begin1<=end1) tmp[index++] = v[begin1++];
	while(begin2<=end2) tmp[index++] = v[begin2++];

	for (int i = left; i <= right; i++)	v[i] = tmp[i];
 }
void MergeSort(vector<int>& v)
{
	vector<int> tmp(v.size());//辅助空间
	_MergeSort(v,0,v.size()-1,tmp);
}

void ShellSort1(vector<int>& v)
{
	int n = v.size();
	int gap = n;
	while (gap > 1)
	{
		gap /= 2;
		for (int j = 0; j < gap; j++)
		{
			for (int i = j; i < n - gap; i += gap)
			{
				int end = i;
				int x = v[end + gap];
				while (end >= 0)
				{
					if (v[end] > x)
					{
						v[end + gap] = v[end];
						end -= gap;
					}
					else
					{
						break;
					}
				}
				v[end + gap] = x;
			}
		}
	}
}


void ShellSort2(vector<int>& v)
{
	int n = v.size();
	int gap = n;
	while (gap > 1)
	{
		gap /= 2;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int x = v[end + gap];
			while (end >= 0)
			{
				if (v[end] > x)
				{
					v[end + gap] = v[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			v[end + gap] = x;
		}
	}
}