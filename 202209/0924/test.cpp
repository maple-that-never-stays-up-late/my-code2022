#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include"sort.hpp"
void test1()
{
	vector<int> v1{ 9,8,7,6,54,3,22,1 };
	vector<int> v2{ 66,5,341,92,1426,25,11,6 };
	QucikSort(v1,0,v1.size()-1);
	Print(v1);
	QucikSort(v2, 0, v2.size() - 1);
	Print(v2);
}

void test2()
{
	vector<int> v1{ 9,8,7,6,54,3,22,1 };
	vector<int> v2{ 66,5,341,92,1426,25,11,6 };
	MergeSort(v1);
	Print(v1);
	MergeSort(v2);
	Print(v2);
}

void test3()
{
	vector<int> v1{ 9,8,7,6,54,3,22,1 };
	vector<int> v2{ 66,5,341,92,1426,25,11,6 };
	QuickSortNonR(v1, 0, v1.size() - 1);
	Print(v1);
	QuickSortNonR(v2, 0, v2.size() - 1);
	Print(v2);
}

void test4()
{
	vector<int> v1{ 9,8,7,6,54,3,22,1 };
	vector<int> v2{ 66,5,341,92,1426,25,11,6 };
	ShellSort1(v1);
	Print(v1);
	ShellSort1(v2);
	Print(v2);
}

void test5()
{
	vector<int> v1{ 9,8,7,6,54,3,22,1 };
	vector<int> v2{ 66,5,341,92,1426,25,11,6 };
	ShellSort2(v1);
	Print(v1);
	ShellSort2(v2);
	Print(v2);
}
int main()
{
	test5();
	return 0;
}