#pragma once
#include<iostream>
#include<assert.h>
using namespace std;

namespace Mango
{
	//模拟实现vector
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;

		//默认成员函数
		vector()
			:_start(nullptr),_finish(nullptr),_endofstorage(nullptr)
		{}

		vector(size_t n, const T& val)
			:_start(nullptr), _finish(nullptr), _endofstorage(nullptr)
		{
			reserve(n);
			for (size_t i = 0; i < n; i++)
				push_back(val);
		}

		vector(int n, const T& val)
			:_start(nullptr), _finish(nullptr), _endofstorage(nullptr)
		{
			reserve(n);
			for (int i = 0; i < n; i++)
				push_back(val);
		}

		vector(long n, const T& val)
			:_start(nullptr), _finish(nullptr), _endofstorage(nullptr)
		{
			reserve(n);
			for (long i = 0; i < n; i++)
				push_back(val);
		}

		template<class InputIterator>
		vector(InputIterator first, InputIterator last)
			: _start(nullptr), _finish(nullptr), _endofstorage(nullptr)
		{
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}
		vector(const vector<T>& v)
			:_start(nullptr), _finish(nullptr), _endofstorage(nullptr)
		{
			vector<int> tmp(v.begin(), v.end());
			swap(tmp);
		}

		vector<T>& operator=(const vector<T>& v)
		{
			if (this != &v)
			{
				delete[] _start;
				_start = new T[v.capacity()];
				for (size_t i = 0; i < v.size(); i++)
					_start[i] = v[i];
				
				_finish = _start + v.size();
				_endofstorage = _start + v.capacity();
			}
			return *this;
		}
		~vector()
		{
			if (_start)
				delete[] _start;
			
			_start = _endofstorage = _finish = nullptr;
		}

		//迭代器相关函数
		iterator begin()
		{
			return _start;
		}
		iterator end()
		{
			return _finish;
		}
		const_iterator begin()const
		{
			return _start;
		}
		const_iterator end()const
		{
			return _finish;
		}

		//容量和大小相关函数
		size_t size()const
		{
			return _finish - _start;
		}
		size_t capacity()const
		{
			return _endofstorage - _start;
		}
		void reserve(size_t n)
		{
			if (n > capacity())
			{
				size_t sz = size();
				T* tmp = new T[n];
				for (int i = 0; i < sz; i++)
					tmp[i] = _start[i];

				delete[] _start;
				_start = tmp;
				_finish = _start + sz;
				_endofstorage = _start + n;
			}
		}
		void resize(size_t n, const T& val = T())
		{
			if (n < size())
			{
				_finish = _start + n;
			}
			else
			{
				if (n > capacity())
				{
					reserve(n);
				}

				while (_finish != _start + n)
				{
					*_finish = val;
					_finish++;
				}
			}
		}
		bool empty()const
		{
			return _finish == _start;
		}

		//修改容器内容相关函数
		void push_back(const T& x)
		{
			if (size() == capacity())
			{
				size_t newCapacity = capacity() == 0 ? 4 :capacity() * 2;
				reserve(newCapacity);
			}
			*_finish = x;
			_finish++;
		}
		void pop_back()
		{
			assert(!empty());
			_finish--;
		}

		void insert(iterator pos, const T& x)
		{
			if (size() == capacity())
			{
				size_t len = pos - _start;
				size_t newCapacity = capacity() == 0 ?4: capacity() * 2;
				reserve(newCapacity);
				pos = _start + len;
			}
			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				end--;
			}
			*pos = x;
			_finish++;
		}
		iterator erase(iterator pos)
		{
			iterator it = pos + 1;
			while (it != end())
			{
				*(it - 1) = *it;
				it++;
			}
			_finish--;

			return pos;
		}
		void swap(vector<T>& v)
		{
			::swap(_start, v._start);
			::swap(_finish, v._finish);
			::swap(_endofstorage, v._endofstorage);
		}

		//访问容器相关函数
		T& operator[](size_t i)
		{
			assert(i >= 0);
			assert(i < size());
			return _start[i];
		}
		const T& operator[](size_t i)const
		{
			assert(i >= 0);
			assert(i < size());
			return _start[i];
		}

	private:
		iterator _start;        //指向容器的头
		iterator _finish;       //指向有效数据的尾
		iterator _endofstorage; //指向容器的尾
	};
}