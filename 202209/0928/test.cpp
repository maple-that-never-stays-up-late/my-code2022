#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include"Vector.hpp"
#include<string>
using namespace Mango;
#if 0
void test_vector1()
{
    vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.push_back(4);
    v.push_back(5);
    v.push_back(6);

    for (size_t i = 0; i < v.size(); ++i)
    {
        cout << v[i] << " ";
    }
    cout << endl;

    vector<int>::iterator it = v.begin();
    while (it != v.end())
    {
        cout << *it << " ";
        ++it;
    }
    cout << endl;

    for (auto e : v)
    {
        cout << e << " ";
    }
    cout << endl;
}



void test_vector3()
{
    vector<int> v1;
    v1.push_back(1); v1.push_back(2); v1.push_back(3); v1.push_back(4);
    vector<int> v2(v1);
    for (auto e : v2)
        cout << e << " ";
    cout << endl;

    vector<int> v3;
    v3.push_back(10);
    v3.push_back(20);
    v3.push_back(30);

    v1 = v3;
    for (auto e : v1)
        cout << e << " ";
    cout << endl;
}

void test_vector4()
{
    vector<int> v1;
    v1.push_back(1);
    v1.push_back(2);
    v1.push_back(3);
    v1.push_back(4);
    typename vector<int>::iterator it = find(v1.begin(), v1.end(), 2);
    if (it != v1.end())
    {
        // 如果insert中发生了扩容，那么会导致it指向空间被释放
        // it本质就是一个野指针，这种问题，我们就叫迭代器失效
        v1.insert(it, 20);
    }
    // v1.insert(v1.begin(), -1);

    for (auto e : v1)
    {
        cout << e << " ";
    }
    cout << endl;
}

void test_vector5()
{
    // 三种场景去测试
    // 1 2 3 4 5 -> 正常
    // 1 2 3 4   -> 崩溃
    // 1 2 4 5   -> 没删除完
    vector<int> v1;
    v1.push_back(1);
    v1.push_back(2);
    v1.push_back(3);
    v1.push_back(4);
    v1.push_back(5);

        // 要求删除v1所有的偶数

    vector<int>::iterator it = v1.begin();
    while (it != v1.end())
    {
        if (*it % 2 == 0)
            it = v1.erase(it);
        else
            ++it;
    }


    for (auto e : v1)
        cout << e << " ";
    cout << endl;
}

void test_vector6()
{
    vector<string> v;
    v.push_back("111111111111111111111111");
    v.push_back("111111111111111111111111");
    v.push_back("1111111111");
    v.push_back("1111111111");
    v.push_back("1111111111");

    for (auto& e : v)
    {
        cout << e << endl;
    }
    cout << endl;
}

int main()
{
    test_vector6();
	return 0;
}
#endif


#include <iostream>
#include <list>
using namespace std;
#if 0 
int main()
{
    list<int> lt;
    lt.push_back(4);
    lt.push_back(7);
    lt.push_back(5);
    lt.push_back(9);
    lt.push_back(6);
    lt.push_back(0);
    lt.push_back(3);
    for (auto e : lt)
    {
        cout << e << " ";
    }
    cout << endl; //4 7 5 9 6 0 3
    lt.sort(greater<int>()); //默认将容器内数据排为升序
    for (auto e : lt)
    {
        cout << e << " ";
    }
    cout << endl; //0 3 4 5 6 7 9
    return 0;
}
#endif

//int main()
//{
//    int a = 10;
//    int* p = &a;
//    int*& refp = p;
//    cout << *refp << endl;
//    return 0;
//}

//int main()
//{
//    int a;
//    while (cin >> a)
//    {
//      //ctrl+z+enter终止程序
//    }
//}