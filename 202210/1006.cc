#include<iostream>
#include<vector>
using namespace std;
// 比特位计数
#if 0
vector<int> CountBit(int n )
{
	vector<int> ans(n+1,0);
	for(int i = 0;i<=n;i++)
	{
		ans[i] = ans[i/2] + (i&1);
	}
	return ans;
}

vector<int> CountBit2(int n)
{
	vector<int> ans(n+1,0);
	for(int i = 0;i<=n;i++)
	{
		if(i&1)
		{
			ans[i] = ans[i-1] + 1;
		}
		else
		{
			ans[i] = ans[i/2];
		}
	}
	return ans;
}
int BitCount(int n )
{
	int count = 0 ;
	while(n) count+=n&1,n>>=1;
	return count;
}
vector<int> CountBit3(int n )
{
	vector<int> ans(n+1,0);
	for(int i = 0;i<=n;i++)
	{
		ans[i] = BitCount(n);
	}
	return ans;
}
int main()
{
	vector<int> v  = CountBit(3);
	for(auto& x:v)
		cout << x <<" ";
	cout << endl;
	
	vector<int>::iterator it = v.begin();
	while(it!=v.end())
	{
		cout << *it <<" ";
		++it;
	}
	cout << endl;
	return 0;
}
#endif

int main()
{
	
	return 0;
}
