 #include<iostream>
 #include<algorithm>
 #include<vector>
#include<set>
using namespace std;
// int main()
// {
// 	int a[] = {0,0,2,4,5,6,7,8,9};//前提是有序序列
// 	int sz =sizeof(a)/sizeof(a[0]);
//	cout << *lower_bound(a,a+sz,7)<<endl;//查找第一个大于或等于num的位置
//	cout << *upper_bound(a,a+sz,7)<<endl;//查找第一个大于num的位置
//	
//	int i = lower_bound(a,a+sz,7) - a;//下标
//	int j = upper_bound(a,a+sz,7) - a;//得到下标
//	cout << i << " " << j<<endl;
// 	return 0;
//}


int main()
{
	vector<int> v{5,6,3,2,1,8};
	multiset<int> mt(v.begin(),v.end());
	auto pos = mt.lower_bound(5);
	if( pos!= mt.end())
	{
		cout << *pos << endl;//5
	}
	
	pos = mt.upper_bound(5);
	if( pos!= mt.end())
	{
		cout << *pos << endl;//6
	}
	return 0;
}