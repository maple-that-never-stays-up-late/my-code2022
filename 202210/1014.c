﻿#define _CRT_SECURE_NO_WARNINGS 1
#pragma once

//第一题
/*
#include<stdio.h>
int main()
{
	int a, aa, aaa; //三个有符号整数
	unsigned int b, bb;//两个无符号整数
	char c;//一个字符
	double d;//一个浮点数
	scanf("%d,%d,%d,%d,%d,%c,%lf", &a, &aa, &aaa, &b, &bb, &c, &d);

	//输出格式:
	//有符号整形:%d 无符号整形:%u long:%ld short:%hd char:%c float:%f 
	printf("int :%d %d %d %d %d %d %d\n", a, aa, aaa, b, bb, c, d);
	printf("unsigned int :%u %u %u %u %u %u %u\n", a, aa, aaa, b, bb, c, d);
	printf("long:%ld %ld %ld %ld %ld %ld %ld\n", a, aa, aaa, b, bb, c, d);
	printf("short:%hd %hd %hd %hd %hd %hd %hd\n", a, aa, aaa, b, bb, c, d);
	printf("signed char:%c %c %c %c %c %c %c\n", a, aa, aaa, b, bb, c, d);
	printf("float:%f %f %f %f %f %f %f\n", a, aa, aaa, b, bb, c, d);
	return 0;
}
*/

//第三题
/*
#include<stdio.h>
int main()
{
	double p =12345.0123456789;
	double q = 123456789.0123456789;
	printf("p = %lf ,q = %lf\n", p, q);//%lf默认只保留6位小数
	return 0;
}
*/




//第四题
/*
#include<stdio.h>
//浮点数直接比较是有精度损失的,不可以直接比较 
#define eps 1e-8
#define MoreEqu(a, b) (((a) - (b)) > (-eps))
#define LessEqu(a, b) (((a) - (b)) < (eps))
int main()
{
	double d;
	scanf("%lf", &d);
	int result = 0;
	if (d > 0) 
	{
		double tmp = d + 0.4 - (int)d; 
		if (MoreEqu(tmp,1)) //5舍6入 
		{
			result = d + 1;
		}
		else
		{
			result = d;
		}
	}
	else //d<=0
	{
		double tmp = d - 0.4 - (int)d; 
		if (LessEqu(tmp, -1))
		{
			result = d - 1;
		}
		else
		{
			result = d;
		}
	}
	printf("the result is %d\n", result);
	return 0;
}
*/

//第五题
/*
#include<stdio.h>
int main()
{
	int time1, time2;
	scanf("%d %d", &time1, &time2);
	//根据题目要求,DDHHMM的形式，其中DD表示是某⽇,HH是某时,MM是某分
	//1.拿到出发时间和到达时间对应的时间
	int data1 = time1 / 10000;
	int data2 = time2 / 10000;
	int hour1 = time1 / 100 % 100;
	int hour2 = time2 / 100 % 100;
	int min1 = time1 % 100;
	int min2 = time2 % 100;

	//2.先计算差值
	int min = min2 - min1;
	int hour = min < 0 ? hour2 - hour1 - 1 : hour2 - hour1;
	int data = hour < 0 ? data2 - data1 - 1 : data2 - data1;
	//可能需要借位,跨月/跨时的情况
	min = min < 0 ? min + 60 : min;
	hour = hour < 0 ? hour + 24 : hour;
	data = data < 0 ? data + 12 : data;
	printf("The journey time is %d day %d hrs %d mins\n", data, hour, min);
	return 0;
}

*/
//第六题
/*
#include<stdio.h>
int main()
{
	char a1, a2, a3, a4;
	scanf("%c %c %c %c", &a1, &a2, &a3, &a4);
	//按照条件加密
	a1 = ((a1 - 'A' + 9) % 26) + 65;
	a2 = ((a2 - 'A' + 9) % 26) + 65;
	a3 = ((a3 - 'A' + 9) % 26) + 65;
	a4 = ((a4 - 'A' + 9) % 26) + 65;
	//交换a1和a3 
	char tmp = a1;
	a1 = a3;
	a3 = tmp;
	//交换a2和a4
	tmp = a2;
	a2 = a4;
	a4 = tmp;
	//输出结果
	printf("The encrypted chars are %c%c%c%c\n", a1, a2, a3, a4);
	return 0;
}
*/

//第七题
/*
#include<stdio.h>
int main()
{
	printf("sizeof char = %d\n", sizeof(char));
	printf("sizeof short = %d\n", sizeof(short));
	printf("sizeof int = %d\n", sizeof(int));
	printf("sizeof long = %d\n", sizeof(long));
	printf("sizeof long long = %d\n", sizeof(long long));

	printf("sizeof unsigned char = %d\n", sizeof(unsigned char));
	printf("sizeof unsigned short = %d\n", sizeof(unsigned short));
	printf("sizeof unsigned int = %d\n", sizeof(unsigned int));
	printf("sizeof unsigned long = %d\n", sizeof(unsigned long));
	printf("sizeof unsigned long long = %d\n", sizeof(unsigned long long));
	
	char a0 = -128, a1 = 127;
	short b0 = -32768, b1 = 32767;
	int c0, c1; c0 = 0 - 2147483648, c1 = 2147483647;
	long d0, d1; d0 = 0 - 2147483648, d1 = 2147483647;
	long long e0, e1; e0 = 0 - 9223372036854665808, e1 = 9223372036854665807;
	printf("\n char:%d to %d \n short:%d to %d \n int:%d to %d\n", a0, a1, b0, b1, c0, c1);
	printf("\n long:%ld to %ld \n long long:%lld to %lld \n", d0,d1,e0,e1);
	return 0;
}
*/


//
#include<stdio.h>
void PrintBit(int n)
{
	for (int i = 31; i >= 0; i--)
	{
		if ((n >> i) & 1)
		{
			printf("1");
		}
		else
		{
			printf("0");
		}
	}
	printf("\n");
}
