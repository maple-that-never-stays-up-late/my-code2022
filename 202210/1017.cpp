#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
using namespace std;
//int main()
//{
//	int a = 10;
//	int b = 20;
//	auto p = [&](){
//		swap(a, b);
//	};
//	p();
//
//	return 0;
//}

#if 0
class Add
{
public:
	Add(int base)
		:_base(base)
	{}
	int operator()(int num)
	{
		return _base + num;
	}
private:
	int _base;
};

int main()
{
	int base = 1;

	//函数对象
	Add add1(base);
	add1(1000);

	//lambda表达式
	auto add2 = [base](int num)->int //以值的方式捕获base,有一个参数,返回值是int
	{
		return base + num;
	};
	add2(1000);
	return 0;
}
#endif

#if 0
int main()
{
	int a = 10, b = 20;

	auto swap1 = [&]() {
		swap(a, b);
	};
	swap1();

	auto swap2 = [&]() {
		swap(a, b);
	};
	swap2();
	cout << typeid(swap1).name() << endl;
	cout << typeid(swap2).name() << endl;
	return 0;
}
#endif

//class A
//{
//public:
//	void test()
//	{
//		auto p = [&]{
//			this->a = 20;
//		};
//	}
//private:
//	int a = 10;
//};
//int main()
//{
//	A a;
//	a.test();
//	return 0;
//}

#if 0
int main()
{
	auto add1 = [](int a, int b)->int {
		return a + b;
	};
	auto add2 = [](int a, int b) {	// 返回值类型可以省略
		return a + b;
	};

	//如果此时我们想定义一个和add1一样类型的对象:
	//方法1:
	auto add3 = add1;
	//方法2:
	decltype(add1) add4 = add1;
	cout << typeid(add1).name() << endl;
	cout << typeid(add3).name() << endl;
	cout << typeid(add4).name() << endl;

	cout << add1(10, 20) << " " << add2(10, 20) << " " 
		<< add3(10, 20) << " " << add3(10, 20) << " " << endl;

	return 0;
}
#endif

#include<assert.h>
#include<algorithm>
////模拟实现的string
namespace Mango
{
	class string
	{
	public:
		//构造函数
		string(const char* str = "")
		{
			cout << "string(const char* str) -- 构造函数" << endl;

			_size = strlen(str); //初始时,字符串大小设置为字符串长度
			_capacity = _size; //初始时,字符串容量设置为字符串长度
			_str = new char[_capacity + 1]; //为存储字符串开辟空间（多开一个用于存放'\0'）
			strcpy(_str, str); //将C字符串拷贝到已开好的空间
		}
		//交换两个对象的数据
		void swap(string& s)
		{
			//调用库里的swap
			::swap(_str, s._str); //交换两个对象的C字符串
			::swap(_size, s._size); //交换两个对象的大小
			::swap(_capacity, s._capacity); //交换两个对象的容量
		}
		//拷贝构造函数（现代写法）
		string(const string& s)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			cout << "string(const string& s) -- 拷贝构造" << endl;

			string tmp(s._str); //调用构造函数,构造出一个C字符串为s._str的对象
			swap(tmp); //交换这两个对象
		}
		//移动构造
		string(string&& s)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			cout << "string(string&& s) -- 移动构造" << endl;
			swap(s);
		}
		//拷贝赋值函数（现代写法）
		string& operator=(const string& s)
		{
			cout << "string& operator=(const string& s) -- 深拷贝" << endl;

			string tmp(s); //用s拷贝构造出对象tmp
			swap(tmp); //交换这两个对象
			return *this; //返回左值（支持连续赋值）
		}
		//移动赋值
		string& operator=(string&& s)
		{
			cout << "string& operator=(string&& s) -- 移动赋值" << endl;
			swap(s);
			return *this;
		}
		//析构函数
		~string()
		{
			//delete[] _str;  //释放_str指向的空间
			_str = nullptr; //及时置空,防止非法访问
			_size = 0;      //大小置0
			_capacity = 0;  //容量置0
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};
}
//
//int main()
//{
//	Mango::string s;
//	s = Mango::to_string(123);
//	//cout << Mango::to_string(123).c_str() << endl;
//	return 0;
//}

//int main()
//{
//	Mango::string s1("hello");
//	Mango::string s2("hello");
//
//	Mango::string s3 = move(s1);
//	move(s2);
//}

//#include<list>
//int main()
//{
//	list<Mango::string> lt;
//	Mango::string s("1111");
//
//	lt.push_back(s); //调用string的拷贝构造
//
//	lt.push_back("2222");             //调用string的移动构造
//	lt.push_back(Mango::string("3333")); //调用string的移动构造
//	lt.push_back(std::move(s));       //调用string的移动构造
//	return 0;
//}

//void Func(int& x)
//{
//	cout << "左值引用" << endl;
//}
//void Func(const int& x)
//{
//	cout << "const 左值引用" << endl;
//}
//void Func(int&& x)
//{
//	cout << "右值引用" << endl;
//}
//void Func(const int&& x)
//{
//	cout << "const 右值引用" << endl;
//}
//
//template<class T>
//void PerfectForward(T&& t)
//{
//	Func(std::forward<T>(t));
//}
//int main()
//{
//	int a = 10;
//	PerfectForward(a);       //a是左值
//	PerfectForward(move(a)); //move(a)是右值
//
//	const int b = 20;
//	PerfectForward(b);       //const 左值
//	PerfectForward(move(b)); //const 右值
//	return 0;
//}

namespace Mango
{
	template<class T>
	struct ListNode
	{
		T _data;
		ListNode* _next = nullptr;
		ListNode* _prev = nullptr;
	};
	template<class T>
	class list
	{
		typedef ListNode<T> node;
	public:
		//构造函数
		list()
		{
			_head = new node;
			_head->_next = _head;
			_head->_prev = _head;
		}
		//左值引用版本的push_back
		void push_back(const T& x)
		{
			insert(_head, x);
		}
		//右值引用版本的push_back
		void push_back(T&& x)
		{
			insert(_head, std::forward<T>(x)); //完美转发
		}
		//左值引用版本的insert
		void insert(node* pos, const T& x)
		{
			node* prev = pos->_prev;
			node* newnode = (node*)malloc(sizeof(node));//使用malloc开辟空间
			new(&newnode->_data)T(x);//调用定位new初始化
			prev->_next = newnode;
			newnode->_prev = prev;
			newnode->_next = pos;
			pos->_prev = newnode;
		}
		//右值引用版本的insert
		void insert(node* pos, T&& x)
		{
			node* prev = pos->_prev;
			node* newnode = (node*)malloc(sizeof(node));//开空间
			new(&newnode->_data)T(std::forward<T>(x));//调用定位new初始化

			prev->_next = newnode;
			newnode->_prev = prev;
			newnode->_next = pos;
			pos->_prev = newnode;
		}
	private:
		node* _head; //指向链表头结点的指针
	};
}
#include<list>
//int main()
//{
//	list<Mango::string> lt;
//	Mango::string s("1111");
//	lt.push_back(s);      
//
//	lt.push_back("2222"); 
//	return 0;
//}

//int main()
//{
//	Mango::string s1= Mango::string("hello");
//
//	return 0;
//}
////递归终止函数
//template<class T>
//void ShowListArg(const T& t)
//{
//	cout << t << endl;
//}
////供外部调用的函数
//template<class ...Args>
//void ShowList(Args... args)
//{
//	ShowListArg(args ...);
//}
////展开函数
//template<class T, class ...Args>
//void ShowListArg(T value, Args... args)
//{
//	cout << value << " "; //打印传入的若干参数中的第一个参数
//	ShowList(args...); //将剩下参数继续向下传
//}
//
//int main()
//{
//	ShowList();
//	ShowList(1);
//	ShowList(1, 'A');
//	ShowList(1, 'A', string("hello"));
//	return 0;
//}

////展开函数
//template<class ...Args>
//void ShowList(Args... args)
//{
//	int arr[] = { args... }; //列表初始化
//	//打印参数包中的各个参数
//	for (auto e : arr)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//}
//int main()
//{
//	ShowList(1);
//	ShowList(1, 2);
//	ShowList(1, 2, 3);
//	return 0;
//}

////处理参数包中的每个参数
//template<class T>
//void PrintArg(const T& t)
//{
//	cout << t << " ";
//}
////展开函数
//template<class ...Args>
//void ShowList(Args... args)
//{
//	int arr[] = { (PrintArg(args), 0) ... }; //列表初始化+逗号表达式
//	cout << endl;
//}
//int main()
//{
//	ShowList();
//	ShowList(1);
//	ShowList(1, 2);
//	ShowList(1, 2, 3);
//	return 0;
//}

////递归终止函数
//void ShowList()
//{
//	cout << endl;
//}
//// 递归终止函数
//template <class T>
//void ShowList(const T& t)
//{
//	cout << t << endl;
//}
////展开函数
//template<class T, class ...Args>
//void ShowList(T value, Args... args)
//{
//	cout << value << " "; //打印分离出的第一个参数
//	ShowList(args...);    //递归调用.将参数包继续向下传
//}
//int main()
//{
//	ShowList();
//	ShowList(1);
//	ShowList(1, 2);
//	ShowList(1, 2, 3);
//	return 0;
//}
//int main()
//{
//	for (int i = 1; i <=9; i++)
//	{
//		for (int j = 1; j <=i; j++)
//		{
//			cout << i << "*" << j << "=" << i * j << " ";
//		}
//		cout << endl;
//	}
//	return 0;
//}

//int main()
//{
//	std::list<std::pair<int, string>> mylist;
//	pair<int, string> kv(10, "111");
//	mylist.push_back(kv);                              //传左值
//	mylist.push_back(pair<int, string>(20, "222"));    //传右值
//	mylist.push_back({ 30, "333" });                   //列表初始化
//
//	mylist.emplace_back(kv);                           //传左值
//	mylist.emplace_back(pair<int, string>(40, "444")); //传右值
//	mylist.emplace_back(50, "555");                    //传参数包
//
//	return 0;
//}  
//int main()
//{
//	list<pair<int,Mango::string>> mylist;
//
//	pair<int,Mango::string> kv(1, "one");
//	mylist.emplace_back(kv);                              //传左值
//	cout << endl;
//
//	mylist.emplace_back(pair<int,Mango::string>(2, "two")); //传右值
//	cout << endl;
//
//	mylist.emplace_back(3, "three");                      //传参数包
//	return 0;
//}

int main()
{
	list<pair<int, Mango::string>> mylist;
	pair<int, Mango::string> kv(1, "one");
	mylist.push_back(kv);                              //传左值
	cout << endl;

	mylist.push_back(pair<int, Mango::string>(2, "two")); //传右值
	cout << endl;

	mylist.push_back({ 3, "three" });                  //列表初始化
	return 0;
}