#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
#include<functional>
using namespace std;

//template <class T> function;     // undefined


//int f(int a, int b)
//{
//	return a + b;
//}
//struct Functor
//{
//public:
//	int operator()(int a, int b)
//	{
//		return a + b;
//	}
//};
//class Plus
//{
//public:
//	static int plusi(int a, int b)
//	{
//		return a + b;
//	}
//	double plusd(double a, double b)
//	{
//		return a + b;
//	}
//};
//int main()
//{
//	//1、包装函数指针（函数名）
//	function<int(int, int)> func1 = f;
//	cout << func1(1, 2) << endl;
//
//	//2、包装仿函数（函数对象）
//	function<int(int, int)> func2 = Functor();
//	cout << func2(1, 2) << endl;
//
//	//3、包装lambda表达式
//	function<int(int, int)> func3 = [](int a, int b) {return a + b; };
//	cout << func3(1, 2) << endl;
//
//	//4、类的静态成员函数
//	//function<int(int, int)> func4 = Plus::plusi;
//	function<int(int, int)> func4 = &Plus::plusi; //&可省略
//	cout << func4(1, 2) << endl;
//
//	//5、类的非静态成员函数
//	function<double(Plus, double, double)> func5 = &Plus::plusd; //&不可省略
//	cout << func5(Plus(), 1.1, 2.2) << endl;
//	return 0;
//}

//template<class F, class T>
//T useF(F f, T x)
//{
//	static int count = 0;
//	cout << "count: " << ++count << endl;
//	cout << "count: " << &count << endl;
//
//	return f(x);
//}
//double f(double i)
//{
//	return i / 2;
//}
//struct Functor
//{
//	double operator()(double d)
//	{
//		return d / 3;
//	}
//};
//int main()
//{
//	//函数名
//	function<double(double)> func1 = f;
//	cout << useF(func1, 11.11) << endl;
//
//	//函数对象
//	function<double(double)> func2 = Functor();
//	cout << useF(func2, 11.11) << endl;
//
//	//lambda表达式
//	function<double(double)> func3 = [](double d)->double {return d / 4; };
//	cout << useF(func3, 11.11) << endl;
//	return 0;
//}

//int Plus(int a, int b)
//{
//	return a + b;
//}
//int main()
//{
//	//无意义的绑定
//	function<int(int, int)> func = bind(Plus, placeholders::_1, placeholders::_2);
//	cout << func(1, 2) << endl; //3
//	return 0;
//}

//
//#include<thread>
//void func(int n)
//{
//	for (int i = 0; i <= n; i++)
//	{
//		cout << i << " ";
//	}
//	cout << endl;
//}
//int main()
//{
//	thread t1;
//	t1 = thread(func, 10);
//
//	t1.join();
//	return 0;
//}

#if 0
#include<thread>
void func()
{
	cout << this_thread::get_id() << endl; //获取线程id
}
int main()
{
	thread t(func);

	t.join();
	return 0;
}
#endif

//#include<thread>
//void add(int& num)
//{
//	num++;
//}
//int main()
//{
//	int num = 0;
//	thread t(add, std::ref(num));//借助ref函数保持对实参的引用
//	t.join();
//
//	cout << num << endl; //1
//	return 0;
//}

#include<thread>
//void add(int* num)
//{
//	(*num)++;
//}
//int main()
//{
//	int num = 0;
//	thread t(add, &num);
//	t.join();
//
//	cout << num << endl; //1
//	return 0;
//}
//
//int main()
//{
//	int num = 0;
//	thread t([&num] {num++; }); 
//	thread t2([&] {num++; });
//	t.join();
//	t2.join();
//	cout << num << endl; //1
//	return 0;
//}

//class A
//{
//public:
//	void func()
//	{
//		cout << "void func()" << endl;
//	}
//};
//int main()
//{
//	A a;
//	thread t(&A::func,&a);
//	t.join();
//	return 0;
//}
//struct A
//{
//	void operator()(int a = 10)
//	{
//		cout << "A::operator(),a =  " <<a << endl;
//	}
//};
//int main()
//{
//	A a1;
//	thread t1(a1); //调用A::operator()  a1是可调用对象
//	thread t2(A(), 20); 
//	t1.join();
//	t2.join();
//	return 0;
//}

//void func(int n)
//{
//	for (int i = 0; i <= n; i++)
//	{
//		cout << i << endl;
//	}
//}
//int main()
//{
//	thread t(func, 20);
//	t.join();
//	t.join(); //程序崩溃
//	return 0;
//}
//
//void func(int n)
//{
//	for (int i = 0; i <= n; i++)
//	{
//		cout << i << endl;
//	}
//}
//bool DoSomething()
//{
//	return false;
//}
//int main()
//{
//	thread t(func, 20);
//
//	if (!DoSomething())
//		return -1;
//	t.join(); //因为上面返回了,所以这里不会被执行
//	return 0;
//}

//class myThread
//{
//public:
//	myThread(thread& t)
//		:_t(t) //引用变量必须在初始化列表初始化
//	{}
//	~myThread()
//	{
//		if (_t.joinable())
//			_t.join();
//	}
//	//防拷贝
//	myThread(myThread const&) = delete;
//	myThread& operator=(const myThread&) = delete;
//private:
//	thread& _t; //这里要用引用,因为thread不支持拷贝和赋值
//};
//int main()
//{
//	thread t(func, 20);
//	t.detach();
//
//	if (!DoSomething())
//		return -1;
//	return 0;
//}

//void func(int n)
//{
//	for (int i = 1; i <= n; i++)
//	{
//		cout << i << " ";
//	}
//
//	cout << endl;
//}
//int main()
//{
//	thread t1(func, 100);
//	thread t2(func, 100);
//
//	t1.join();
//	t2.join();
//	return 0;
//}
//#include<mutex>
//void func(vector<int>& v, int n, int base, mutex& mtx)
//{
//	try
//	{
//		for (int i = 0; i < n; ++i)
//		{
//			mtx.lock();
//			cout << this_thread::get_id() << ":" << base + i << endl;
//
//			// push_back失败了,例如：扩容失败,抛异常 -- 异常安全的问题
//			v.push_back(base + i);
//
//			// 模拟push_back失败抛异常
//			if (base == 1 && i == 8) //让第一个线程抛异常
//				throw bad_alloc();
//
//			//抛异常之后,这句代码就执行不到了
//			mtx.unlock();
//		}
//	}
//	catch (const exception& e)
//	{
//		cout << e.what() << endl;
//		mtx.unlock();//解锁
//	}
//}
//
//int main()
//{
//	thread t1, t2;
//	vector<int> vec;
//	mutex mtx;
//	t1 = thread(func, std::ref(vec), 100, 1, std::ref(mtx));//线程1从值为1开始插入,插入个数100个
//	t2 = thread(func, std::ref(vec), 100, 100, std::ref(mtx));//线程1从值为100开始插入,插入个数100个
//	t1.join();
//	t2.join();
//	for (auto e : vec)
//	{
//		cout << e << " ";
//	}
//	cout << endl << endl;
//	cout << vec.size() << endl;
//	return 0;
//}
//
//void func(int& n, int times)
//{
//	for (int i = 0; i < times; i++)
//	{
//		n++;
//	}
//}
//int main()
//{
//	int n = 0;
//	int times = 100000; //每个线程对n++的次数
//	thread t1(func, ref(n), times);
//	thread t2(func, ref(n), times);
//
//	t1.join();
//	t2.join();
//	n++;
//	cout << n << endl; //打印n的值
//	return 0;
//}
//#include<mutex>
//void func(int& n, int times, mutex& mtx)
//{
//	mtx.lock();
//	for (int i = 0; i < times; i++)
//	{
//		//mtx.lock();
//		n++;
//		//mtx.unlock();
//	}
//	mtx.unlock();
//}
//int main()
//{
//	int n = 0;
//	int times = 100000; //每个线程对n++的次数
//	mutex mtx;
//	thread t1(func, ref(n), times, ref(mtx));
//	thread t2(func, ref(n), times, ref(mtx));
//
//	t1.join();
//	t2.join();
//	cout << n << endl; //打印n的值 200000
//	return 0;
//}
//#include<mutex>
//int main()
//{
//	int n = 100;
//	mutex mtx;
//	condition_variable cv;
//	bool flag = true;
//	//奇数
//	thread t1([&] {
//		int i = 1;
//		while (i <= 100)
//		{
//			unique_lock<mutex> ul(mtx);
//			cv.wait(ul, [&flag]()->bool {return flag; }); //等待条件变量满足
//			cout << this_thread::get_id() << ":" << i << endl;
//			i += 2;
//			flag = false;
//			cv.notify_one(); //唤醒条件变量下等待的一个线程
//		}
//		});
//	//偶数
//	thread t2([&] {
//		int j = 2;
//		while (j <= 100)
//		{
//			unique_lock<mutex> ul(mtx);
//			cv.wait(ul, [&flag]()->bool {return !flag; }); //等待条件变量满足
//			cout << this_thread::get_id() << ":" << j << endl;
//			j += 2;
//			flag = true;
//			cv.notify_one(); //唤醒条件变量下等待的一个线程
//		}
//		});
//
//	t1.join();
//	t2.join();
//	return 0;
//}
//#include<mutex>
//int main()
//{
//	int n = 100;
//	int i = 1;
//	mutex mtx;
//	condition_variable cv;//条件变量
//	bool flag = false;//默认给的是false
//	//这里后打印
//	thread t2([n, &i, &mtx, &cv, &flag] {
//		while (i < n)
//		{
//			unique_lock<mutex> lock(mtx);
//			// 如果flag是false的时候,这里会一直阻塞，直到另一个线程打印完成后,把flag变成true
//			cv.wait(lock, [&flag]() {return flag; });
//
//			cout << this_thread::get_id() << ":->" << i << endl;
//			++i;
//
//			flag = false;
//
//			cv.notify_one();//通知
//		}
//		});
//	//这里先打印
//	thread t1([n, &i, &mtx, &cv, &flag] {
//		while (i < n)
//		{
//			unique_lock<mutex> lock(mtx);
//			// 最初flag为false, !flag是true,那么这里获取就不会阻塞,优先运行,然后把flag改为true,
//			cv.wait(lock, [&flag]() {return !flag; });
//
//			cout << this_thread::get_id() << "->:" << i << endl;
//			++i;
//
//			// 保证下一个打印运行一定是t2，也可以防止t1连续打印运行
//			flag = true;
//
//			cv.notify_one();
//		}
//		});
//
//	// 交替走
//
//	t1.join();
//	t2.join();
//
//	return 0;
//}

#include<mutex>
#include<thread>
void func(vector<int>& v, int n, int base, mutex& mtx)
{
	try
	{
		for (int i = 0; i < n; ++i)
		{
			mtx.lock();
			cout << this_thread::get_id() << ":" << base + i << endl;

			// push_back失败了,例如：扩容失败,抛异常 -- 异常安全的问题
			v.push_back(base + i);

			// 模拟push_back失败抛异常
			if (base == 1 && i == 8) //让第一个线程抛异常
				throw bad_alloc();

			//抛异常之后,这句代码就执行不到了
			mtx.unlock();
		}
	}
	catch (const exception& e)
	{
		cout << e.what() << endl;
		mtx.unlock();//解锁
	}
}

int main()
{
	thread t1, t2;
	vector<int> vec;
	mutex mtx;
	t1 = thread(func, std::ref(vec), 100, 1, std::ref(mtx));//线程1从值为1开始插入,插入个数100个
	t2 = thread(func, std::ref(vec), 100, 100, std::ref(mtx));//线程1从值为100开始插入,插入个数100个
	t1.join();
	t2.join();
	for (auto e : vec)
	{
		cout << e << " ";
	}
	cout << endl << endl;
	cout << vec.size() << endl;
	return 0;
}