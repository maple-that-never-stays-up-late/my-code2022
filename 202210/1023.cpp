#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
#include<mutex>
using namespace std;

/*
//饿汉模式
class Singleton
{
public:
	static Singleton& GetInstance()
	{
		return m_instance;
	}
private:
	Singleton(){}
	Singleton(const Singleton&) = delete;
	Singleton& operator=(const Singleton&) = delete;
private:
	static Singleton m_instance;
};

Singleton Singleton:: m_instance;

*/

#if 0
//懒汉模式
class Singleton
{
public:
	/*
	static Singleton& GetInstance()
	{
		mtx.lock();
		if (_inst == nullptr)
		{
			_inst = new Singleton;
		}
		mtx.unlock();
		return *_inst;
	}
	*/
	/*
	static Singleton& GetInstance()
	{
		std::unique_lock<mutex> lock(mtx);
		if (_inst == nullptr)
		{
			_inst = new Singleton;
		}
		//lock.unlock();
		return *_inst;
	}
	*/
	static Singleton& GetInstance()
	{
		if (_inst == nullptr) //双加锁检查
		{
			std::unique_lock<mutex> lock(mtx);
			if (_inst == nullptr)
			{
				_inst = new Singleton;
			}
			//lock.unlock();
		}
		return *_inst;
	}
private:
	Singleton() {}
	Singleton(const Singleton&) = delete;
	Singleton& operator=(const Singleton&) = delete;
private:
	static mutex mtx;
	static Singleton* _inst;
};
Singleton* Singleton::_inst = nullptr;
mutex Singleton::mtx;

#endif
class Singleton
{
public:
	static Singleton& GetInstance()
	{
		static Singleton _inst;
		return _inst;
	}
private:
	Singleton() {}
	Singleton(const Singleton&) = delete;
	Singleton& operator=(const Singleton&) = delete;
};

//int main()
//{
//	thread t1([] {cout << &Singleton::GetInstance() << endl; });
//
//	thread t2([] {cout << &Singleton::GetInstance() << endl; });
//
//	t1.join();
//	t2.join();
//	cout << &Singleton::GetInstance() << endl;
//	cout << &Singleton::GetInstance() << endl;
//	cout << &Singleton::GetInstance() << endl;
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 1; i <= 100; i += 2)
//	{
//		int flag = 1;	//假设为素数
//		int j = 0;
//		//试除到开平方i
//		for (j = 2; j <= sqrt(i); j++)
//		{
//
//			if (i % j == 0)
//			{
//				//被整除了,说明不是素数
//				flag = 0;
//				break;
//			}
//		}
//		if (flag == 1)
//		{
//			printf("%d ", i);
//			count++;
//		}
//	}
//	printf("\n100-200之间的素数个数为%d个\n", count);
//	return 0;
//}

#include<cstdio>
int main()
{
	int a, b, c;
	scanf("%d %d %d", &a, &b, &c);
	int big = max(max(a, b), c);
	int less = min(min(a, b), c);
	printf("%d %d\n", big, less);
	return 0;
}

int main()
{
	int ans = 1;
	for (int i = 1; i <= 100; i++)
	{
		ans *= i;
	}
	printf("%d\n", ans);
	return 0;
}

