#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
using namespace std;

//C = A + B, A >= 0, B >= 0  其中低下标存的是数据的低位
vector<int> add(vector<int>& A, vector<int>& B)
{
	vector<int> C;//存放相加结果
	int carry = 0;//进位
	for (int i = 0; i < A.size() || i < B.size(); i++)
	{
		//ans = A[i] + B[i] + carry
		int ans = carry;//本轮计算结果
		if (i < A.size()) ans += A[i];
		if (i < B.size()) ans += B[i];
		C.push_back(ans % 10);
		carry = ans / 10;//进位
	}
	if (carry == 1)//最后看一下最高位有没有进位,有进位就要补1
		C.push_back(1);

	//123+456 = 579  最后C中存储的就是:975
	return C;
}
//判断是否有A>=B
bool cmp(vector<int>& A, vector<int>& B)
{
	if (A.size() != B.size())  
		return A.size() > B.size();
	//二者长度相等,需要判断谁更大,从数的高位开始判断  例如:判断999和998的大小
	//数组中低下标存的是低位,所以倒着比较
	for (int i = A.size() - 1; i >= 0; i--)
	{
		if (A[i] != B[i])
			return A[i] > B[i];
	}
	return true;//二者相等
}

// 高精度减法
// C = A - B, 满足A >= B, A >= 0, B >= 0
vector<int> sub(vector<int>& A, vector<int>& B) //调用时要保证A数组更大
{
	vector<int> C;
	int t = 0;//借位
	for (int i = 0; i < A.size(); i++)
	{
		//直接复用借位变量,当前这一位的计算值就是:t = A[i] - B[i] - t
		t = A[i] - t;
		if (i < B.size()) t -= B[i];

		//如果此时的计算结果t>=0的话,就是t本身,如果<0的话,就返回t+10 (向上一位借位)
		//上面两种情况合二为一: (t + 10) % 10
		C.push_back((t + 10) % 10);
		if (t < 0) //需要向上一位借位
			t = 1;
		else
			t = 0;
	}
	//去掉前导0
	//如果最后结果是0的话,就要保留这个0,所以必须保证C.size() > 1
	while (C.size() > 1 && C.back() == 0)
		C.pop_back();
	return C;
}

// C = A * b, A >= 0, b > 0
vector<int> mul(vector<int>& A, int b)//高精度整数乘低精度整数
{
	vector<int> C;
	int carry = 0;//进位
	for (int i = 0; i < A.size() || carry; i++)//A数组没有循环完成 || 还有进位
	{
		if (i < A.size()) carry =carry +  A[i] * b;
		C.push_back(carry % 10);//当前这一位的结果
		carry /= 10;//向上一位的进位
	}
	return C;
}

// A / b = C ... rem, A >= 0, b > 0  余数是rem 商是C
vector<int> div(vector<int>& A, int b, int& rem) //高精度除以低精度
{
	vector<int> C;
	rem = 0;//余数
	for (int i = A.size() - 1; i >= 0; i--) //从高位开始运算
	{
		rem = rem * 10 + A[i];//当前位的除数
		C.push_back(rem / b);//当前位计算结果
		rem %= b;//当前位计算之后的余数
	}

	//注意:此时C[0]存的是计算结果的最高位
	//为了统一倒叙打印数组C需要反转让C[0]存的是最低位
	reverse(C.begin(), C.end());

	//需要去除前导0 实际计算结果:00999  数组中:99900 需要去掉前导0
	while (C.size() > 1 && C.back() == 0) 
		C.pop_back();
	return C;
}
//int	main()
//{
//	vector<int> v1{ 3, 2, 1 };
//	vector<int> v2{ 6,5,4 };
//	vector<int> ans = add(v1,v2);
//
//	//for (int i = ans.size() - 1; i >= 0; i--)//倒序打印
//	//	cout << ans[i] << " ";
//	//cout << endl;
//
//	//ans = sub(v2,v1);
//	//for (int i = ans.size() - 1; i >= 0; i--)//倒序打印
//	//	cout << ans[i] << " ";
//	//cout << endl;
//
//	//ans = mul(v2, 3);
//	//for (int i = ans.size() - 1; i >= 0; i--)//倒序打印
//	//	cout << ans[i] << " ";
//	//cout << endl;
//
//	int rem = 0;
//	ans = div(v2, 3,rem); //456 /3 
//	for (int i = ans.size() - 1; i >= 0; i--)//倒序打印
//		cout << ans[i] << " ";
//	cout << rem << endl;
//	cout << endl;
//	return 0;
//}

//差分 时间复杂度 o(m)
#include<iostream>
using namespace std;
const int N = 1e5 + 10;
int a[N], b[N];
int main()
{
	int n, m;
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; i++)
	{
		scanf("%d", &a[i]);
		b[i] = a[i] - a[i - 1];      //构建差分数组
	}
	int l, r, c;
	while (m--) //进行m次操作
	{
		scanf("%d%d%d", &l, &r, &c);
		b[l] += c;     //将序列中[l, r]之间的每个数都加上c
		b[r + 1] -= c;
	}
	for (int i = 1; i <= n; i++)
	{
		//b[i] = a[i] - a[i - 1] -> a[i] =  b[i] + a[i - 1]
		a[i] = b[i] + a[i - 1];    //前缀和运算
		printf("%d ", a[i]);
	}
	return 0;
}