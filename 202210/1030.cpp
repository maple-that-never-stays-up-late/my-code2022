#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
#include<stdlib.h>
#include<stdio.h>
#include<string>
using namespace std;
#if 0
int main()
{
	string str;
	getline(cin, str);
	int n = str.size();
	for (int i = 0; i < n; i++)
	{
		int j = i;//单词的开头位置
		while (j < n && str[j] != ' ')
			j++;
		//此时j指向的就是空格 [i,j)就是单词
		for (int k = i; k < j; k++)
			cout << str[k] ;
		cout << endl;

		i = j;//下一步i++,就到了下一个单词的开头
	}
	return 0;
}
#endif
#include<algorithm>
#if 0
int main()
{
	vector<int> v{ 2,1,1,4,4,5,3,3 };
	sort(v.begin(), v.end());
	for (auto& x : v)	cout << x << " "; cout << endl;

	auto it = unique(v.begin(), v.end());
	for (auto& x : v)	cout << x << " "; cout << endl;
	cout << *it << endl;
	return 0;
}
#endif
