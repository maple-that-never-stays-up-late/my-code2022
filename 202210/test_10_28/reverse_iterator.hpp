namespace Mango
{

 template<class Iterator,class Ref,class Ptr>
 class reverse_iterator
 {
public:
	typedef reverse_iterator<Iterator,Ref,Ptr> self;//迭代器自身类型
	
	reverse_iterator(Iterator it)
		:_it(it)
	{}
	Ref operator*()
	{
		Iterator prev = --_it;
		return *prev;
	}
	Ptr operator->()
	{
		Iterator prev = --_it;
		return &prev;
	}
	self& operator--()
	{
		++_it;
		return *this;
	}
	self operator--(int)
	{
		self tmp(*this);
		_it--;
		return tmp;
	}
	self& operator++()
	{
		--_it;
		return *this;
	}
	self operator++(int)
	{
		self tmp(*this);
		_it--;
		return tmp;	
	}
	bool operator!=(const self& rit) 
	{
		return _it != rit._it;
	}
	bool operator==(const self& rit)
	{
		return _it == rit._it;
	}
private:
	Iterator _it;//正向迭代器	 	
 };
}