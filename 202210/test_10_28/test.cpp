#include"list.hpp"

//测试构造+拷贝构造 + 赋值重载
void test_list1()
{
    Mango::list<int> lt1;
    lt1.push_back(1);
    lt1.push_back(2);
    lt1.PrintList();

    Mango::list<int> lt2 = lt1;//拷贝构造
    lt2.PrintList();

    Mango::list<int> lt3;
    lt3 = lt1;//赋值重载
    lt3.PrintList();
}
struct Date
{
    int _year;
    int _month;
    int _day;

    Date(int year = 1, int month = 1, int day = 1)
        :_year(year)
        , _month(month)
        , _day(day)
    {}
};

//测试正向迭代器的operator* 和operator->
void test_list2()
{
    Mango::list<Date> lt;
    lt.push_back(Date(2022, 3, 12));
    lt.push_back(Date(2022, 3, 13));
    lt.push_back(Date(2022, 3, 14));

    Mango::list<Date>::iterator it = lt.begin();
    while (it != lt.end())
    {
        cout << (*it)._year << "/" << (*it)._month << "/" << (*it)._day << endl;
        cout << it->_year << "/" << it->_month << "/" << it->_day << endl;

        ++it;
    }
    cout << endl;
}
//测试正向迭代器
void test_list3()
{
    Mango::list<int> lt1;
    lt1.push_back(1);
    lt1.push_back(2);
    lt1.push_back(3);

    //lt1.clear();

    Mango::list<int> lt2(lt1);
    for (auto& e : lt2)
    {
        cout << e << " ";
    }
    cout << endl;

    Mango::list<int> lt3;
    lt3.push_back(10);
    lt3.push_back(10);
    lt3.push_back(10);
    lt3.push_back(10);

    lt1 = lt3;
    for (auto e : lt1)
    {
        cout << e << " ";
    }
    cout << endl;
}


//测试n个值初始化+迭代器区间初始化是否能准确匹配
void test_list4()
{
    //没有问题
    Mango::list<Date> lt1(5, Date(2022, 3, 15));//用5个日期类对象初始化
    for (auto& e : lt1)
    {
        cout << e._year << "/" << e._month << "/" << e._day << endl;
    }
    cout << endl;

    Mango::list<int> lt2(5, 1);//用5个1初始化  ->但是调用的是迭代器区间初始化->err.报错:非法寻址
    for (auto e : lt2)
    {
        cout << e << " ";
    }
    cout << endl;
}

//测试构造函数时的列表初始化
void test_list5()
{
    Mango::list<int> lt{ 1,2,3,4,5 };
    lt.PrintList();
}
//测试反向迭代器
void test_list6()
{
    Mango::list<int> lt{ 1,2,3,4,5 };
    lt.PrintList();
    auto it = lt.rbegin();
    while (it != lt.rend())
    {
        cout << *it << " ";
        ++it;
    }
    cout << endl;
}
//int main()
//{
//    //test_list6();
//    bool a = ((true == true) ? (true == true) ? false : true : true);
//    cout << a << endl;
//	return 0;
//}