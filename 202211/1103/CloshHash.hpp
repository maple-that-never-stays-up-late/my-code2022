#pragma once
#include<iostream>
#include<vector>
using namespace std;

#if 0
enum Status
{
	EXIST,
	EMPTY,
	DELETE
};

template<class K,class V>
struct HashNode
{
	pair<K, V> _kv;
	Status _status = EMPTY;
	HashNode() { }
	HashNode(const pair<K,V>& kv)
		:_kv(kv),_status(EMPTY)
	{}
};

template<class K,class V>
class HashTable
{
public:
	typedef HashNode<K, V> HashNode;
	HashNode* Find(const K& key)
	{
		if (_tables.size() == 0)
			return nullptr;
		size_t start = key % _tables.size();
		int index = start;
		while (_tables[index]._status != EMPTY)
		{
			if ((_tables[index]._status == EXIST) &&(_tables[index]._kv.first == key))
				return &_tables[index];

			index++; //线性探测
			index %= _tables.size();
		}
		return nullptr;
	}
	bool insert(const pair<K, V>& kv)
	{
		HashNode* ret = Find(kv.first);
		if (ret != nullptr) return false;

		if (_n == 0 || _n * 10 / _tables.size() > 7  )
		{
			size_t newcapacity = _tables.size() == 0 ? 10 : _tables.size() * 2;
			HashTable<K, V> newTable;
			newTable._tables.resize(newcapacity);
			for (auto& e : _tables) //e:hashnode类型的节点
			{
				if (e._status == EXIST)
				{
					newTable.insert(e._kv);
				}
			}
			_tables.swap(newTable._tables);
		}

		size_t start = kv.first % _tables.size();
		int index = start;
		while (_tables[index]._status == EXIST)
		{
			index++;
			index %= _tables.size();
		}
		_tables[index]._kv = kv;
		_tables[index]._status = EXIST;
		_n++;
		return true;
	}
	bool erase(const K& key)
	{
		HashNode* ret = find(key);
		if (!ret)
		{
			return false;
		}
		ret->_status = DELETE;
		_n--;
	}

	void TestHashTable1()
	{
		//HashTable<int, int, Hash<int>> ht;
		HashTable<int, int> ht;

		int a[] = { 2, 12, 22, 32, 42, 52, 62 };
		for (auto e : a)
		{
			ht.insert(make_pair(e, e));
		}

		ht.insert(make_pair(72, 72));
		ht.insert(make_pair(32, 32));
		ht.insert(make_pair(-1, -1));
		ht.insert(make_pair(-999, -999));
	}
private:
	vector<HashNode> _tables;
	int _n;
};

#endif

//开散列
template<class K>
struct Hash
{
	//将返回值转为无符号整数！这样可以把负数也处理了！
	size_t operator()(const K& key)
	{
		return key;
	}
};

template<class K,class V>
struct HashNode
{
	pair<K, V> _kv;
	HashNode<K, V>* _next = nullptr;
	HashNode(const pair<K, V>& kv)
	{
		_kv = kv;
		_next = nullptr;
	}
};

template<class K,class V,class HashFunc = Hash<K> >
class LinkHash
{
public:
	typedef HashNode<K, V> HashNode;
	HashNode* Find(const K& key)
	{
		if (_tables.size() == 0)
		{
			return nullptr;
		}
		HashFunc hf;//定义一个仿函数对象
		size_t start = hf(key) % _tables.size();
		HashNode* cur = _tables[start];
		while (cur)
		{
			if (cur->_kv.first == key)
			{
				return cur;
			}
			cur = cur->_next;
		}
		return nullptr;
	}

	bool insert(const pair<K, V>& kv)
	{
		HashNode* ret = Find(kv.first);
		if (ret)
			return false;
		HashFunc hf;
		if (_n == 0 || _n == _tables.size())
		{
			size_t newcapacity = _n == 0 ? 10 : _n * 2;
			vector<HashNode*> newtables;
			newtables.resize(newcapacity);

			for (int i = 0; i < _tables.size(); i++)
			{
				HashNode* cur = _tables[i];
				while (cur)
				{
					HashNode* next = cur->_next;
					size_t index = hf(cur->_kv.first) % newtables.size();
					cur->_next = newtables[index];
					newtables[index] = cur;
					cur = next;
				}
				_tables[i] = nullptr;
			}
			_tables.swap(newtables);
		}
		
		//插入当前的值
		size_t index = hf(kv.first) % _tables.size();
		HashNode* newnode = new HashNode(kv);
		newnode->_next = _tables[index];
		_tables[index] = newnode;
		_n++;
		return true;
	}
	bool Erase(const K& key)
	{
		HashNode* ret = Find(key);
		if (!ret) return false;
		
		size_t index = hf(key) % _tables.size();
		HashNode* cur = _tables[index];
		HashNode* prev = nullptr;
		while (cur)
		{
			if (cur->_kv.first == key)
			{
				if (cur == _tables[index]) //头删,换头
				{
					_tables[index] = cur->_next;
				}
				else
				{
					prev->_next = cur->_next;
				}
				delete cur;

				break;
			}
			prev = cur;
			cur = cur->_next;
		}
		--_n;
		return true;
	}
	void TestHashTable()
	{
		int a[] = { 4, 24, 14,7,37,27,57,67,34,14,54 };
		LinkHash<int, int> ht;
		for (auto e : a)	//只插入了10个元素,因为14冗余了不插入
		{
			cout << ht.Find(14) << " ";
			ht.insert(make_pair(e, e));
		}

		ht.insert(make_pair(84, 84));//触发增容
	}
private:
	vector<HashNode*> _tables;
	size_t _n = 0;
};