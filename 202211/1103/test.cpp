#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include"CloshHash.hpp"
#if 0
int main()
{
	LinkHash<int, int> h1;
	h1.TestHashTable();
	return 0;
}
#endif

#if 0
//求一个数左边比它大的数
int st[100];
int main()
{
	vector<int> v{ 6,5,8,3,1,6,8,5,2,7,9 };
	int top = -1;
	for (int i = 0; i < v.size(); i++)
	{
		//当前数>=栈顶,就弹出栈顶,因为我更靠右,更大,我肯定更适合作为答案
		while (top != -1 && st[top] <= v[i])
			top--;
		top == -1 ? cout << "-1" << endl : cout << st[top] << endl;
		st[++top] = v[i];
	}

	return 0;
}

#endif

//求一个数右边第一个比它小的数
int st[100];
int main()
{
	vector<int> v{ 6,5,8,3,1,6,8,5,2,7,9 };
	int top = -1;
	for (int i = v.size() - 1; i >= 0; i--)
	{
		//逆序遍历,如果当前数<=栈顶元素,当前元素肯定是作为答案 
		while (top != -1 && v[i] <= st[top])
			top--;
		st[++top] = v[i];
		cout << (top == -1 ? -1 : st[top]) << endl;
		
	}
	return 0;
}