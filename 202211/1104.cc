#include<iostream>
using namespace std;

template<class K>
struct BSTreeNode
{
	BSTreeNode<K>* _left;
	BSTreeNode<K>* _right;
	K _val;
	BSTreeNode(const K& val)
		:_val(val),_left(nullptr),_right(nullptr)
	{}
};

template<class K>
class BST
{
public:
	typedef BSTreeNode<K> TreeNode;		
	BST()
	{
		_root = nullptr;
	}
	
	bool insert(const K& key)
	{
		if(_root == nullptr)
		{
			_root  = new TreeNode(key);
			return true;
		}
		TreeNode* parent = nullptr;
		TreeNode* cur = _root;
		while(cur)
		{
			if(cur -> _val > key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if(cur ->_val < key)
			{
				parent = cur;
				cur  = cur -> _right;
			}
			else
			{
				return false;
			}
		}
		cur = new TreeNode(key);
		if(parent -> _val > key)
		{
			parent->_left = cur;
		}
		else
		{
			parent->_right = cur;
		}
		return true;
	}
	
	TreeNode* find(const K& key)
	{
		TreeNode* cur = _root;
		while(cur)
		{
			if(cur -> _val > key)
			{
				cur = cur-> _left;
			}
			else if(cur -> _val < key)
			{
				 cur = cur->_right;
			}
			else
			{
				return cur;
			}
		}
		return nullptr;
	}
	
	bool Erase(const K& key)
	{
		if(_root == nullptr) return false;
		TreeNode* cur = _root;
		TreeNode* parent = nullptr;
		while(cur)
		{
			if(cur -> _val > key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if(cur->_val < key)
			{
				parent = cur;
				cur = cur -> _right;
			}
			else
			{
				if(cur ->_left == nullptr)
				{
					if(parent == nullptr)
					{
						_root = cur->_right;
					}
					else
					{
						if(parent->_left == cur)
						{
							parent->_left = cur->_right;
						}
						else
						{
							parent->_right = cur->_right;
						}
					}
					delete cur;
				}
				else if(cur -> _right == nullptr)
				{
					if(parent == nullptr)
					{
						_root = cur->_left;
					}
					else
					{
						if(parent->_left == cur)
						{
							parent->_left = cur->_left;
						}
						else
						{
							parent->_right = cur->_left;
						}
					}
					delete cur;
				}
				else
				{
					TreeNode* minParent = cur;
					TreeNode* min = cur->_right;
					while(min->_left)
					{
						minParent = min;
						min = min -> _left;
					}
					cur->_val = min->_val;
					
					if(minParent->_left == min)
					{
						minParent->_left = min->_right;
					}
					else
					{
						minParent->_right= min->_right;
					}
					delete min;
				}		
			}
		}
		return false;
	}
	//将头节点返回
	TreeNode* GetNode()
	{
		return _root;
	}
	//子函数
	void _Inorder(TreeNode* root)
	{
		if (root == nullptr)
		{
			return;
		}
		_Inorder(root->_left);
		cout << root->_val << " ";
		_Inorder(root->_right);
	}
private:
	TreeNode* _root;
};

int main()
{
	int arr[] = { 326,3,532,6,5,2,32,6,547,6 };
	BST<int> bst;
	for (auto x : arr)
	{
		bst.insert(x);
	}
	bst._Inorder(bst.GetNode());
	return 0;
}