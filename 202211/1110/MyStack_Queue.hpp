#pragma once
#include<iostream>
//���������
template<class T>
struct Node
{
	Node(T val = 0, Node<T>* point = nullptr)
	{
		value = val;
		next = point;
	}
	~Node()
	{
		value = 0;
		next = nullptr;
	}
	T value;
	Node<T>* next;
};

template<class T>
class Mystack
{
public:
	Mystack()
	{
		tail = nullptr;
		_size = 0;
	}
	void push(const T& val)
	{
		Node<T>* newnode = new Node<T>(val);
		if (tail == nullptr)
		{
			tail = newnode;
		}
		else
		{
			newnode->next = tail;
			tail = newnode;
		}
		_size++;
	}
	void pop()
	{
		if (tail == nullptr)
			return;
		Node<T>* next = tail->next;
		delete tail;
		tail = next;
		_size--;
	}
	T& top()
	{
		return tail->value;
	}
	bool empty()
	{
		return _size == 0;
	}
private:
	Node<T>* tail;
	size_t _size = 0;
};

template<class T>
class Myqueue
{
public:
	Myqueue()
	{
		tail = nullptr;
		head = nullptr;
		_size = 0;
	}
	bool empty()
	{
		return _size == 0;
	}
	size_t size()
	{
		return _size;
	}
	void push(const T& val)
	{
		Node<T>* newnode = new Node<T>(val);
		if (head == nullptr)
		{
			head = tail = newnode;
		}
		else
		{
			tail->next = newnode;
			tail = newnode;
		}
		_size++;
	}
	void pop()
	{
		if (head == NULL)
			return;
		if (_size == 1)
		{
			delete head;
			head = tail = nullptr;
		}
		else
		{
			Node<T>* next = head->next;
			delete head;
			head = next;
		}
		_size--;
	}
	T& front()
	{
		return head->value;
	}
	T& back()
	{
		return tail->value;
	}
private:
	Node<T>* tail;
	Node<T>* head;
	size_t _size = 0;
};