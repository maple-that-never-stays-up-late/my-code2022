#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
using namespace std;

namespace Mango
{
	struct ListNode
	{
		ListNode* _next;
		int _val;

		ListNode(const int& val, ListNode* next = nullptr) //ע��:ȱʡ��˳��
			:_next(next), _val(val)
		{}	
	};

	void PrintList(ListNode* head)
	{
		while (head)
		{
			cout << head->_val << " ";
			head = head->_next;
		}
		cout << endl;
	}
	ListNode* ReverseList(ListNode* head)
	{
		if (head == nullptr) return nullptr;
		ListNode* next = nullptr;
		ListNode* prev = nullptr;
		ListNode* cur = head;
		while (cur)
		{
			next = cur->_next;
			cur->_next = prev;

			prev = cur;
			cur = next;
		}
		return prev;
	}
}


