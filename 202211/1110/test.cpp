#include"ReverseList.hpp"
#include"MyStack_Queue.hpp"
void Test1()
{
	using namespace Mango;
	ListNode* n1 = new ListNode(1);
	ListNode* n2 = new ListNode(2,n1);
	ListNode* n3 = new ListNode(3,n2);
	ListNode* n4 = new ListNode(4,n3);
	ListNode* n5 = new ListNode(5,n4);
	PrintList(n5);
	n5 = ReverseList(n5);
	PrintList(n5);
}
void Test2()
{
	Mystack<int> s1;
	s1.push(1);
	s1.push(2);
	s1.push(3);
	s1.push(1);
	while (!s1.empty())
	{
		cout << s1.top() << " ";
		s1.pop();
	}
	cout << endl;

	Myqueue<int> q;
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);
	while (!q.empty())
	{
		cout << q.front() << " ";
		q.pop();
	}
	cout << endl;
}
int main()
{
	Test2();
	return 0;
}