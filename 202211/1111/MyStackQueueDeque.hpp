#pragma once
#include<iostream>
using namespace std;
namespace Mango
{
template<class T>
struct ListNode
{
	ListNode(const T& v, ListNode<T>* n = nullptr)
		:next(n), val(v)
	{}
	~ListNode()
	{	//注意这里不要再delete了,否则会对一块空间释放两次,因为我们下面pop的时候已经delete了
		next = nullptr;
	}
	ListNode<T>* next;
	T val;
};
template<class T>
class MyStack
{
public:
	MyStack()
	{
		tail = nullptr;
		_size = 0;
	}
	void push(const T& val)
	{
		ListNode<T>* newnode = new ListNode<T>(val);
		if (tail == nullptr)
		{
			tail = newnode;
		}
		else
		{
			//链表头 <------ 链表尾
			//栈底   <------ 栈顶
			newnode->next = tail;
			tail = newnode;
		}
		_size++;
	}
	void pop()
	{
		if (tail == nullptr)
		{
			return;
		}
		else
		{
			ListNode<T>* tmp = tail;
			tail = tail->next;
			delete tmp;
		}
		_size--;
	}
	bool empty()
	{
		return _size == 0;
	}
	size_t size()
	{
		return _size;
	}
	T& top()
	{
		return tail->val;
	}
private:
	ListNode<T>* tail;
	size_t _size = 0;
}; 

template<class T>
class MyQueue
{
public:
	MyQueue()
		:head(nullptr),tail(nullptr),_size(0)
	{}
	void push(const T& val)
	{
		ListNode<T>* newnode = new ListNode<T>(val);
		if (head == nullptr)
		{
			head = tail = newnode;
		}
		else
		{
			tail->next = newnode;
			tail = newnode;
		}
		_size++;
	}
	void pop()
	{
		if (head == nullptr) return;
		if (head == tail)
		{
			delete head;
			head = tail = nullptr;
		}
		else
		{
			ListNode<T>* tmp = head;
			head = head->next;
			delete tmp;
		}
		_size--;
	}
	bool empty()
	{
		return _size == 0;
	}
	size_t size()
	{
		return _size;
	}
	T& front()
	{
		return head->val;
	}
	T& back()
	{
		return tail->val;
	}
private:
	ListNode<T>* head;
	ListNode<T>* tail;
	size_t _size = 0;
};

template<class T>
struct DLNode
{
	DLNode(const T& v)
		:next(nullptr),prev(nullptr),val(v)
	{}

	DLNode<T>* next;
	DLNode<T>* prev;
	T val;
};

template<class T>
class MyDeque
{
public:
	typedef DLNode<T> Node;
	MyDeque()
		:tail(nullptr),head(nullptr),_size(0)
	{}
	bool empty()
	{
		return head == nullptr;
	}
	size_t size()
	{
		return _size;
	}
	void push_front(const T& val)
	{
		Node* newnode = new Node(val);
		if (head == nullptr)
		{
			head = tail = newnode;
		}
		else
		{
			head->prev = newnode;
			newnode->next = head;
			head = newnode;
		}
		_size++;
	}
	void push_back(const T& val)
	{
		Node* newnode = new Node(val);
		if (head == nullptr)
		{
			head = tail = newnode;
		}
		else
		{
			newnode->prev = tail;
			tail->next = newnode;
			tail = newnode;
		}
		_size++;
	}
	void pop_front()
	{
		if (head == nullptr)
			return;
		if (head == tail)
		{
			delete head;
			head = tail = nullptr;
		}
		else
		{
			Node* tmp = head;
			head = head->next;
			head->prev = nullptr;//不要忘了！！！
			delete tmp;
		}
		_size--;
	}
	void pop_back() 
	{
		if (head == nullptr)
			return;
		if (head == tail)
		{
			delete head;
			head = tail = nullptr;
		}
		else
		{
			Node* tmp = tail;
			tail = tail->prev;
			tail->next = nullptr;//不要忘了！！！
			delete tmp;
		}
		_size--;
	}
	T& front()
	{
		return head->val;
	}
	T& back()
	{
		return tail->val;
	}
private:
	Node* tail;
	Node* head;
	size_t _size;
};
}

