#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include"MyStackQueueDeque.hpp"
void Test1()
{
	Mango::MyStack<int> st1;
	st1.push(1);
	st1.push(2);
	st1.push(3);
	st1.push(4);
	while (!st1.empty())
	{
		cout << st1.top() << " ";
		st1.pop();
	}
	cout << endl;
}
void Test2()
{
	Mango::MyQueue<int> st1;
	st1.push(1);
	st1.push(2);
	st1.push(3);
	st1.push(4);
	while (!st1.empty())
	{
		cout << st1.front() << " ";
		st1.pop();
	}
	cout << endl;
}
void Test3()
{
	Mango::MyDeque<int> q;
	q.push_back(1);
	q.push_front(3);
	q.push_back(2);
	q.push_front(4);
	//4 3 1 2
	while (!q.empty())
	{
		//��ʽ1��
		//cout << q.front() << " ";
		//q.pop_front();

		//��ʽ2:
		cout << q.back() << " ";
		q.pop_back();
	}
	cout << endl;
}
int main()
{
	Test3();
	return 0;
}