#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include"BSTree.hpp"
#if 0
class Base
{
public:
	virtual void func()
	{
		cout << "Base::func()" << endl;
	}
	void func1()
	{
		cout << "Base::func1()" << endl;
	}
};
int main()
{
	Base b1;
	Base* b2 = nullptr;
	//b2->func();
	b2->func1();
	return 0;
}
#endif

class Base
{
public:
	inline virtual void func() = 0{}
	void func1()
	{
		cout << "Base::func1()" << endl;
	}
};
int main()
{
	Base* pb = nullptr;
	pb->func();

	return 0;
}