#pragma once
#include<iostream>
#include<vector>
using namespace std;
template<class K>
struct BSTNode
{
	BSTNode(const K& v)
		:left(nullptr),right(nullptr),val(v)
	{}
	BSTNode<K>* left;
	BSTNode<K>* right;
	K val;
};

template<class K>
class BSTree 
{
public:
	typedef BSTNode<K> Node;
	BSTree()
		:root(nullptr)
	{}
	bool insert(const K& val)
	{
		if (Find(val))
			return false;

		if (root == nullptr)
		{
			root = new Node(val);
			return true;
		}
		Node* parent = nullptr;
		Node* cur = root;
		while (cur)
		{
			if (cur->val > val)
			{
				parent = cur;
				cur = cur->left;
			}
			else if(cur->val < val)
			{
				parent = cur;
				cur = cur->right;
			}
			else
			{
				return false;
			}
		}
		cur = new Node(val);
		if (parent->val > val)
			parent->left = cur;
		else
			parent->right = cur;

		return true;
	}
	bool Find(const K& val)
	{
		if (root == nullptr)
			return false;
		Node* cur = root;
		while (cur)
		{
			if (cur->val > val)
				cur = cur->left;
			else if (cur->val < val)
				cur = cur->right;
			else
				return true;
		}
		return false;
	}
	K& RootVal()
	{
		return root->val;
	}
	bool erase(const K& val)
	{
		if (!Find(val))
			return false;
		Node* parent = nullptr;
		Node* cur = root;
		while (cur)
		{
			if (cur->val > val)
			{
				parent = cur;
				cur = cur->left;
			}
			else if (cur->val < val)
			{
				parent = cur;
				cur = cur->right;
			}
			else
			{
				//找到了要删除的节点cur
				if (cur->left == nullptr)
				{
					if (parent == nullptr) //要删的是头
					{
						root = cur->right;
					}
					else
					{
						if (parent->left == cur)
						{
							parent->left = cur->right;
						}
						else
						{
							parent->right = cur->right;	
						}
					}
					delete cur;
				}
				else if (cur->right == nullptr)
				{
					if (parent == nullptr)
					{
						root = cur->left;
					}
					else
					{
						if (parent->left == cur)
						{
							parent->left = cur->left;
						}
						else
						{
							parent->right = cur->left;
						}
					}
					delete cur;
				}
				else //左右孩子双全 ->找右树最左节点替换
				{
					Node* minParent = cur;
					Node* min = cur->right;
					while (min->left)
					{
						minParent = min;
						min = min->left;
					}
					cur->val = min->val;
					if (minParent->left == min)
					{
						minParent->left = min->right;
					}
					else
					{
						minParent->right = min->right;
					}
					delete min;
				}
				return true;
			}
		}
		return false;
	}
	void Inorder()
	{
		_Inorder(root);
	}

	//递归版本
	
	bool insertR(const K& key)
	{
		return _insertR(root,key);
	}
	bool FindR(const K& key)
	{
		return _FindR(root, key);
	}
	bool eraseR(const K& key)
	{
		return _eraseR(root, key);
	}
private:
	void _Inorder(Node* root)
	{
		if (!root) return;
		_Inorder(root->left);
		cout << root->val << " ";
		_Inorder(root->right);
	}
	bool _FindR(Node*& root, const K& key)
	{
		if (root == nullptr)
			return false;

		if (root->val > key)
			_FindR(root->left, key);
		else if (root->val < key)
			_FindR(root->right, key);
		else
			return true;
	}
	bool _eraseR(Node*& root, const K& key)
	{
		if (root == nullptr)
			return false;
		if (root->val > key)
			_eraseR(root->left, key);
		else if (root->val < key)
			_eraseR(root->right, key);
		else
		{
			Node* del = root;
			if (root->left == nullptr)
			{
				root = root->right;
			}
			else if (root->right == nullptr)
			{
				root = root->left;
			}
			else
			{
				Node* min = root->right;
				while (min->left)
				{
					min = min->left;
				}
				swap(min->val, root->val);
				return _eraseR(root->right, key);
			}
			delete del;
			return true;
		}
	}
	bool _insertR(Node*& root, const K& key)
	{
		if (root == nullptr)
		{
			root = new Node(key);
			return true;
		}
		if (root->val > key)
			_insertR(root->left, key);
		else if (root->val < key)
			_insertR(root->right, key);
		else
			return false;
	}
	Node* root;
};
