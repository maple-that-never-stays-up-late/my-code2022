#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include"BST.hpp"
int main()
{
	BSTree<int> bt;
	vector<int> v{ 8,7,9,5,4,5,6,2};
	for (auto& x : v)
	{
		bt.insertR(x);
	}
	bt.Inorder();
	cout << endl;

	bt.erase(8);
	cout << bt.RootVal() << endl;
	bt.Inorder();

	bt.eraseR(4);
	cout << bt.RootVal() << endl;
	bt.Inorder();

	return 0;
}


//class A
//{
//public:
//	A()
//	{
//		cout << "A()" << endl;
//	}
//	explicit A(int i)
//	{
//		cout << "A(int i )" << endl;
//	}
//	A(short i)
//	{
//		cout << "A(short i)" << endl;
//	}
//	A& operator=(int i)
//	{
//		cout << "A& operator=(int i)" << endl;
//	}
//};
//int main()
//{
//	A a1 = 1;
//	A a2(1);
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	a |= (a + 1);
//	cout << a << endl;
//	return 0;
//}

#if 0
struct A {
	A() { std::cout << "A"; }
};
struct B {
	B() { std::cout << "B"; }
};

class C {
public:
	C() : a(), b() { std::cout << "C"; }

private:
	B b;
	A a;
};

int main() {
	float a = 1;
	cout << boolalpha << ((int)a == (int&)a);
	float b = 0;
	cout << boolalpha << ((int)b == (int&)b);
}
#endif