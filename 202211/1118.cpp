#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
using namespace std;
//https://www.nowcoder.com/practice/fcf87540c4f347bcb4cf720b5b350c76
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param nums int整型vector
     * @return int整型
     */
    int findPeakElement(vector<int>& nums) {
        // write code here
        if (nums[0] > nums[1])
            return 0;
        if (nums[nums.size() - 1] > nums[nums.size() - 2])
            return nums.size() - 1;
        int left = 1;
        int right = nums.size() - 2;
        //方法1:遍历
        for (int i = left; i <= right; ++i)
        {
            if (nums[i] > nums[i + 1] && nums[i] > nums[i - 1])
            {
                return i;
            }
        }
        return -1;
    }
};
int findPeakElement(vector<int>& nums) {
    // write code here
    if (nums[0] > nums[1])
        return 0;
    if (nums[nums.size() - 1] > nums[nums.size() - 2])
        return nums.size() - 1;
    int left = 1;
    int right = nums.size() - 2;
    //如果中间位置不是峰值,一定有一边比中间值mid大
    //假设mid+1位置的值大于mid的值,则右边一定存在峰值
    //因为右边的值从mid开始要么是/\ 要么是/这种单调性,两种都一定存在峰
    while (left <= right) //因为要和左右比较,所以只有一个元素不处理
    {
        int mid = (left + right) / 2;
        if (nums[mid] < nums[mid + 1])
        {
            left = mid + 1;//右边存在峰值
        }
        else if (nums[mid] < nums[mid - 1])
        {
            right = mid - 1;//左边存在峰值
        }
        else // nums[mid-1] < nums[mid] && nums[mid+1] < nums[mid]
        {
            return mid;
        }
    }
    return -1;
}

//https://www.nowcoder.com/practice/8daa4dff9e36409abba2adbe413d6fae
#include<queue>
struct TreeNode {
    int val;
    struct TreeNode* left;
    struct TreeNode* right;
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    
};
bool isCompleteTree(TreeNode* root) {
    // write code here
    if (root == nullptr)
        return true;
    queue<TreeNode*> q;
    q.push(root);
    while (!q.empty())
    {
        TreeNode* front = q.front();
        q.pop();
        if (front == nullptr)
            break;
        q.push(front->left);
        q.push(front->right);
    }

    while (!q.empty())
    {
        TreeNode* front = q.front();
        q.pop();
        if (front != nullptr)
            return false;
    }
    return true;
}


int main()
{
    vector<int> v{ 2, 4, 1, 2, 7, 8, 4 };
    cout << findPeakElement(v);
	return 0;
}