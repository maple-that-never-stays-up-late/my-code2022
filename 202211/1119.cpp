#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<vector>
#include<string>
using namespace std;
#if 0
struct TreeNode 
{
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
    TreeNode(int v)
        :val(v),left(nullptr), right(nullptr)
    {}
};
 
//https://www.nowcoder.com/practice/7298353c24cc42e3bd5f0e0bd3d1d759 合并二叉树
TreeNode* _mergeTrees(TreeNode* head1, TreeNode* head2)
{
    if (head1 == nullptr)
        return head2;
    if (!head2)
        return head1;
    TreeNode* phead = new TreeNode(head1->val + head2->val);
    phead->left = _mergeTrees(head1->left, head2->left);
    phead->right = _mergeTrees(head1->right, head2->right);
    return phead;
}
TreeNode* mergeTrees(TreeNode* t1, TreeNode* t2) {
    // write code here
    return _mergeTrees(t1, t2);
}

struct ListNode {
    int val;
    struct ListNode* next;
    ListNode(int v)
        :val(v),next(nullptr)
    {}
};
//删除链表倒数第n个节点
//https://www.nowcoder.com/practice/f95dcdafbde44b22a6d741baf71653f6
ListNode* removeNthFromEnd(ListNode* head, int n) {
    // write code here
    ListNode* fast = head;
    ListNode* slow = head;
    ListNode* prev = nullptr;
    while (n--)
    {
        if (fast == nullptr)
            return head;
        fast = fast->next;
    }
    while (fast)
    {
        fast = fast->next;
        prev = slow;
        slow = slow->next;
    }
    ListNode* del = slow;
    if (prev == nullptr) //要删的是头节点
    {
        del = head;
        head = head->next;
    }
    else
    {
        prev->next = slow->next;
    }
    delete del;
    return head;
}
#include<stack>
//https://www.nowcoder.com/practice/3fed228444e740c8be66232ce8b87c2f
//判断一个链表是否为回文结构
class Solution {
public:
    /**
     *
     * @param head ListNode类 the head
     * @return bool布尔型
     */
    bool isPail(ListNode* head) {
        // write code here
        stack<ListNode*> st;
        for (ListNode* cur = head; cur != nullptr; cur = cur->next)
            st.push(cur);
        for (ListNode* cur = head; cur != nullptr; cur = cur->next)
        {
            if (st.top()->val != cur->val) return false;
            st.pop();
        }
        return true;
    }
};

//方法2:
ListNode* findMid(ListNode* head)
{
    ListNode* fast = head;
    ListNode* slow = head;
    while (fast && fast->next)
        fast = fast->next->next, slow = slow->next;
    return slow;
}
ListNode* ReversList(ListNode* head)
{
    ListNode* prev = nullptr;
    ListNode* next = head;
    while (head)
    {
        next = head->next;
        head->next = prev;
        prev = head;
        head = next;
    }
    return prev;
}
bool isPail(ListNode* head) {
    // write code here
    ListNode* mid = findMid(head);
    ListNode* head1 = ReversList(mid);
    ListNode* head2 = head;
    while (head1 && head2)
    {
        if (head1->val != head2->val)
            return false;
        head1 = head1->next;
        head2 = head2->next;
    }
    return true;
}
#if 0
//恢复原状版本
class Solution {
public:
    /**
     *
     * @param head ListNode类 the head
     * @return bool布尔型
     */
    ListNode* findMid(ListNode* head)
    {
        ListNode* fast = head;
        ListNode* slow = head;
        while (fast && fast->next)
            fast = fast->next->next, slow = slow->next;
        return slow;
    }
    ListNode* ReversList(ListNode* head)
    {
        ListNode* prev = nullptr;
        ListNode* next = head;
        while (head)
        {
            next = head->next;
            head->next = prev;
            prev = head;
            head = next;
        }
        return prev;
    }
    bool isPail(ListNode* head) {
        // write code here
        ListNode* mid = findMid(head);
        ListNode* head1 = ReversList(mid);
        ListNode* head2 = head;
        ListNode* rhead1 = head1;
        bool flag = true;
        while (head1 && head2)
        {
            if (head1->val != head2->val)
            {
                flag = false;
                break;
            }
            head1 = head1->next;
            head2 = head2->next;
        }
        ReversList(rhead1);//恢复原状
        return flag;
    }
};
#endif

//删除有序链表中重复的元素-I
//https://www.nowcoder.com/practice/c087914fae584da886a0091e877f2c79
ListNode* deleteDuplicates(ListNode* head) {
    // write code here
    ListNode* dummy = new ListNode(101);//链表中任意节点的值满足|val|≤100
    dummy->next = head;
    ListNode* cur = head;
    ListNode* prev = dummy;

    while (cur)
    {
        if (prev->val != cur->val)
        {
            prev->next = cur;
            prev = cur;
        }
        cur = cur->next;
    }
    prev->next = nullptr;
    return dummy->next;
}

//https://www.nowcoder.com/practice/b49c3dc907814e9bbfa8437c251b028e
//链表中的节点每k个一组翻转
ListNode * GetKNode(ListNode * start, int k)
{
    while (--k && start != nullptr)
        start = start->next;
    return start;
}
void reverseSE(ListNode* start, ListNode* end)
{
    ListNode* pnext = end->next;
    ListNode* next = nullptr, * prev = nullptr, * cur = start;
    while (cur != pnext) {
        next = cur->next;
        cur->next = prev;
        prev = cur;
        cur = next;
    }
    start->next = pnext;
}
ListNode* reverseKGroup(ListNode* head, int k) {
    // write code here
    ListNode* start = head;
    ListNode* end = GetKNode(start, k);
    if (end == nullptr)
        return head;
    reverseSE(start, end);
    ListNode* lastEnd = start;//记录上一次的结尾
    head = end;
    while (lastEnd)
    {
        start = lastEnd->next;
        end = GetKNode(start, k);
        if (end == nullptr)  return head;
        reverseSE(start, end);
        lastEnd->next = end;
        lastEnd = start;
    }
    return head;
}

//二叉树的最大深度
int maxDepth(TreeNode* root) {
    // write code here
    if (root == nullptr)
        return 0;
    return max(maxDepth(root->left), maxDepth(root->right)) + 1;
}
//判断是不是二叉搜索树
/**
 * struct TreeNode {
 *  int val;
 *  struct TreeNode *left;
 *  struct TreeNode *right;
 *  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 * };
 */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param root TreeNode类
//     * @return bool布尔型
//     */
//    TreeNode* prev = nullptr;
//    bool result = true;
//    void Inorder(TreeNode* root) {
//        if (!root) return;
//        Inorder(root->left);
//        if (prev != nullptr && prev->val > root->val)
//        {
//            result = false;
//            return;
//        }
//        prev = root;
//        Inorder(root->right);
//    }
//    bool isValidBST(TreeNode* root) {
//        // write code here
//        Inorder(root);
//        return result;
//    }
//};

//判断是不是平衡二叉树
class Solution {
public:
    int depth(TreeNode* root)
    {
        if (!root)   return 0;
        return max(depth(root->left), depth(root->right)) + 1;
    }
    bool IsBalanced_Solution(TreeNode* root) {
        if (root == nullptr)
            return true;
        int ldepth = depth(root->left);
        int rdepth = depth(root->right);
        bool height = abs(ldepth - rdepth) <= 1; //注意:高度不超过1 -》 <=1

        return height && IsBalanced_Solution(root->left) && IsBalanced_Solution(root->right);
    }
};

#endif
//void func(ListNode* a)
//{
//    a->val = 30;
//}
int main()
{
    //ListNode* a = new ListNode(1);
    //ListNode* b = new ListNode(2);
    //a->next = b;
    //func(a);
    //cout << a->val << endl;
    //string a = "ab";
    //a += 1 + '0';
    //cout << a;
    char* str = (char*)"hello";
    string s = str;
    cout << s << endl;
	return 0;
}