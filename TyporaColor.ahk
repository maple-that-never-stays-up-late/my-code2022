; Typora
; 快捷增加字体颜色
; SendInput {Text} 解决中文输入法问题
 
#IfWinActive ahk_exe Typora.exe
{
    ; Ctrl+Alt+O 橙色
    ^!o::addFontColor("orange")
 
    ; Ctrl+Alt+R 红色
    ^!r::addFontColor("red")
 
    ; Ctrl+Alt+B 浅蓝色
    ^!b::addFontColor("cornflowerblue")

    ; alt+1 红色
    
    !1::addFontColor("red")
  

    
    ; alt+2 蓝色
    
    !2::addFontColor("blue")


    
    ; alt+5 橙色
    
    !5::addFontColor("green") 


     
    ; alt+4 黄色
    
    !4::addFontColor("yellow")

     

    ; alt+3 绿色
    
    !3::addFontColor("orange")

    

    ; alt+6 浅蓝色
    
    !6::addFontColor("cornflowerblue")

     

    ; alt+7 青色
    
    !7::addFontColor("cyan") 

   

    ; alt+8 紫色
    
    !8::addFontColor("purple")

}
 
; 快捷增加字体颜色
addFontColor(color){
    clipboard := "" ; 清空剪切板
    Send {ctrl down}c{ctrl up} ; 复制
    SendInput {TEXT}<font color='%color%'>
    SendInput {ctrl down}v{ctrl up} ; 粘贴
    If(clipboard = ""){
        SendInput {TEXT}</font> ; Typora 在这不会自动补充
    }else{
        SendInput {TEXT}</ ; Typora中自动补全标签
    }
}
